# hex-emu - an august 2022 fork of fex-emu

hex-emu includes several of the changes that led to a dispute, and me not participating in fex-emu anymore.

This is more than a year behind work in fex-emu right now, and unlikely to compile as is, however, I've updated this readme to include the core improvements and diffeting ideas between the projects.

See the [fex-emu.org archive](https://web.archive.org/web/20230405072217/https://fex-emu.org/) for some more information from my side.

[Showreel from this fork](https://www.youtube.com/playlist?list=PL_ZGlfnzU2tvlk-Ui-3D9kqwODXPwDU5O) (and past work on fex)

# Major changes from 2022.08 fex-emu

## Restructuring the source code (!1979, mega merge)
There are several commits related to that. The FEX-Emu codebase was not cleaned up for several years pior to this.

This mostly deals with the basics, like folder structure matching files inside folder, file name matching file contents, etc.

Also, some lightweight interface cleanup/removal/refactor.

## Guest Build System (!1891)
- Formalizes guest builds as cross compiler builds (x86_32/x86_64)
- Tries to do this using CMake Toolchain files
- Modified `FEXLinuxTests` to compile via the updated infra

## Just in time OBJ/IR Caching (!1842)
This is a new, realtime caching system, utilizing index + code files per guest file, and having the ability to work both in JIT and AOT modes. It is relativelly fault tolerant, and crashes will in general not corrupt the files - just waste some space.

The inspiration for this system is that fex would rely much more on caching, and far less on JITing, as the JIT was fairly slow. Also, there were several hopes for a slower AOT mode, where more/different optimisation was performed, possibly using a modified LLVM or some such, and/or having 'pre-optimized' files generated.

- Reading from the caches only takes a shared_lock,
- Writing to the caches takes a unique_lock, and also fnctl advisory lock when the index or the data files are resized. Atomics are also used.
- Index files are re-mapped as they grow, they grow in 64kb chunks.
- Data files are mapped/handled in 16Megs chunks.
- A translation is always contained into a single chunk. If it doesn't fit, another chunk is used. this may waste some space.
- Relocation still has to be done. This could be avoided.

Overall, the ObjCache (binary code) is ~ several faster than jitting (see !1842 for entire lifetime numbers). 

## New Register Allocator (!1972)
A very simple, linear reverse scan allocator that can handle global values - always spilled as a start, while having similar codegen results to the older register allocator.

The previous register allocator was much more complex, and most importantly had bugs around global values.

## rethunks (!1974)
This is a nearly complete re-implementation of thunks, switching to using templates and python for a much simpler code base.

Also, 32-bit thunk support, for OpenGL was a major motivator.

From the PR:
- Switching thunkgen from c++/clang + string building to python/libclang + jinja templates. Sadly, there are several bugs in libclang, that were worked around, it works around the clang issues with ubuntu 20.04 and is similar to the struct verifier scripts.
- Add 32 and 64 bit thunks, with different pairs of guest/host libs
- Build thunks using chroot headers / libs using the Guest Build System work
- Generate guest struct defs such that host libs can use guest structs directly, even cross arch
- Add NX/SEGV detection during initial compilation
- Track guest vma's NX internally, and don't map them as host PROT_EXEC

## Guest Deferred Signals, reworked dispatch, others (!1979)
This was a HUGE pr. In addition to the source re-org, major changes where

### Guest Deffered Signals
- Deliver a guest signal at a specific point, where the guest state can be re-consucted
- Deliver a guest signal with a mechanism that works similar to a real linux kernel

### Several dispatch changes
Mostly avoiding lockups / erratic behaviour in edge cases that were't handled correctly before.

# Building (as of aug 2023, on arm64 host)
```
git clone <hex-emu-repo>
cd hex-emu
git submodule update --init # depends on links from fex-emu, may break any time
mkdir build
cd build
CC=clang CXX=clang++ cmake .. # don't use ninja, it has a bug
ccmake . # Update build type to RelWithDebInfo, otherwise a vixl assert in an unused thunk breaks init
# also disable tests and such to avoid building them if you don't want them
# press c to configure and then q to exit
```

Get a rootfs (i used one from fex, named `Ubuntu_20_04.sqsh`).

Extract somewhere.


Generate a 'compile root fs' with
```
Scripts/FixupCompileRootFSLinks.py <rootfs dir> <another dir, for compilefs>
```
A 'compile root fs' simply has the symlinks resolved in such a way, that clang can use it for cross compilation.


Reconfigure with compile rootfs
```
ccmake . # Fill in compile rootfs path
```

Then finally
```
make -j 10 # replace 10 with number of cores
```

Should build.

Testing it
```
Bin/FEXLoader `which true`
# or
Bin/FEXLoader `which bash`
```

Configuration is in `~/.aex-emu/...`. Mine is

`Config.json`
```
{"Config":{"APP_CONFIG_NAME":"","ServerSocketPath":"","x86dec_SynchronizeRIPOnAllBlocks":"0","StallProcess":"0","ParanoidTSO":"0","ABINoPF":"1","ABILocalFlags":"1","X87ReducedPrecision":"1","ThunkConfig":"/home/skmp/.aex-emu/ThunkConfig.json","RA":"0","EnableAVX":"0","ObjCache":"3","IRCache":"0","Threads":"0","SingleStep":"0","MaxInst":"5000","TSOAutoMigration":"1","Multiblock":"1","TSOEnabled":"0","Core":"1","SMCChecks":"1","RootFS":"Ubuntu_20_04","GdbServer":"0","DumpIR":"no","DumpGPRs":"0","DebugHelpers":"0","O0":"0","SRA":"1","Force32BitAllocator":"0","GlobalJITNaming":"0","LibraryJITNaming":"0","BlockJITNaming":"0","GDBSymbols":"0","SilentLog":"0","OutputLog":"server"}}
```

and `ThunkConfig.json`
```
{
  "ThunksDB": {
    "GL": 1,
    "vulkan": 1,
    "wayland-client": 1
  }
}
```

to use thunks
```
sudo make install # to install them
```
then `/usr/local/share/aex-emu/ThunksDB.json`, `/usr/local/share/aex-emu/thunks-guest/libGL-x86_32-guest.so`, etc, should exist.

testing thunks
```
Bin/FEXLoader `which glxgears` # should open window and work fine
```


