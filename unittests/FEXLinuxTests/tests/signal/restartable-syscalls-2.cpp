#include<stdio.h>
#include<unistd.h>
#include<signal.h>

int ends[2];
void sig_handler(int signum) {
  printf("Inside handler function, expecting to be interrupted\n");
  char p;
}

int main() {
  struct sigaction act = { 0 };

  act.sa_flags = 0; // no restart
  act.sa_handler = &sig_handler;

  sigaction(SIGALRM, &act, NULL); // Register signal handler

  pipe(ends);

  alarm(1);  // Scheduled alarm after 2 seconds
  printf("Will interrupt in 1 second\n");

  char t;
  int rv = read(ends[0], &t, 1);

  printf("Done, %d\n", rv);

  return rv;
}
