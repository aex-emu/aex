#include<stdio.h>
#include<unistd.h>
#include<signal.h>

int ends[2];
void sig_handler(int signum) {
  printf("Inside handler function, unblocking read\n");
  char p;
  write(ends[1], &p, 1);
}

int main() {
  signal(SIGALRM, sig_handler); // Register signal handler

  pipe(ends);

  alarm(1);  // Scheduled alarm after 2 seconds
  printf("Will interrupt in 1 second\n");

  char t;
  int rv = read(ends[0], &t, 1);

  printf("Done, %d\n", rv);

  return rv;
}
