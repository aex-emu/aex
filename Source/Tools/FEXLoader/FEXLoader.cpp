/*
$info$
tags: Bin|FEXLoader
desc: Glues the ELF loader, FEXCore and LinuxSyscalls to launch an elf under fex
$end_info$
*/

#include "AOT/AOTGenerator.h"
#include "Config/ArgumentLoader.h"
#include "Tools/FEXServer/FEXServerClient.h"
#include "ELFCodeLoader2.h"
#include "GuestOS/LinuxKernel/Helpers/LinuxAllocator.h"
#include "GuestOS/LinuxKernel/Helpers/SignalDelegator.h"
#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"

#include "Config/Config.h"
#include "Config/ConfigLoader.h"

#include "Common/Paths.h"

#include "GuestCPU/Context/Context.h"
#include "GuestCPU/Context/GuestState.h"

#include "GuestCPU/Translator/Decoder/X86Tables/X86Tables.h"
#include "GuestCPU/Translator/OpcodeDispatcher/OpcodeDispatcher.h"

#include "Allocator/Allocator.h"
#include "LogMgr/LogManager.h"
#include "Telemetry/Telemetry.h"
#include "Threading/Threads.h"

#include <atomic>
#include <cerrno>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <elf.h>
#include <fcntl.h>
#include <fstream>
#include <filesystem>
#include <memory>
#include <mutex>
#include <optional>
#include <queue>
#include <set>
#include <sstream>
#include <string>
#include <sys/auxv.h>
#include <sys/resource.h>
#include <sys/select.h>
#include <system_error>
#include <thread>
#include <unistd.h>
#include <utility>
#include <vector>

#include <fmt/format.h>
#include <sys/sysinfo.h>
#include <sys/signal.h>

#include <jemalloc/jemalloc.h>
#include "Generated/git_version.h"

#include "UnitTestRunner.h"

namespace {
static bool SilentLog;
static int OutputFD {STDERR_FILENO};
static bool ExecutedWithFD {false};

void MsgHandler(LogMan::DebugLevels Level, char const *Message) {
  if (SilentLog) {
    return;
  }

  const char *CharLevel{nullptr};

  switch (Level) {
  case LogMan::NONE:
    CharLevel = "NONE";
    break;
  case LogMan::ASSERT:
    CharLevel = "ASSERT";
    break;
  case LogMan::ERROR:
    CharLevel = "ERROR";
    break;
  case LogMan::DEBUG:
    CharLevel = "DEBUG";
    break;
  case LogMan::INFO:
    CharLevel = "Info";
    break;
  case LogMan::STDOUT:
    CharLevel = "STDOUT";
    break;
  case LogMan::STDERR:
    CharLevel = "STDERR";
    break;
  default:
    CharLevel = "???";
    break;
  }

  const auto Output = fmt::format("[{}] {}\n", CharLevel, Message);
  write(OutputFD, Output.c_str(), Output.size());
  fsync(OutputFD);
}

void AssertHandler(char const *Message) {
  if (SilentLog) {
    return;
  }

  const auto Output = fmt::format("[ASSERT] {}\n", Message);
  write(OutputFD, Output.c_str(), Output.size());
  fsync(OutputFD);
}

} // Anonymous namespace

namespace FEXServerLogging {
  int FEXServerFD{};
  void MsgHandler(LogMan::DebugLevels Level, char const *Message) {
    FEXServerClient::MsgHandler(FEXServerFD, Level, Message);
  }

  void AssertHandler(char const *Message) {
    FEXServerClient::AssertHandler(FEXServerFD, Message);
  }
}

void InterpreterHandler(std::string *Filename, std::string const &RootFS, std::vector<std::string> *args) {
  // Open the file pointer to the filename and see if we need to find an interpreter
  std::fstream File(*Filename, std::fstream::in | std::fstream::binary);

  if (!File.is_open()) {
    return;
  }

  File.seekg(0, std::fstream::end);
  const auto FileSize = File.tellg();
  File.seekg(0, std::fstream::beg);

  // Is the file large enough for shebang
  if (FileSize <= 2) {
    return;
  }

  // Handle shebang files
  if (File.get() == '#' &&
      File.get() == '!') {
    std::string InterpreterLine;
    std::getline(File, InterpreterLine);
    std::vector<std::string> ShebangArguments{};

    // Shebang line can have a single argument
    std::istringstream InterpreterSS(InterpreterLine);
    std::string Argument;
    while (std::getline(InterpreterSS, Argument, ' ')) {
      if (Argument.empty()) {
        continue;
      }
      ShebangArguments.push_back(std::move(Argument));
    }

    // Executable argument
    std::string &ShebangProgram = ShebangArguments[0];

    // If the filename is absolute then prepend the rootfs
    // If it is relative then don't append the rootfs
    if (ShebangProgram[0] == '/') {
      ShebangProgram = RootFS + ShebangProgram;
    }
    *Filename = ShebangProgram;

    // Insert all the arguments at the start
    args->insert(args->begin(), ShebangArguments.begin(), ShebangArguments.end());

    // Done here
    return;
  }
}

void RootFSRedirect(std::string *Filename, std::string const &RootFS) {
  auto RootFSLink = ELFCodeLoader2::ResolveRootfsFile(*Filename, RootFS);

  std::error_code ec{};
  if (std::filesystem::exists(RootFSLink, ec)) {
    *Filename = RootFSLink;
  }
}

bool RanAsInterpreter(const char *Program) {
  return ExecutedWithFD || strstr(Program, "FEXInterpreter") != nullptr;
}

bool RanAsAOTGen(const char *Program) {
  return strstr(Program, "AOTGen") != nullptr;
}

bool RanAsUnitTestRunner(const char *Program) {
  return strstr(Program, "UnitTestRunner") != nullptr;
}

bool IsInterpreterInstalled() {
  // The interpreter is installed if both the binfmt_misc handlers are available
  // Or if we were originally executed with FD. Which means the interpreter is installed

  std::error_code ec{};
  return ExecutedWithFD ||
         (std::filesystem::exists("/proc/sys/fs/binfmt_misc/FEX-x86", ec) &&
         std::filesystem::exists("/proc/sys/fs/binfmt_misc/FEX-x86_64", ec));
}

// TODO: cleanup, these really don't belong here
static void InitializeStaticTables(FEXCore::Context::OperatingMode Mode) {
  FEXCore::X86Tables::InitializeInfoTables(Mode);
  FEXCore::IR::InstallOpcodeHandlers(Mode);
}

int main(int argc, char **argv, char **const envp) {

  const bool IsUnitTestRunner = RanAsUnitTestRunner(argv[0]);

  if (IsUnitTestRunner) {
    return unittest_main(argc, argv, envp);
  }

  const bool IsInterpreter = RanAsInterpreter(argv[0]);
  const bool IsAOTGen = RanAsAOTGen(argv[0]);

  ExecutedWithFD = getauxval(AT_EXECFD) != 0;

  LogMan::Throw::InstallHandler(AssertHandler);
  LogMan::Msg::InstallHandler(MsgHandler);

  auto Program = FEX::Config::LoadConfig(
    IsInterpreter,
    true,
    argc, argv, envp
  );

  if (Program.first.empty()) {
    if (IsAOTGen) {
      printf("Usage: %s [options] path/to/executable\n", argv[0]);
    }
    // Early exit if we weren't passed an argument
    return 0;
  }

  {
    ELFParser ProgramElf;
    int elfFd = ExecutedWithFD ? getauxval(AT_EXECFD) : open(Program.first.c_str(), O_RDONLY);
    bool Is64Bit = false;
    // This assumes any  scripts are 64-bit processes 
    // TODO: #2007
    if (!ProgramElf.ReadElfArch(elfFd) || ProgramElf.type == ELF_TYPE_X86_64) {
      Is64Bit = true;
    }

    FEXCore::Allocator::Init(Is64Bit ? 1ULL << 47 : 1ULL << 32);

    if(!ExecutedWithFD) {
      close(elfFd);
    }
  }

  auto Args = FEX::ArgLoader::Get();
  auto ParsedArgs = FEX::ArgLoader::GetParsedArgs();

  // Reload the meta layer
  FEXCore::Config::ReloadMetaLayer();
  FEXCore::Config::Set(FEXCore::Config::CONFIG_IS_INTERPRETER, IsInterpreter ? "1" : "0");
  FEXCore::Config::Set(FEXCore::Config::CONFIG_INTERPRETER_INSTALLED, IsInterpreterInstalled() ? "1" : "0");

  // Early check for process stall
  // Doesn't use CONFIG_ROOTFS and we don't want it to spin up a squashfs instance
  FEX_CONFIG_OPT(StallProcess, STALLPROCESS);
  if (StallProcess) {
    while (1) {
      // Stall this process out forever
      select(0, nullptr, nullptr, nullptr, nullptr);
    }
  }

  // Ensure FEXServer is setup before config options try to pull CONFIG_ROOTFS
  if (!FEXServerClient::SetupClient(argv[0])) {
    LogMan::Msg::EFmt("FEXServerClient: Failure to setup client");
    return -1;
  }

  FEX_CONFIG_OPT(SilentLog, SILENTLOG);
  FEX_CONFIG_OPT(IRCache, IRCACHE);
  FEX_CONFIG_OPT(ObjCache, OBJCACHE);
  FEX_CONFIG_OPT(OutputLog, OUTPUTLOG);
  FEX_CONFIG_OPT(LDPath, ROOTFS);
  FEX_CONFIG_OPT(Environment, ENV);
  FEX_CONFIG_OPT(HostEnvironment, HOSTENV);
  ::SilentLog = SilentLog();

  if (::SilentLog) {
    LogMan::Throw::UnInstallHandlers();
    LogMan::Msg::UnInstallHandlers();
  }
  else {
    auto LogFile = OutputLog();
    // If stderr or stdout then we need to dup the FD
    // In some cases some applications will close stderr and stdout
    // then redirect the FD to either a log OR some cases just not use
    // stderr/stdout and the FD will be reused for regular FD ops.
    //
    // We want to maintain the original output location otherwise we
    // can run in to problems of writing to some file
    if (LogFile == "stderr") {
      OutputFD = dup(STDERR_FILENO);
    }
    else if (LogFile == "stdout") {
      OutputFD = dup(STDOUT_FILENO);
    }
    else if (LogFile == "server") {
      LogMan::Throw::UnInstallHandlers();
      LogMan::Msg::UnInstallHandlers();

      FEXServerLogging::FEXServerFD = FEXServerClient::RequestLogFD(FEXServerClient::GetServerFD());
      if (FEXServerLogging::FEXServerFD != -1) {
        LogMan::Throw::InstallHandler(FEXServerLogging::AssertHandler);
        LogMan::Msg::InstallHandler(FEXServerLogging::MsgHandler);
      }
    }
    else if (!LogFile.empty()) {
      OutputFD = open(LogFile.c_str(), O_CREAT | O_CLOEXEC | O_WRONLY);
    }
  }
  std::string cmdline = "";

  for (int i = 0; i < argc; i++) {
    if (i != 0) {
      cmdline += " ";
    }
    cmdline += argv[i];
  }

  LogMan::Msg::IFmt("**** NEW PROCESS **** " GIT_DESCRIBE_STRING " ~ pid: {}, cmdline: '{}', ppid: {}", getpid(), cmdline, getppid());

  FEXCore::Telemetry::Initialize();

  RootFSRedirect(&Program.first, LDPath());
  InterpreterHandler(&Program.first, LDPath(), &Args);

  std::error_code ec{};
  if (!std::filesystem::exists(Program.first, ec)) {
    // Early exit if the program passed in doesn't exist
    // Will prevent a crash later
    fmt::print(stderr, "{}: command not found\n", Program.first);
    return -ENOEXEC;
  }

  uint32_t KernelVersion = FEX::HLE::SyscallHandler::CalculateHostKernelVersion();
  if (KernelVersion < FEX::HLE::SyscallHandler::KernelVersion(4, 17)) {
    // We require 4.17 minimum for MAP_FIXED_NOREPLACE
    ERROR_AND_DIE_FMT("FEXLoader requires kernel 4.17 minimum.");
  }

  // Before we go any further, set all of our host environment variables that the config has provided
  for (auto &HostEnv : HostEnvironment.All()) {
    // We are going to keep these alive in memory.
    // No need to split the string with setenv
    putenv(HostEnv.data());
  }

  ELFCodeLoader2 Loader{Program.first, LDPath(), Args, ParsedArgs, envp, &Environment};
  //FEX::HarnessHelper::ELFCodeLoader Loader{Program.first, LDPath(), Args, ParsedArgs, envp, &Environment};

  if (!Loader.ELFWasLoaded()) {
    // Loader couldn't load this program for some reason
    fmt::print(stderr, "Invalid or Unsupported elf file.\n");
#ifdef _M_ARM_64
    fmt::print(stderr, "This is likely due to a misconfigured x86-64 RootFS\n");
    fmt::print(stderr, "Current RootFS path set to '{}'\n", LDPath());
    std::error_code ec;
    if (LDPath().empty() ||
        std::filesystem::exists(LDPath(), ec) == false) {
      fmt::print(stderr, "RootFS path doesn't exist. This is required on AArch64 hosts\n");
      fmt::print(stderr, "Use FEXRootFSFetcher to download a RootFS\n");
    }
#endif
    return -ENOEXEC;
  }

  FEXCore::Config::Set(FEXCore::Config::CONFIG_APP_FILENAME, std::filesystem::canonical(Program.first).string());
  FEXCore::Config::Set(FEXCore::Config::CONFIG_APP_CONFIG_NAME, Program.second);
  FEXCore::Config::Set(FEXCore::Config::CONFIG_IS64BIT_MODE, Loader.Is64BitMode() ? "1" : "0");


  FEXCore::Paths::InitializePaths();

  // System allocator is now system allocator or FEX
  InitializeStaticTables(Loader.Is64BitMode() ? FEXCore::Context::MODE_64BIT : FEXCore::Context::MODE_32BIT);

  auto CTX = new FEXCore::Context::Context();
  
  // TODO: Cleanup
  // auto Initialized = FEXCore::CPU::CreateCPUCore(CTX); is the same as InitializeContext
  // FEXCore::Context::InitializeContext(CTX);
  // is the same as
  CTX->CPUID.Init(CTX);

  auto SignalDelegation = std::make_unique<FEX::HLE::SignalDelegator>();

  SignalDelegation->RegisterFrontendHostSignalHandler(SIGILL, [&SignalDelegation](FEXCore::Core::InternalThreadState *Thread, int Signal, void *info, void *ucontext) -> bool {
    ucontext_t* _context = (ucontext_t*)ucontext;
    auto &mcontext = _context->uc_mcontext;
    uint64_t PC{};
#ifdef _M_ARM_64
    PC = mcontext.pc;
#else
    PC = mcontext.gregs[REG_RIP];
#endif
    if (PC == reinterpret_cast<uint64_t>(&FEXCore::Assert::ForcedAssert)) {
      // This is a host side assert. Don't deliver this to the guest
      // We want to actually break here
      SignalDelegation->UninstallHostHandler(Signal);
      return true;
    }
    return false;
  }, true);

  auto SyscallHandler = Loader.Is64BitMode() ? FEX::HLE::x64::CreateHandler(CTX, SignalDelegation.get())
                                             : FEX::HLE::x32::CreateHandler(CTX, SignalDelegation.get(), FEX::HLE::CreatePassthroughAllocator());

  auto Mapper = std::bind_front(&FEX::HLE::SyscallHandler::GuestMmap, SyscallHandler.get());
  auto Unmapper = std::bind_front(&FEX::HLE::SyscallHandler::GuestMunmap, SyscallHandler.get());

  if (!Loader.MapMemory(Mapper, Unmapper)) {
    // failed to map
    LogMan::Msg::EFmt("Failed to map {}-bit elf file.", Loader.Is64BitMode() ? 64 : 32);
    return -ENOEXEC;
  }

  SyscallHandler->SetCodeLoader(&Loader);

  auto BRKInfo = Loader.GetBRKInfo();

  SyscallHandler->DefaultProgramBreak(BRKInfo.Base, BRKInfo.Size);

  CTX->SignalDelegation = SignalDelegation.get();

  CTX->SyscallHandler = SyscallHandler.get();
  CTX->SourcecodeResolver = SyscallHandler->GetSourcecodeResolver();
  CTX->X86CodeGen.Init(CTX);

  CTX->InitCore(Loader.DefaultRIP(), Loader.GetStackPointer());
  
  auto ExitCode = 0; //FEXCore::Context::ExitReason::EXIT_SHUTDOWN;

  if (IRCache() || ObjCache()) {
    LogMan::Msg::IFmt("Warning: OBJ/IR Caches are experimental, and might lead to crashes.");
  }

  if (IsAOTGen) {
    LogMan::Msg::IFmt("Warning: AOT Generation is experimental and might not work.");
  }

  auto GetCacheReader = [](bool IsIR) {

    auto JitCacheFolder = std::filesystem::path(FEXCore::Config::GetDataDirectory()) / "JitCache";

    return [JitCacheFolder=std::move(JitCacheFolder), IsIR](const std::string &fileid, const std::string &filename) -> std::optional<FEXCore::Context::CacheFDSet> {
      auto EntryFolder = JitCacheFolder / fileid;

      std::error_code ec;
      std::filesystem::create_directories(EntryFolder, ec);

      if (ec) {
        return std::nullopt;
      }

      auto EntryPath = EntryFolder / "Path";
      auto EntryIRIndex = EntryFolder / (IsIR? "IRIndex" : "OBJIndex");
      auto EntryIRData = EntryFolder / (IsIR? "IRData" : "OBJData");
      
      // Generate Path entry
      int PathFD = open(EntryPath.c_str(), O_CREAT | O_EXCL | O_WRONLY, 0644);
      if (PathFD != -1) {
        write(PathFD, filename.c_str(), filename.size());
        close(PathFD);
      }

      // Create or open cache files
      int IndexFD = open(EntryIRIndex.c_str(), O_CREAT | O_RDWR | O_CLOEXEC, 0644);
      int DataFD = open(EntryIRData.c_str(), O_CREAT | O_RDWR | O_CLOEXEC, 0644);

      if (IndexFD == -1 || DataFD == -1) {

        if (IndexFD != -1) {
          close(IndexFD);
        }

        if (DataFD != -1) {
          close(DataFD);
        }

        return std::nullopt;
      } else {
        return FEXCore::Context::CacheFDSet {
          .IndexFD = IndexFD, .DataFD = DataFD
        };
      }
    };
  };

  if (ObjCache()) {
    CTX->SetObjCacheOpener(GetCacheReader(false));
  }

  if (IRCache()) {
    CTX->SetIRCacheOpener(GetCacheReader(true));
  }


  auto Sections = Loader.PullSections();
  if (IsAOTGen) {
    for(auto &Section: Sections) {
      FEX::AOT::AOTGenSection(CTX, Section);
    }
  } else {
    unsigned arena_ind, old_arena_ind;
    if (!Loader.Is64BitMode()) {
      // set a new arena and flush tcache so that pointers > 32 bits aren't returned from malloc & friends
	    size_t sz = sizeof(unsigned);
	    je_mallctl("arenas.create", (void *)&arena_ind, &sz, NULL, 0);

      size_t size = sizeof(arena_ind);
      je_mallctl("thread.arena", (void *)&old_arena_ind, &size, (void *)&arena_ind, sizeof(arena_ind));

      je_mallctl("thread.tcache.flush", 0, 0, 0, 0);
    }
    ExitCode = CTX->RunUntilExit();
    if (!Loader.Is64BitMode()) {
      size_t size = sizeof(arena_ind);
      je_mallctl("thread.arena", (void *)&arena_ind, &size, (void *)&old_arena_ind, sizeof(arena_ind));
    }
  }

  //auto ProgramStatus = CTX->GetProgramStatus();

  SyscallHandler.reset();
  SignalDelegation.reset();
  
  // TODO: CLEAN THIS UP
  if (CTX->ParentThread) {
      CTX->DestroyThread(CTX->ParentThread);
  }
  delete CTX;

  FEXCore::Paths::ShutdownPaths();
  FEXCore::Threads::Shutdown();

  FEXCore::Config::Shutdown();

  LogMan::Throw::UnInstallHandlers();
  LogMan::Msg::UnInstallHandlers();

  // Allocator is now original system allocator
  FEXCore::Telemetry::Shutdown(Program.second);

  return ExitCode;
  /*
  if (ShutdownReason == FEXCore::Context::ExitReason::EXIT_SHUTDOWN) {
    return ProgramStatus;
  }
  else {
    return -64 | ShutdownReason;
  }*/
}
