/*
$info$
tags: Bin|TestHarnessRunner
desc: Used to run Assembly tests
$end_info$
*/

#include "Config/ArgumentLoader.h"
#include "UnitTestHostRunner.h"
#include "Tools/Common/HarnessHelpers.h"
#include "GuestOS/LinuxKernel/Helpers/LinuxAllocator.h"
#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include"GuestOS/LinuxKernel/Helpers/SignalDelegator.h"

#include "Config/Config.h"
#include "GuestCPU/Context/Context.h"
#include "GuestCPU/Context/GuestState.h"
#include "GuestCPU/Translator/CodeGen/CPUBackend.h"
#include "HostFeatures/HostFeatures.h"
#include "GuestCPU/Context/InternalThreadState.h"
#include "Allocator/Allocator.h"
#include "LogMgr/LogManager.h"

#include "GuestCPU/Translator/Decoder/X86Tables/X86Tables.h"
#include "GuestCPU/Translator/OpcodeDispatcher/OpcodeDispatcher.h"

#include <cstdint>
#include <errno.h>
#include <memory>
#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <vector>
#include <utility>
#include <functional>

static void MsgHandler(LogMan::DebugLevels Level, char const *Message) {
  const char *CharLevel{nullptr};

  switch (Level) {
  case LogMan::NONE:
    CharLevel = "NONE";
    break;
  case LogMan::ASSERT:
    CharLevel = "ASSERT";
    break;
  case LogMan::ERROR:
    CharLevel = "ERROR";
    break;
  case LogMan::DEBUG:
    CharLevel = "DEBUG";
    break;
  case LogMan::INFO:
    CharLevel = "Info";
    break;
  default:
    CharLevel = "???";
    break;
  }
  fmt::print("[{}] {}\n", CharLevel, Message);
}

static void AssertHandler(char const *Message) {
  fmt::print("[ASSERT] {}\n", Message);

  // make sure buffers are flushed
  fflush(nullptr);
}

namespace {
static const std::vector<std::pair<const char*, FEXCore::Config::ConfigOption>> EnvConfigLookup = {{
#define OPT_BASE(type, group, enum, json, default) {"FEX_" #enum, FEXCore::Config::ConfigOption::CONFIG_##enum},
#include "Generated/Config/ConfigValues.inl"
}};

// Claims to be a local application config layer
class TestEnvLoader final : public FEXCore::Config::Layer {
public:
  explicit TestEnvLoader(std::vector<std::pair<std::string_view, std::string_view>> _Env)
    : FEXCore::Config::Layer(FEXCore::Config::LayerType::LAYER_LOCAL_APP)
    , Env {std::move(_Env)} {
    Load();
  }

  void Load() override {
    std::unordered_map<std::string_view, std::string_view> EnvMap;
    for (auto &Option : Env) {
      std::string_view Key = Option.first;
      std::string_view Value = Option.second;

#define ENVLOADER
#include "Generated/Config/ConfigOptions.inl"

      EnvMap.insert_or_assign(Key, Value);
    }

    auto GetVar = [&](const std::string_view id) -> std::optional<std::string_view> {
      const auto it = EnvMap.find(id);
      if (it == EnvMap.end())
        return std::nullopt;

      return it->second;
    };

    for (auto &it : EnvConfigLookup) {
      if (auto Value = GetVar(it.first); Value) {
        Set(it.second, *Value);
      }
    }
  }

private:
  std::vector<std::pair<std::string_view, std::string_view>> Env;
};
}

static bool DidFault = true;

class HarnessSignalDelegator: public FEX::HLE::SignalDelegator {
  uint64_t HandleGuestSignalOrFault(bool Is64BitMode, FEXCore::Core::CpuStateFrame *Frame, void *SigInfo) {
    constexpr uint8_t HLT = 0xF4;
      if (reinterpret_cast<uint8_t*>(Frame->State.rip)[0] == HLT) {
        // Reached HLT -> Completed successfully
        DidFault = false;
        Frame->Thread->CTX->ExitCurrentThread(Frame->Thread);
      }
    return FEX::HLE::SignalDelegator::HandleGuestSignalOrFault(Is64BitMode, Frame, SigInfo);
  }
};

// TODO: cleanup, these really don't belong here
static void InitializeStaticTables(FEXCore::Context::OperatingMode Mode) {
  FEXCore::X86Tables::InitializeInfoTables(Mode);
  FEXCore::IR::InstallOpcodeHandlers(Mode);
}

int unittest_main(int argc, char **argv, char **const envp) {
  // Setup for 64 bit address space
  FEXCore::Allocator::Init(1ULL << 47);

  LogMan::Throw::InstallHandler(AssertHandler);
  LogMan::Msg::InstallHandler(MsgHandler);
  FEXCore::Config::Initialize();
  FEXCore::Config::AddLayer(std::make_unique<FEX::ArgLoader::ArgLoader>(argc, argv));

  // Shell env vars are ignored to not affect unit test results
  // FEXCore::Config::AddLayer(FEXCore::Config::CreateEnvironmentLayer(envp));
  FEXCore::Config::Load();

  auto Args = FEX::ArgLoader::Get();

  if (Args.size() < 2) {
    LogMan::Msg::EFmt("Not enough arguments");
    return -1;
  }

  FEX::HarnessHelper::HarnessCodeLoader Loader{Args[0], Args[1].c_str()};

  // Adds in environment options from the test harness config
  FEXCore::Config::AddLayer(std::make_unique<TestEnvLoader>(Loader.GetEnvironmentOptions()));
  FEXCore::Config::ReloadMetaLayer();

  FEXCore::Config::Set(FEXCore::Config::CONFIG_IS64BIT_MODE, Loader.Is64BitMode() ? "1" : "0");

  FEX_CONFIG_OPT(Core, CORE);

  std::unique_ptr<FEX::HLE::SignalDelegator> SignalDelegation = std::make_unique<HarnessSignalDelegator>();
  bool SupportsAVX = false;
  FEXCore::Core::CPUState State;

  InitializeStaticTables(Loader.Is64BitMode() ? FEXCore::Context::MODE_64BIT : FEXCore::Context::MODE_32BIT);

  auto CTX = new FEXCore::Context::Context();

  // Init cpuid directly
  //FEXCore::Context::InitializeContext(CTX);
  CTX->CPUID.Init(CTX);

  // Skip any tests that the host doesn't support features for
  auto HostFeatures = CTX->HostFeatures;
  SupportsAVX = HostFeatures.SupportsAVX;

  bool TestUnsupported =
    (!HostFeatures.Supports3DNow && Loader.Requires3DNow()) ||
    (!HostFeatures.SupportsSSE4A && Loader.RequiresSSE4A()) ||
    (!SupportsAVX && Loader.RequiresAVX()) ||
    (!HostFeatures.SupportsRAND && Loader.RequiresRAND()) ||
    (!HostFeatures.SupportsSHA && Loader.RequiresSHA()) ||
    (!HostFeatures.SupportsCLZERO && Loader.RequiresCLZERO()) ||
    (!HostFeatures.SupportsBMI1 && Loader.RequiresBMI1()) ||
    (!HostFeatures.SupportsBMI2 && Loader.RequiresBMI2());

  if (TestUnsupported) {
    delete CTX;
    return 0;
  }

  // TODO: CLEANUP
  //if (Core != FEXCore::Config::CONFIG_CORE) 
  if (true)
  {
    // Run through FEX
    auto SyscallHandler = Loader.Is64BitMode() ? FEX::HLE::x64::CreateHandler(CTX, SignalDelegation.get())
                                               : FEX::HLE::x32::CreateHandler(CTX, SignalDelegation.get(), FEX::HLE::CreatePassthroughAllocator());

    auto Mapper = std::bind_front(&FEX::HLE::SyscallHandler::GuestMmap, SyscallHandler.get());
    auto Unmapper = std::bind_front(&FEX::HLE::SyscallHandler::GuestMunmap, SyscallHandler.get());

    if (!Loader.MapMemory(Mapper, Unmapper)) {
      // failed to map
      LogMan::Msg::EFmt("Failed to map {}-bit elf file.", Loader.Is64BitMode() ? 64 : 32);
      return -ENOEXEC;
    }

    CTX->SignalDelegation = SignalDelegation.get();
    CTX->SyscallHandler = SyscallHandler.get();

    // FEXCore::Context::SetSignalDelegator(CTX, SignalDelegation.get());
    // FEXCore::Context::SetSyscallHandler(CTX, SyscallHandler.get());

    bool Result1 = CTX->InitCore(Loader.DefaultRIP(), Loader.GetStackPointer());

    if (!Result1) {
      return 1;
    }

    SupportsAVX = HostFeatures.SupportsAVX;
    CTX->RunUntilExit();

    // Just re-use compare state. It also checks against the expected values in config.
    State = CTX->ParentThread->CurrentFrame->State;

    SyscallHandler.reset();
  } else {
    // Run as host
    SupportsAVX = true;
    SignalDelegation->RegisterTLSState((FEXCore::Core::InternalThreadState*)UINTPTR_MAX);
    if (!Loader.MapMemory(mmap, munmap)) {
      // failed to map
      LogMan::Msg::EFmt("Failed to map {}-bit elf file.", Loader.Is64BitMode() ? 64 : 32);
      return -ENOEXEC;
    }

    DidFault = !RunAsHost(SignalDelegation, Loader.DefaultRIP(), Loader.GetStackPointer(), &State);
  }

  if (CTX->ParentThread) {
      CTX->DestroyThread(CTX->ParentThread);
  }
  delete CTX;

  // TODO: This needs to be re-done - was it Paths?
  //FEXCore::Context::ShutdownStaticTables();
  

  bool Passed = !DidFault && Loader.CompareStates(&State, nullptr, SupportsAVX);

  LogMan::Msg::IFmt("Faulted? {}", DidFault ? "Yes" : "No");
  LogMan::Msg::IFmt("Passed? {}", Passed ? "Yes" : "No");


  SignalDelegation.reset();

  FEXCore::Config::Shutdown();

  LogMan::Throw::UnInstallHandlers();
  LogMan::Msg::UnInstallHandlers();

  return Passed ? 0 : -1;
}

