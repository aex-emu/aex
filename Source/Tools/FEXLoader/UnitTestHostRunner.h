#pragma once

#include <memory>

#include "Common/ForwardDeclarations.h"

// Returns false if abnormally terminated
bool RunAsHost(std::unique_ptr<FEX::HLE::SignalDelegator> &SignalDelegation, uintptr_t InitialRip, uintptr_t StackPointer, FEXCore::Core::CPUState *OutputState);