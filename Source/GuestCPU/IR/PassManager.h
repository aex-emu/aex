/*
$info$
tags: ir|opts
$end_info$
*/

#pragma once

#include "Config/Config.h"
#include "Allocator/ThreadPoolAllocator.h"

#include <functional>
#include <memory>
#include <utility>
#include <vector>

#include "Common/ForwardDeclarations.h"


namespace FEXCore::IR {
using ShouldExitHandler = std::function<void(void)>;

class Pass {
public:
  virtual ~Pass() = default;
  virtual bool Run(IREmitter *IREmit) = 0;

  void RegisterPassManager(PassManager *_Manager) {
    Manager = _Manager;
  }

protected:
  PassManager *Manager;
};

class PassManager final {
  friend class SyscallOptimization;
public:
  void AddDefaultPasses(FEXCore::Context::Context *ctx, bool InlineConstants, bool StaticRegisterAllocation, bool NRA);
  void AddDefaultValidationPasses();
  Pass* InsertPass(std::unique_ptr<Pass> Pass, std::string Name = "") {
    Pass->RegisterPassManager(this);
    auto PassPtr = Passes.emplace_back(std::move(Pass)).get();

    if (!Name.empty()) {
      NameToPassMaping[Name] = PassPtr;
    }
    return PassPtr;
  }

  void InsertRegisterAllocationPass(bool OptimizeSRA, bool SupportsAVX, bool NRA);

  bool Run(IREmitter *IREmit);

  void RegisterExitHandler(ShouldExitHandler Handler) {
    ExitHandler = std::move(Handler);
  }

  bool HasPass(std::string Name) const {
    return NameToPassMaping.contains(Name);
  }

  template<typename T>
  T* GetPass(std::string Name) {
    return dynamic_cast<T*>(NameToPassMaping[Name]);
  }

  Pass* GetPass(std::string Name) {
    return NameToPassMaping[Name];
  }

  void RegisterSyscallHandler(FEXCore::HLE::LinuxSyscallHandlerBase *Handler) {
    SyscallHandler = Handler;
  }

protected:
  ShouldExitHandler ExitHandler;
  FEXCore::HLE::LinuxSyscallHandlerBase *SyscallHandler;

private:
  std::vector<std::unique_ptr<Pass>> Passes;
  std::unordered_map<std::string, Pass*> NameToPassMaping;

#if defined(ASSERTIONS_ENABLED) && ASSERTIONS_ENABLED
  std::vector<std::unique_ptr<Pass>> ValidationPasses;
  void InsertValidationPass(std::unique_ptr<Pass> Pass, std::string Name = "") {
    Pass->RegisterPassManager(this);
    auto PassPtr = ValidationPasses.emplace_back(std::move(Pass)).get();

    if (!Name.empty()) {
      NameToPassMaping[Name] = PassPtr;
    }
  }
#endif

  FEX_CONFIG_OPT(Is64BitMode, IS64BIT_MODE);
};
}

