/*
$info$
meta: ir|opts ~ IR to IR Optimization
tags: ir|opts
desc: Defines which passes are run, and runs them
$end_info$
*/

#include "LogMgr/LogManager.h"
#include "GuestCPU/Context/Context.h"
#include "GuestCPU/IR/PassManager.h"
#include "GuestCPU/IR/Passes.h"
#include "GuestCPU/Translator/IRPasses/RegisterAllocationPass.h"

#include "Config/Config.h"

#include "Common/ForwardDeclarations.h"

namespace FEXCore::IR {

void PassManager::AddDefaultPasses(FEXCore::Context::Context *ctx, bool InlineConstants, bool StaticRegisterAllocation, bool DoCompaction) {
  FEX_CONFIG_OPT(DisablePasses, O0);

  if (!DisablePasses()) {
    InsertPass(CreateContextLoadStoreElimination(ctx->HostFeatures.SupportsAVX));

    if (Is64BitMode()) {
      // This needs to run after RCLSE
      // This only matters for 64-bit code since these instructions don't exist in 32-bit
      InsertPass(CreateLongDivideEliminationPass());
    }

    InsertPass(CreateDeadStoreElimination(ctx->HostFeatures.SupportsAVX));
    InsertPass(CreatePassDeadCodeElimination());
    InsertPass(CreateConstProp(InlineConstants, ctx->HostFeatures.SupportsTSOImm9));

    ////// InsertPass(CreateDeadFlagCalculationEliminination());

    InsertPass(CreateSyscallOptimization());
    InsertPass(CreatePassDeadCodeElimination());

    // only do SRA if enabled and JIT
    if (InlineConstants && StaticRegisterAllocation)
      InsertPass(CreateStaticRegisterAllocationPass(ctx->HostFeatures.SupportsAVX));
  }
  else {
    // RA requires DCE
    InsertPass(CreatePassDeadCodeElimination());
    // only do SRA if enabled and JIT
    if (InlineConstants && StaticRegisterAllocation)
      InsertPass(CreateStaticRegisterAllocationPass(ctx->HostFeatures.SupportsAVX));
  }

  // not required for NRA
  if (DoCompaction) {
    // If the IR is compacted post-RA then the node indexing gets messed up and the backend isn't able to find the register assigned to a node
    // Compact before IR, don't worry about RA generating spills/fills
    InsertPass(CreateIRCompaction(ctx->OpDispatcherAllocator), "Compaction");
  }
}

void PassManager::AddDefaultValidationPasses() {
#if defined(ASSERTIONS_ENABLED) && ASSERTIONS_ENABLED
  //InsertValidationPass(Validation::CreatePhiValidation());
  //InsertValidationPass(Validation::CreateIRValidation(), "IRValidation");\

  // RAValidation is disabled for now, as it can't handle multiple fills from single spill
  //InsertValidationPass(Validation::CreateRAValidation());
  
  // Compaction is required for this, but NRA doesn't guarantee compaction has been run
  // Disable for now
  //InsertValidationPass(Validation::CreateValueDominanceValidation());
#endif
}

void PassManager::InsertRegisterAllocationPass(bool OptimizeSRA, bool SupportsAVX, bool NRA) {
  if (NRA) {
    InsertPass(IR::CreateNewRegisterAllocationPass(OptimizeSRA, SupportsAVX), "RA");
  } else {
    auto Compaction = GetPass("Compaction");
    LogMan::Throw::AFmt(Compaction, "Must have Compaction pass here");
    InsertPass(IR::CreateRegisterAllocationPass(Compaction, OptimizeSRA, SupportsAVX), "RA");
  }
}

bool PassManager::Run(IREmitter *IREmit) {
  bool Changed = false;
  for (auto const &Pass : Passes) {
    Changed |= Pass->Run(IREmit);
  }

#if defined(ASSERTIONS_ENABLED) && ASSERTIONS_ENABLED
  for (auto const &Pass : ValidationPasses) {
    Changed |= Pass->Run(IREmit);
  }
#endif

  return Changed;
}

}
