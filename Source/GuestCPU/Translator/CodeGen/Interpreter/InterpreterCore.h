#pragma once

#include <memory>


#include <Common/ForwardDeclarations.h>


namespace FEXCore::CPU {

[[nodiscard]] std::unique_ptr<CPUBackend> CreateInterpreterCore(FEXCore::Context::Context *ctx,
                                                                FEXCore::Core::InternalThreadState *Thread);
void InitializeInterpreterSignalHandlers(FEXCore::Context::Context *CTX);
CPUBackendFeatures GetInterpreterBackendFeatures();

} // namespace FEXCore::CPU
