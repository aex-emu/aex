/*
$info$
tags: ir|opts
desc: Sanity checking pass
$end_info$
*/

#include "GuestCPU/IR/IR.h"
#include "GuestCPU/IR/IREmitter.h"
#include "GuestCPU/IR/IntrusiveIRList.h"
#include "LogMgr/LogManager.h"

#include "GuestCPU/IR/PassManager.h"

#include <memory>
#include <sstream>
#include <string>

namespace FEXCore::IR::Validation {

class PhiValidation final : public FEXCore::IR::Pass {
public:
  bool Run(IREmitter *IREmit) override;
};

bool PhiValidation::Run(IREmitter *IREmit) {
  bool HadError = false;
  auto CurrentIR = IREmit->ViewIR();

  std::ostringstream Errors;

  // Walk the list and calculate the control flow
  for (auto [BlockNode, BlockHeader] : CurrentIR.GetBlocks()) {

    bool FoundNonPhi{};

    for (auto [CodeNode, IROp] : CurrentIR.GetCode(BlockNode)) {

      switch (IROp->Op) {
        // BEGINBLOCK doesn't matter for us
        case IR::OP_BEGINBLOCK: break;
        default:
          FoundNonPhi = true;
          break;
      }
    }
  }

  if (HadError) {
    std::stringstream Out;
    FEXCore::IR::Dump(&Out, &CurrentIR, nullptr);
    Out << "Errors:" << std::endl << Errors.str() << std::endl;
    LogMan::Msg::EFmt("{}", Out.str());
  }

  return false;
}

std::unique_ptr<FEXCore::IR::Pass> CreatePhiValidation() {
  return std::make_unique<PhiValidation>();
}

}
