/*
$info$
tags: ir|opts
$end_info$
*/

#include "GuestCPU/Translator/IRPasses/RegisterAllocationPass.h"
#include "GuestCPU/IR/Passes.h"
#include "GuestCPU/Context/GuestState.h"
#include "SignalDelegator/SignalDelegator.h"
#include "GuestCPU/IR/IR.h"
#include "GuestCPU/IR/IREmitter.h"
#include "GuestCPU/IR/IntrusiveIRList.h"
#include "GuestCPU/IR/RegisterAllocationData.h"
#include "Common/BucketList.h"
#include "LogMgr/LogManager.h"
#include "Common/Utils.h"
#include "Common/Utils.h"

#include <algorithm>
#include <bit>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <optional>
#include <set>
#include <strings.h>
#include <unordered_map>
#include <unordered_set>
#include <sys/user.h>
#include <utility>
#include <vector>

#define SRA_DEBUG(...) // fmt::print(__VA_ARGS__)
#define NRA_DEBUG(...) // fmt::print(__VA_ARGS__)

constexpr bool NRA_DEBUG_GLOBALS = false;

namespace FEXCore::IR {
namespace {
  constexpr uint32_t INVALID_REG = FEXCore::IR::InvalidReg;
  constexpr uint32_t INVALID_CLASS = FEXCore::IR::InvalidClass.Val;

  struct Register {
    bool Virtual;
    uint64_t Index;
  };

  struct RegisterClassShape {
    uint8_t PhysicalCount;
  };

  struct ValueMetadata {
    IR::NodeID BlockID{UINT32_MAX};
    uint32_t SpillSlot{UINT32_MAX};
  };


  struct RegisterFileShape {
    std::vector<RegisterClassShape> Classes;
  };

  FEXCore::IR::RegisterClassType GetRegClassFromNode(FEXCore::IR::IROp_Header *IROp) {
    using namespace FEXCore;

    // FIXME: This shouldn't require indirection, /OR/ control flow
    FEXCore::IR::RegisterClassType Class = IR::GetRegClass(IROp->Op);
    if (Class != FEXCore::IR::ComplexClass)
      return Class;

    // IR should be required to not need this
    // Complex register class handling
    switch (IROp->Op) {
      case IR::OP_LOADCONTEXT: {
        auto Op = IROp->C<IR::IROp_LoadContext>();
        return Op->Class;
        break;
      }
      case IR::OP_LOADREGISTER: {
        auto Op = IROp->C<IR::IROp_LoadRegister>();
        return Op->Class;
        break;
      }
      case IR::OP_LOADCONTEXTINDEXED: {
        auto Op = IROp->C<IR::IROp_LoadContextIndexed>();
        return Op->Class;
        break;
      }
      case IR::OP_LOADMEM:
      case IR::OP_LOADMEMTSO: {
        auto Op = IROp->C<IR::IROp_LoadMem>();
        return Op->Class;
        break;
      }
      case IR::OP_FILLREGISTER: {
        auto Op = IROp->C<IR::IROp_FillRegister>();
        return Op->Class;
        break;
      }
      default: break;
    }

    ERROR_AND_DIE_FMT("Unreachable");

    // Unreachable
    return FEXCore::IR::InvalidClass;
  };
} // Anonymous namespace

  union regset_t {
    struct {
      uint32_t gpr;
      uint32_t fpr;
    };
    uint32_t regs[2];
  };

  static constexpr regset_t EmptyRegset = { {0, 0} };

  class NRAPass final : public RegisterAllocationPass {     
    // Per Backend
    RegisterFileShape RFShape;

    // Per Thread / Per Allocation
    std::vector<ValueMetadata> Nodes;
    std::unique_ptr<RegisterAllocationData> AllocData;
    
    bool OptimizeSRA;
    bool SupportsAVX;

    uint32_t regfreeset[InvalidClass.Val];
    uint8_t regused[InvalidClass.Val];
    uint32_t lastraw[256];
    uint32_t defraw[256];

    IREmitter *IREmit;
    IRListView IR;
    uint32_t NextSpillSlot;

    // FIXME: This could be improved
    IR::IROp_CodeBlock *CurrentBlockIROp;

    std::vector<OrderedNode *> Globals;

#pragma region Per Run Init & Teardown
      void InitRun(IREmitter *_IREmit) {
        IREmit = _IREmit;
        IR = IREmit->ViewIR();

        // UINT32_MAX is reserved to mean no spill
        NextSpillSlot = 0;

        // Terribly inefficient, allocate twice as much to be on the safe side
        uint32_t NodeCount = IR.GetSSACount() * 2;

        // Resize to our correct size
        Nodes.resize(NodeCount);

        AllocData = RegisterAllocationData::Create(NodeCount);
      }

      void TeardownRun() {
        Globals.clear();
        Nodes.clear();
      }
#pragma region

#pragma region Allocation Logic

      [[nodiscard]] regset_t AllocateRegister(IR::NodeID CodeId, PhysicalRegister PhyReg, IR::NodeID ArgNodeId, regset_t LockedRegs) {
        return AllocateRegister(CodeId, PhyReg, ArgNodeId, LockedRegs, PhyReg);
      }

      [[nodiscard]] regset_t AllocateRegister(IR::NodeID CodeId, PhysicalRegister PhyReg, IR::NodeID ArgNodeId, regset_t LockedRegs, PhysicalRegister IRReg) {
        regused[PhyReg.Class]++;
        regfreeset[PhyReg.Class] &= ~(1 << PhyReg.Reg);

        if (PhyReg.Class == FPRClass.Val) {
          LockedRegs.fpr |= (1 << PhyReg.Reg);
        } else {
          LockedRegs.gpr |= (1 << PhyReg.Reg);
        }

        AllocData->Map[ArgNodeId.Value] = IRReg;

        lastraw[PhyReg.Raw] = CodeId.Value;
        defraw[PhyReg.Raw] = ArgNodeId.Value;

        return LockedRegs;
      }

      void GenerateRegisterFill(PhysicalRegister Phy) {
        NRA_DEBUG("Generating fill (%d): last: %d, def: %d, current: %d",
                  __LINE__, lastraw[Phy.Raw], defraw[Phy.Raw],
                  CodeId.Value);
        LogMan::Throw::AFmt(lastraw[Phy.Raw] != 0, "Cannot generate fill on a non-allocated register");

        auto LastWNode = OrderedNodeWrapper::WrapOffset(lastraw[Phy.Raw] * sizeof(IR::OrderedNode));
        lastraw[Phy.Raw] = 0;

        auto DefWNode = OrderedNodeWrapper::WrapOffset(defraw[Phy.Raw] * sizeof(IR::OrderedNode));
        defraw[Phy.Raw] = 0;

        auto LastNode = IREmit->UnwrapNode(LastWNode);
        auto DefNode = IREmit->UnwrapNode(DefWNode);
        
        IREmit->SetWriteCursor(IREmit->UnwrapNode(LastNode->Header.Previous));

        auto DefWId = DefWNode.ID().Value;
        auto SpillSlot = Nodes[DefWId].SpillSlot;

        if (SpillSlot == UINT32_MAX) {
          SpillSlot = Nodes[DefWId].SpillSlot = NextSpillSlot++;
        }

        auto DefIROp = IR.GetOp<IROp_Header>(DefNode);
        LogMan::Throw::AFmt(GetRegClassFromNode(DefIROp) != GPRPairClass, "Cannot fill GPRPairClass");

        OrderedNode *FillOp = TryRemat(DefIROp);

        if (!FillOp) {
          FillOp = IREmit->_FillRegister(DefNode, SpillSlot, RegisterClassType{Phy.Class}); // XXYX
        }

        AllocData->Map[IREmit->WrapNode(FillOp).ID().Value] = Phy;

        IREmit->ReplaceAllUsesWithRange(DefNode, FillOp,
                                        IREmit->GetIterator(LastWNode),
                                        IREmit->GetIterator(CurrentBlockIROp->Last));

        AllocData->Map[DefWId] = PhysicalRegister::Invalid();
        regused[Phy.Class]--;
        regfreeset[Phy.Class] |= (1 << Phy.Reg);
      }

      [[nodiscard]] regset_t SelectRegister(IR::NodeID CodeId, IR::OrderedNode * ArgNode, IR::IROp_Header *ArgIROp, IR::NodeID ArgNodeId, regset_t LockedRegs) {
        // we need to allocate a reg for this one
        auto Class = GetRegClassFromNode(ArgIROp);

        if (Class.Val != GPRPairClass.Val) {
          auto RegsUsed = regused[Class.Val];

          auto RegNum = ffs(regfreeset[Class.Val]) - 1;
          auto Phy = PhysicalRegister{Class, uint8_t(RegNum)};

          if (RegsUsed == RFShape.Classes[Class.Val].PhysicalCount) {
            RegNum = ffs(~LockedRegs.regs[Class.Val == FPRClass.Val ? 1 : 0]) - 1;

            Phy = PhysicalRegister{Class, uint8_t(RegNum)};

            LogMan::Throw::AFmt(RegNum < RFShape.Classes[Class.Val].PhysicalCount && (regfreeset[Phy.Class] & (1 << Phy.Reg)) == 0,
             "Spill time: failed tp pick, Class: %d, RegsUsed: %d",
              Class.Val, RegsUsed
            );

            GenerateRegisterFill(Phy);
          }

          // Allocation completed
          return AllocateRegister(CodeId, Phy, ArgNodeId, LockedRegs);
        } else {
          auto freeset = regfreeset[GPRClass.Val];

          for (uint8_t i = 0; i < RFShape.Classes[Class.Val].PhysicalCount; i++) {
            auto mask = 3 << (i * 2);
            if ((freeset & mask) == mask) {
              // found free, rejoice
              
              auto Phy0 = PhysicalRegister{GPRClass, uint8_t(i *2 + 0)};
              auto Phy1 = PhysicalRegister{GPRClass, uint8_t(i *2 + 1)};

              auto IR0 = PhysicalRegister{Class, i};

              LockedRegs = AllocateRegister(CodeId, Phy0, ArgNodeId, LockedRegs, IR0);
              LockedRegs = AllocateRegister(CodeId, Phy1, ArgNodeId, LockedRegs, IR0);

              // Allocation completed
              return LockedRegs;
            }
          }

          // Here we have to spill
          for (uint8_t i = 0; i < RFShape.Classes[Class.Val].PhysicalCount; i++) {
            auto mask = 3 << (i * 2);
            if ((LockedRegs.gpr & mask) == 0) {
              // yay let's spill it

              auto Phy0 = PhysicalRegister{GPRClass, uint8_t(i *2 + 0)};
              auto Phy1 = PhysicalRegister{GPRClass, uint8_t(i *2 + 1)};

              auto IR0 = PhysicalRegister{Class, i};

              if (!(freeset & (1 << Phy0.Reg))) {
                GenerateRegisterFill(Phy0);
              }
              if (!(freeset & (1 << Phy1.Reg))) {
                GenerateRegisterFill(Phy1);
              }

              LockedRegs = AllocateRegister(CodeId, Phy0, ArgNodeId, LockedRegs, IR0);
              LockedRegs = AllocateRegister(CodeId, Phy1, ArgNodeId, LockedRegs, IR0);

              // Allocation completed
              return LockedRegs;
            }
          }

          // Failed to spill :'(
          ERROR_AND_DIE_FMT("Spill time: failed to pick PAIR Class: %d, freeset: %x", Class.Val, freeset);
        }
      }

      // This logic should be moved to the IR frontend / emitter
      void SpillGlobals() {

        for (auto [BlockNode, BlockHeader] : IR.GetBlocks()) {
          auto BlockIROp = BlockHeader->CW<FEXCore::IR::IROp_CodeBlock>();
          LOGMAN_THROW_AA_FMT(BlockIROp->Header.Op == IR::OP_CODEBLOCK,
                              "IR type failed to be a code block");

          const auto BlockNodeID = IR.GetID(BlockNode);

          auto PreviousNode = BlockNode;

          for (auto [CodeNode, IROp] : IR.GetCode(BlockNode)) {

            auto CodeId = IREmit->WrapNode(CodeNode).ID().Value;

            Nodes[CodeId].BlockID = BlockNodeID;

            const uint8_t NumArgs = IR::GetArgs(IROp->Op);
            for (uint8_t i = 0; i < NumArgs; ++i) {
              const auto &Arg = IROp->Args[i];

              if (Arg.IsInvalid()) {
                continue;
              }
              auto ArgNode = IR.GetNode(Arg);
              auto ArgIROp = IR.GetOp<IROp_Header>(ArgNode);

              if (!ArgIROp->HasDest) {
                continue;
              }

              // Spill register has a non-local, unused, validation parameter
              if (ArgIROp->Op == OP_FILLREGISTER ||
                  ArgIROp->Op == OP_SPILLREGISTER) {
                continue;
              }

              const auto ArgNodeId = Arg.ID();
              const auto ArgNodeBlockID =
                  Nodes[ArgNodeId.Value].BlockID;

              if (ArgNodeBlockID != BlockNodeID) {
                auto &NodeSpillSlot =
                    Nodes[ArgNodeId.Value].SpillSlot;

                // Global !
                if (NodeSpillSlot == UINT32_MAX) {
                  // give it a spill slot
                  NodeSpillSlot = NextSpillSlot++;
                  Globals.push_back(ArgNode);
                }

                IREmit->SetWriteCursor(PreviousNode);
                auto FillNode = IREmit->_FillRegister(
                    ArgNode, NodeSpillSlot, GetRegClassFromNode(ArgIROp));
                IREmit->ReplaceAllUsesWithRange(
                    ArgNode, FillNode,
                    IREmit->GetIterator(IREmit->WrapNode(CodeNode)),
                    IREmit->GetIterator(BlockIROp->Last));
              }
            }

            PreviousNode = CodeNode;
          }
        }
      
        NRA_DEBUG("NRA: %ld globals", Globals.size());

        if (NRA_DEBUG_GLOBALS && Globals.size()) {
          std::stringstream out;
          FEXCore::IR::Dump(&out, &IR, nullptr);
          fmt::print(stderr,"IR-{} 0x{:x}:\n{}\n@@@@@\n", "pre", 0, out.str());
        }
      }

      // Helpers
      //

      [[nodiscard]] OrderedNode *TryRemat(IROp_Header *IROp) {

        if (IROp->Op == IR::OP_CONSTANT) {
          auto ConstantOp = IROp->C<IROp_Constant>();
          return IREmit->_Constant(ConstantOp->Header.Size * 8, ConstantOp->Constant);
        }

        return nullptr;
      }


      // Return LockedRegs, update lastraw
      [[nodiscard]] regset_t FillArgs(OrderedNodeWrapper Args[], const uint8_t NumArgs, IR::NodeID CodeId) {
        regset_t LockedRegs = EmptyRegset;

        for (uint8_t i = 0; i < NumArgs; ++i) {
          const auto &Arg = Args[i];

          if (Arg.IsInvalid()) {
            continue;
          }
          
          const auto ArgNodeId = Arg.ID();
          const auto ArgNode = IR.GetNode(Arg);

          // FIXME: this shouldn't require a memory read here
          const auto ArgIROp = IR.GetOp<IROp_Header>(ArgNode);

          if (!ArgIROp->HasDest) {
            continue; //OP_INLINECONSTANT || OP_INLINEENTRYPOINTOFFSET and such
          }

          auto ArgReg = AllocData->Map[ArgNodeId.Value];
          if (!ArgReg.IsInvalid()) {
            if (ArgReg.Class == FPRClass.Val) {
              lastraw[ArgReg.Raw] = CodeId.Value;
              LockedRegs.fpr |= 1 << ArgReg.Reg;
            } else if (ArgReg.Class == GPRClass.Val) {
              lastraw[ArgReg.Raw] = CodeId.Value;
              LockedRegs.gpr |= 1 << ArgReg.Reg;
            } else if (ArgReg.Class == GPRPairClass.Val) {
              auto PhyReg = PhysicalRegister { GPRClass, ArgReg.Reg};
              lastraw[PhyReg.Raw] = CodeId.Value;
              LockedRegs.gpr |= 3 << (ArgReg.Reg * 2);
            } else {
              ERROR_AND_DIE_FMT("Invalid Class");
            }
          }
        }

        return LockedRegs;
      }
      
      void AllocateArgs(IR::NodeID CodeId, OrderedNodeWrapper Args[], const uint8_t NumArgs, regset_t LockedRegs) {
        for (uint8_t i = 0; i < NumArgs; ++i) {
          const auto &Arg = Args[i];

          if (Arg.IsInvalid()) {
            continue;
          }
          
          const auto ArgNode = IR.GetNode(Arg);
          const auto ArgIROp = IR.GetOp<IROp_Header>(ArgNode);

          if (!ArgIROp->HasDest) {
            continue; //OP_INLINECONSTANT || OP_INLINEENTRYPOINTOFFSET and such
          }

          const auto ArgNodeId = Arg.ID();

          if (AllocData->Map[ArgNodeId.Value].IsInvalid()) {
            LockedRegs = SelectRegister(CodeId, ArgNode, ArgIROp, ArgNodeId, LockedRegs);
          }
        }
      }

      void GenerateValueSpill(IR::OrderedNode *CodeNode, FEXCore::IR::IROp_Header *IROp, uint32_t SpillSlot) {
        IREmit->SetWriteCursor(CodeNode);

        auto CodeClass = GetRegClassFromNode(IROp);
        LogMan::Throw::AFmt(CodeClass != GPRPairClass, "Cannot spill GPRPairClass");

        auto SpillOp = IREmit->_SpillRegister(CodeNode, SpillSlot, CodeClass);

        // FIXME: Shouldn't these be arguments to  _SpillRegister?
        SpillOp.first->Header.Size = IROp->Size;
        SpillOp.first->Header.ElementSize = IROp->ElementSize;
      }

      void FreeRegister(const PhysicalRegister AllocPhy) {
        // FIXME: This should be optimized
        if (AllocPhy.Class != GPRPairClass.Val) {
          regused[AllocPhy.Class]--;
          regfreeset[AllocPhy.Class] |= (1 << AllocPhy.Reg);
          lastraw[AllocPhy.Raw] = 0;
          defraw[AllocPhy.Raw] = 0;
        } else {
          auto Phy0 = PhysicalRegister {GPRClass, uint8_t(AllocPhy.Reg * 2)};
          auto Phy1 = PhysicalRegister {GPRClass, uint8_t(AllocPhy.Reg * 2 + 1)};

          FreeRegister(Phy0);
          FreeRegister(Phy1);
        }
      }

      void AllocateLocalValues() {

        for (auto [BlockNode, BlockHeader] : IR.GetBlocks()) {
          auto BlockIROp = BlockHeader->CW<FEXCore::IR::IROp_CodeBlock>();

          LOGMAN_THROW_AA_FMT(BlockIROp->Header.Op == IR::OP_CODEBLOCK, "IR type failed to be a code block");

          memset(regfreeset, UINT8_MAX, sizeof(regfreeset));
          memset(regused, 0, sizeof(regused));

          CurrentBlockIROp = BlockIROp;
          
          // FIXME: Reverse iteration is a bit tricky in the ir
          // ++ to skip sentinel op
          auto First = ++NodeIterator(IR.GetListData(), IR.GetData(), BlockIROp->Begin);
          auto Current = NodeIterator(IR.GetListData(), IR.GetData(), BlockIROp->Last);

          while (Current != First) {
            auto [CodeNode, IROp] = *--Current;

            auto CodeId = IREmit->WrapNode(CodeNode).ID();

            const uint8_t NumArgs = IR::GetArgs(IROp->Op);

            regset_t LockedRegs = FillArgs(IROp->Args, NumArgs, CodeId);
            
            // handle result allocation
            // FIXME: HasDest should be part of the meta info
            if (IROp->HasDest) {

              auto AllocPhy = AllocData->Map[CodeId.Value];
              auto SpillSlot = Nodes[CodeId.Value].SpillSlot;

              // Alloc Result reg if needed
              if (AllocPhy.IsInvalid()) {

                LogMan::Throw::AFmt(SpillSlot != UINT32_MAX || HasSideEffects(IROp->Op), "Node should have been eliminated here");
                
                [[maybe_unused]] auto Discarded = SelectRegister(CodeId, CodeNode, IROp, CodeId, LockedRegs);
                
                AllocPhy = AllocData->Map[CodeId.Value];
              }

              // generate spill if needed
              if (SpillSlot != UINT32_MAX) {
                GenerateValueSpill(CodeNode, IROp, SpillSlot);
              }

              FreeRegister(AllocPhy);
            }

            // Allocate for all args
            AllocateArgs(CodeId, IROp->Args, NumArgs, LockedRegs);
          };
        }
      }

      // It's okay, we always spill/fill here for now
      void AllocateGlobals() {
        // noop
      }

#pragma region

public:
  #pragma region Interface & Setup
      // main point right now is to get this working, not for it to be optimal
      // Idea: Instead of names, just reuse spill/fill ops for now?
      //       Then the last pass doesn't need to be implemented, for now
      bool Run(IREmitter *IREmit) override {

        InitRun(IREmit);

        // Pass 1: Go over and do LoadNamed / StoreNamed for global uses
        // This should be part of the frontend
        // This actually just spills for now
        SpillGlobals();

        // Pass 2: Allocate for each block using linear-scan
        AllocateLocalValues();
        
        // Pass 3: Allocate Names between blocks
        AllocateGlobals();

        AllocData->SpillSlotCount = NextSpillSlot;
        NRA_DEBUG("Used spill slots: %d", NextSpillSlot);

        TeardownRun();

        return true;
      }


      NRAPass(bool _OptimizeSRA, bool _SupportsAVX) : OptimizeSRA(_OptimizeSRA), SupportsAVX{_SupportsAVX} { }

      void AllocateRegisterSet(uint32_t RegisterCount, uint32_t ClassCount) override {
          LOGMAN_THROW_AA_FMT(RegisterCount <= INVALID_REG, "Up to {} regs supported", INVALID_REG);
          LOGMAN_THROW_AA_FMT(ClassCount <= INVALID_CLASS, "Up to {} classes supported", INVALID_CLASS);

          RFShape.Classes.resize(ClassCount);
        }

      void AddRegisters(FEXCore::IR::RegisterClassType Class, uint32_t RegisterCount) override {
        LOGMAN_THROW_AA_FMT(RegisterCount <= INVALID_REG, "Up to {} regs supported", INVALID_REG);

        RFShape.Classes[Class].PhysicalCount = RegisterCount;
      }

      void AddRegisterConflict(FEXCore::IR::RegisterClassType ClassConflict, uint32_t RegConflict, FEXCore::IR::RegisterClassType Class, uint32_t Reg) override {
        // noop
      }

      RegisterAllocationData* GetAllocationData() override {
        return AllocData.get();
      }

      std::unique_ptr<RegisterAllocationData> PullAllocationData() override {
        return std::move(AllocData);
      }
#pragma endregion
  };


  std::unique_ptr<FEXCore::IR::RegisterAllocationPass> CreateNewRegisterAllocationPass(bool OptimizeSRA, bool SupportsAVX) {
    return std::make_unique<NRAPass>(OptimizeSRA, SupportsAVX);
  }
}
