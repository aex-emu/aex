/*
$info$
tags: glue|thunks
$end_info$
*/

#pragma once

#include <cstdint>

#include "Common/ForwardDeclarations.h"

namespace FEXCore {
    typedef void ThunkedFunction(void* ArgsRv, uintptr_t HostAddr);

    class ThunkHandler {
    public:
        virtual ThunkedFunction* LookupThunk(const IR::SHA256Sum &sha256) = 0;
        virtual ~ThunkHandler() { }

        static ThunkHandler* Create(bool Is64Bit);
    };
};
