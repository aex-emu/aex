#pragma once

#include "GuestCPU/JitSymbols/JitSymbols.h"
#include "GuestOS/Misc/NamedRegion.h"
#include "GuestCPU/CPUID/CPUID.h"
#include "GuestOS/Misc/X86HelperGen.h"
//#include "Interface/Core/ObjectCache/ObjectCacheService.h"
#include "GuestCPU/CodeDispatcher/Dispatcher.h"
#include "Config/Config.h"
#include "GuestCPU/Context/Context.h"
#include "GuestCPU/Context/GuestState.h"
#include "HostFeatures/HostFeatures.h"
#include "SignalDelegator/SignalDelegator.h"
#include "GuestCPU/Context/InternalThreadState.h"
#include "Common/CompilerDefs.h"
#include "Threading/Event.h"
#include <optional>
#include <stdint.h>


#include <atomic>
#include <condition_variable>
#include <functional>
#include <istream>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <memory>
#include <mutex>
#include <shared_mutex>
#include <stddef.h>
#include <string>
#include <queue>
#include <vector>

#include "Common/ForwardDeclarations.h"

//TODO: Needs to be moved and not be defined twice
#ifndef ThunkDBObject_HACK
#define ThunkDBObject_HACK
struct ThunkDBObject {
  std::string LibraryName;
  std::string HostLibrary;
  std::set<std::string> Depends;
  std::vector<std::string> Overlays;
  bool Enabled{};
};
#endif

namespace FEXCore {
struct GuestCodeRange {
  uint64_t start;
  uint64_t length;
};
}

namespace FEXCore::Context {
  
  void LogCrashDump(uintptr_t HostFaultPC, uintptr_t HostFaultAddr);

  enum CoreRunningMode {
    MODE_RUN        = 0,
    MODE_SINGLESTEP = 1,
  };

  enum OperatingMode {
    MODE_32BIT,
    MODE_64BIT,
  };

  struct CustomIRResult {
    void *Creator;
    void *Data;

    explicit operator bool() const noexcept { return !lock; }

    CustomIRResult(std::unique_lock<std::shared_mutex> &&lock, void *Creator, void *Data):
      Creator(Creator), Data(Data), lock(std::move(lock)) { }

    private:
      std::unique_lock<std::shared_mutex> lock;
  };

  using CustomCPUFactoryType = std::function<std::unique_ptr<FEXCore::CPU::CPUBackend> (FEXCore::Context::Context*, FEXCore::Core::InternalThreadState *Thread)>;

  using ExitHandler = std::function<void(uint64_t ThreadId, FEXCore::Context::ExitReason)>;

  struct CacheFDSet {
    int IndexFD;
    int DataFD;
  };

  using CacheOpenerHandler = std::function<std::optional<CacheFDSet>(const std::string& fileid, const std::string& filename)>;

  struct Context {
    friend class FEXCore::HLE::LinuxSyscallHandlerBase;
  #ifdef JIT_ARM64
    friend class FEXCore::CPU::Arm64JITCore;
  #endif
  #ifdef JIT_X86_64
    friend class FEXCore::CPU::X86JITCore;
  #endif

    friend class FEXCore::CPU::InterpreterCore;
    friend class FEXCore::IR::Validation::IRValidation;

    struct {
      CoreRunningMode RunningMode {CoreRunningMode::MODE_RUN};
      uint64_t VirtualMemSize{1ULL << 36};

      // this is for internal use
      bool ValidateIRarser { false };

      FEX_CONFIG_OPT(Multiblock, MULTIBLOCK);
      FEX_CONFIG_OPT(SingleStepConfig, SINGLESTEP);
      FEX_CONFIG_OPT(GdbServer, GDBSERVER);
      FEX_CONFIG_OPT(Is64BitMode, IS64BIT_MODE);
      FEX_CONFIG_OPT(TSOEnabled, TSOENABLED);
      FEX_CONFIG_OPT(TSOAutoMigration, TSOAUTOMIGRATION);
      FEX_CONFIG_OPT(ABILocalFlags, ABILOCALFLAGS);
      FEX_CONFIG_OPT(ABINoPF, ABINOPF);
      FEX_CONFIG_OPT(IRCache, IRCACHE);
      FEX_CONFIG_OPT(ObjCache, OBJCACHE);
      FEX_CONFIG_OPT(SMCChecks, SMCCHECKS);
      FEX_CONFIG_OPT(Core, CORE);
      FEX_CONFIG_OPT(MaxInstPerBlock, MAXINST);
      FEX_CONFIG_OPT(RootFSPath, ROOTFS);
      FEX_CONFIG_OPT(ThunkHostLibsPath, THUNKHOSTLIBS);
      FEX_CONFIG_OPT(ThunkConfigFile, THUNKCONFIG);
      FEX_CONFIG_OPT(DumpIR, DUMPIR);
      FEX_CONFIG_OPT(StaticRegisterAllocation, SRA);
      FEX_CONFIG_OPT(GlobalJITNaming, GLOBALJITNAMING);
      FEX_CONFIG_OPT(LibraryJITNaming, LIBRARYJITNAMING);
      FEX_CONFIG_OPT(BlockJITNaming, BLOCKJITNAMING);
      FEX_CONFIG_OPT(GDBSymbols, GDBSYMBOLS);
      FEX_CONFIG_OPT(ParanoidTSO, PARANOIDTSO);
      FEX_CONFIG_OPT(x87ReducedPrecision, X87REDUCEDPRECISION);
      FEX_CONFIG_OPT(x86dec_SynchronizeRIPOnAllBlocks, X86DEC_SYNCHRONIZERIPONALLBLOCKS);
      FEX_CONFIG_OPT(EnableAVX, ENABLEAVX);
      FEX_CONFIG_OPT(RA, RA);

      FEX_CONFIG_OPT(DebugHelpers, DEBUGHELPERS);

      std::map<std::string, ThunkDBObject> ThunkDB{};
    } Config;

    FEXCore::HostFeatures HostFeatures;

    std::mutex ThreadCreationMutex;
    FEXCore::Core::InternalThreadState* ParentThread;
    std::vector<FEXCore::Core::InternalThreadState*> Threads;
    std::atomic_bool CoreShuttingDown{false};
    bool NeedToCheckXID{true};

    std::mutex IdleWaitMutex;
    std::condition_variable IdleWaitCV;
    std::atomic<uint32_t> IdleWaitRefCount{};

    Event PauseWait;
    bool Running{};

    std::shared_mutex CodeInvalidationMutex;

    FEXCore::CPUIDEmu CPUID;
    FEXCore::HLE::LinuxSyscallHandlerBase *SyscallHandler{};
    FEXCore::HLE::SourcecodeResolver *SourcecodeResolver{};
    std::unique_ptr<FEXCore::ThunkHandler> ThunkHandler;
    std::unique_ptr<FEXCore::CPU::Dispatcher> Dispatcher;

    //FEXCore::Context::ExitHandler CustomExitHandler;

#ifdef BLOCKSTATS
    std::unique_ptr<FEXCore::BlockSamplingData> BlockData;
#endif

    SignalDelegator *SignalDelegation{};
    X86GeneratedCode X86CodeGen;

    Context();
    ~Context();

    FEXCore::Core::InternalThreadState* InitCore(uint64_t InitialRIP, uint64_t StackPointer);
    int RunUntilExit();
    int GetProgramStatus() const;
    bool IsPaused() const { return !Running; }
    void Pause();
    void Run();
    void WaitForThreadsToRun();
    void Step();
    void Stop(bool IgnoreCurrentThread);
    void WaitForIdle();
    static void StopOtherThread(FEXCore::Core::InternalThreadState *Thread);
    [[noreturn]] static void ExitCurrentThread(FEXCore::Core::InternalThreadState *Thread);
    void SignalThread(FEXCore::Core::InternalThreadState *Thread, FEXCore::Core::SignalEvent Event);

    bool GetGdbServerStatus() const { return false; /* DebugServer != nullptr; */ }
    void StartGdbServer();
    void StopGdbServer();
    void HandleCallback(FEXCore::Core::InternalThreadState *Thread, uint64_t RIP);
    void RegisterHostSignalHandler(int Signal, HostSignalDelegatorFunction Func, bool Required);
    void RegisterFrontendHostSignalHandler(int Signal, HostSignalDelegatorFunction Func, bool Required);

    static void ThreadRemoveCodeEntry(FEXCore::Core::InternalThreadState *Thread, uint64_t GuestRIP);
    static void ThreadAddBlockLink(FEXCore::Core::InternalThreadState *Thread, uint64_t GuestDestination, uintptr_t HostLink, const std::function<void()> &delinker);

    template<auto Fn>
    static uint64_t ThreadExitFunctionLink(FEXCore::Core::CpuStateFrame *Frame, uint64_t *record) {
      std::shared_lock lk(Frame->Thread->CTX->CodeInvalidationMutex);

      return Fn(Frame, record);
    }

    // Wrapper which takes CpuStateFrame instead of InternalThreadState and unique_locks CodeInvalidationMutex
    // Must be called from owning thread
    static void ThreadRemoveCodeEntryFromJit(FEXCore::Core::CpuStateFrame *Frame, uint64_t GuestRIP) {

      auto Thread = Frame->Thread;
      
      LogMan::Throw::AFmt(Thread->ThreadManager.GetTID() == gettid(), "Must be called from owning thread {}, not {}", Thread->ThreadManager.GetTID(), gettid());

      std::lock_guard lk(Thread->CTX->CodeInvalidationMutex);

      ThreadRemoveCodeEntry(Thread, GuestRIP);
    }

    // returns false if a handler was already registered
    CustomIRResult AddCustomIREntrypoint(uintptr_t Entrypoint, std::function<void(uintptr_t Entrypoint, FEXCore::IR::IREmitter *)> Handler, void *Creator, void *Data);

    void RemoveCustomIREntrypoint(uintptr_t Entrypoint);

    // Debugger interface
#if FIXME
    void CompileRIP(FEXCore::Core::InternalThreadState *Thread, uint64_t RIP);
    uint64_t GetThreadCount() const;
    FEXCore::Core::RuntimeStats *GetRuntimeStatsForThread(uint64_t Thread);
    bool GetDebugDataForRIP(uint64_t RIP, FEXCore::Core::DebugData *Data);
    bool FindHostCodeForRIP(uint64_t RIP, uint8_t **Code);
#endif

    struct GenerateIRResult {
      FEXCore::IR::IRListView *IRList;
      FEXCore::IR::RegisterAllocationData *RAData;
      uint64_t TotalInstructions;
      uint64_t TotalInstructionsLength;
      uint64_t StartAddr;
      uint64_t Length;
      std::vector<GuestCodeRange> Ranges;
    };
    [[nodiscard]] GenerateIRResult GenerateIR(FEXCore::Core::InternalThreadState *Thread, uint64_t GuestRIP,uint64_t MinAddr, uint64_t MaxAddr, bool ExtendedDebugInfo);

    static constexpr unsigned CODE_NONE = 0;
    static constexpr unsigned CODE_IR = 1;
    static constexpr unsigned CODE_OBJ = 2;

    struct CompileCodeResult {
      void* CompiledCode;
      const FEXCore::IR::IRListView *IRData;
      FEXCore::Core::DebugData *DebugData;
      const FEXCore::IR::RegisterAllocationData *RAData;
      unsigned GeneratedCode;
      uint64_t StartAddr;
      uint64_t Length;
      std::vector<GuestCodeRange> Ranges;
    };

    // Frame is fully synchronized
    void HandleGuestBreak(FEXCore::Core::CpuStateFrame *Frame);

    // Frame is fully synchronized
    void HandleGuestSyscall(FEXCore::Core::CpuStateFrame *Frame);

    // Aborts on failure
    void CompileBlockOrAbort(FEXCore::Core::CpuStateFrame *Frame, uint64_t GuestRIP);

    // Returns false on failure
    uintptr_t TryCompileBlock(FEXCore::Core::CpuStateFrame *Frame, uint64_t GuestRIP);

    // Used for thread creation from syscalls
    /**
     * @brief Used to create FEX thread objects in preparation for creating a true OS thread. Does set a TID or PID.
     *
     * @param NewThreadState The initial thread state to setup for our state
     * @param ParentTID The PID that was the parent thread that created this
     *
     * @return The InternalThreadState object that tracks all of the emulated thread's state
     *
     * Usecases:
     *  OS thread Creation:
     *    - Thread = CreateThread(NewState, PPID);
     *    - InitializeThread(Thread);
     *  OS fork (New thread created with a clone of thread state):
     *    - clone{2, 3}
     *    - Thread = CreateThread(CopyOfThreadState, PPID);
     *    - ExecutionThread(Thread); // Starts executing without creating another host thread
     *  Thunk callback executing guest code from native host thread
     *    - Thread = CreateThread(NewState, PPID);
     *    - InitializeThreadTLSData(Thread);
     *    - HandleCallback(Thread, RIP);
     */
    FEXCore::Core::InternalThreadState* CreateThread(FEXCore::Core::CPUState *NewThreadState, uint64_t ParentTID);

    /**
     * @brief Initializes TID, PID and TLS data for a thread
     *
     * @param Thread The internal FEX thread state object
     */
    void InitializeThreadTLSData(FEXCore::Core::InternalThreadState *Thread);

    /**
     * @brief Initializes the OS thread object and prepares to start executing on that new OS thread
     *
     * @param Thread The internal FEX thread state object
     *
     * The OS thread will wait until RunThread is executed
     */
    void InitializeThread(FEXCore::Core::InternalThreadState *Thread);

    /**
     * @brief Starts the OS thread object to start executing guest code
     *
     * @param Thread The internal FEX thread state object
     */
    void RunThread(FEXCore::Core::InternalThreadState *Thread);

    /**
     * @brief Destroys this FEX thread object and stops tracking it internally
     *
     * @param Thread The internal FEX thread state object
     */
    void DestroyThread(FEXCore::Core::InternalThreadState *Thread);
    void CopyMemoryMapping(FEXCore::Core::InternalThreadState *ParentThread, FEXCore::Core::InternalThreadState *ChildThread);

    void CleanupAfterFork(FEXCore::Core::InternalThreadState *ExceptForThread);

    std::vector<FEXCore::Core::InternalThreadState*>* GetThreads() { return &Threads; }

    uint8_t GetGPRSize() const { return Config.Is64BitMode ? 8 : 4; }

    FEXCore::Core::NamedRegion *LoadNamedRegion(const std::string &filename, const std::string& Fingerprint);
    FEXCore::Core::NamedRegion *ReloadNamedRegion(FEXCore::Core::NamedRegion *NamedRegion);
    void UnloadNamedRegion(FEXCore::Core::NamedRegion *Entry);

    FEXCore::JITSymbols Symbols;

    // Public for threading
    [[nodiscard]] int ExecutionThread(FEXCore::Core::InternalThreadState *Thread);

    void SetIRCacheOpener(CacheOpenerHandler CacheOpener) {
      IRCacheOpener = CacheOpener;
    }

    void SetObjCacheOpener(CacheOpenerHandler CacheOpener) {
      ObjCacheOpener = CacheOpener;
    }

    FEXCore::Utils::PooledAllocatorMMap OpDispatcherAllocator;
    FEXCore::Utils::PooledAllocatorMMap FrontendAllocator;

    bool MarkMemoryShared();

    bool IsTSOEnabled() { return Config.TSOEnabled; }

    void InvalidateGuestCodeRange(uint64_t Start, uint64_t Length);
    void InvalidateGuestCodeRange(uint64_t Start, uint64_t Length, std::function<void(uint64_t start, uint64_t Length)> CallAfter);

  protected:
    void ClearCodeCache(FEXCore::Core::InternalThreadState *Thread);

  private:
    [[nodiscard]] uintptr_t CompileBlockInternal(FEXCore::Core::CpuStateFrame *Frame, uint64_t GuestRIP);
    [[nodiscard]] CompileCodeResult CompileCodeHelper(FEXCore::Core::InternalThreadState *Thread, uint64_t GuestRIP);

    /**
     * @brief Does some final thread initialization
     *
     * @param Thread The internal FEX thread state object
     *
     * InitCore and CreateThread both call this to finish up thread object initialization
     */
    void InitializeThreadData(FEXCore::Core::InternalThreadState *Thread);

    /**
     * @brief Initializes the JIT compilers for the thread
     *
     * @param State The internal FEX thread state object
     *
     * InitializeCompiler is called inside of CreateThread, so you likely don't need this
     */
    void InitializeCompiler(FEXCore::Core::InternalThreadState* Thread);
#if 0
    void WaitForIdleWithTimeout();

    void NotifyPause();
#endif
    void AddBlockMapping(FEXCore::Core::InternalThreadState *Thread, uint64_t Address, void *Ptr);

    // Entry Cache
    std::mutex ExitMutex;
    //std::unique_ptr<GdbServer> DebugServer;

    CacheOpenerHandler IRCacheOpener;
    CacheOpenerHandler ObjCacheOpener;
    
    //std::unique_ptr<FEXCore::CodeSerialize::CodeObjectSerializeService> CodeObjectCacheService;

    bool StartPaused = false;
    bool IsMemoryShared = false;
    FEX_CONFIG_OPT(AppFilename, APP_FILENAME);

    std::shared_mutex CustomIRMutex;
    std::unordered_map<uint64_t, std::tuple<std::function<void(uintptr_t Entrypoint, FEXCore::IR::IREmitter *)>, void *, void *>> CustomIRHandlers;
    FEXCore::CPU::CPUBackendFeatures BackendFeatures;
    FEXCore::CPU::DispatcherConfig DispatcherConfig;
  };
}
