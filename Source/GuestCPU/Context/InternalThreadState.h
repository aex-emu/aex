#pragma once
#include "GuestCPU/Context/GuestState.h"
#include "GuestCPU/Translator/CodeGen/CPUBackend.h"
#include "GuestCPU/IR/IntrusiveIRList.h"
#include "GuestCPU/IR/RegisterAllocationData.h"
#include "Threading/Event.h"
#include "Threading/InterruptableConditionVariable.h"
#include "Threading/Threads.h"

#include <cstdint>
#include <map>
#include <unordered_map>
#include <shared_mutex>
#include <csetjmp>

#include "Common/ForwardDeclarations.h"

namespace FEXCore::Context {
  enum ExitReason {
    EXIT_NONE,
    EXIT_WAITING,
    //EXIT_ASYNC_RUN, // not actually used
    EXIT_SHUTDOWN,
    //EXIT_DEBUG, // not actually implemented
    //EXIT_UNKNOWNERROR,  // not actually used
  };
}

namespace FEXCore::Core {

  struct RuntimeStats {
    std::atomic_uint64_t InstructionsExecuted;
    std::atomic_uint64_t BlocksCompiled;
  };

  struct DebugDataSubblock {
    uint32_t HostCodeOffset;
    uint32_t HostCodeSize;
  };

  struct DebugDataGuestOpcode {
    uint64_t GuestEntryOffset;
    ptrdiff_t HostEntryOffset;
  };

  /**
   * @brief Contains debug data for a block of code for later debugger analysis
   *
   * Needs to remain around for as long as the code could be executed at least
   */
  struct DebugData {
    uint64_t HostCodeSize; ///< The size of the code generated in the host JIT
    std::vector<DebugDataSubblock> Subblocks;
    std::vector<DebugDataGuestOpcode> GuestOpcodes;
    const ObjCacheRelocations *Relocations;
    std::vector<uint8_t> RelocationsStorage; //only used to free the Relocations, if not mmap'd
  };

  enum class SignalEvent {
    Nothing, // If the guest uses our signal we need to know it was errant on our end
    Pause,
    Stop,
    Return,
  };

  struct DebugIREntry {
    uint64_t GuestRIP;
    uint64_t StartAddr;
    uint64_t Length;
    std::unique_ptr<const FEXCore::IR::IRListView> IR;
    std::unique_ptr<const FEXCore::IR::RegisterAllocationData> RAData;
    std::unique_ptr<const FEXCore::Core::DebugData> DebugData;
  };

  struct InternalThreadState {
    FEXCore::Core::CpuStateFrame* CurrentFrame;

    struct {
      std::atomic_bool Running {false};
      std::atomic_bool WaitingToStart {true};
      std::atomic_bool EarlyExit {false};
      std::atomic_bool ThreadSleeping {false};
    } RunningEvents;

    FEXCore::Context::Context *CTX;
    std::atomic<SignalEvent> SignalReason{SignalEvent::Nothing};

    std::unique_ptr<FEXCore::Threads::Thread> ExecutionThread;
    bool StartPaused {false};
    InterruptableConditionVariable StartRunning;
    Event ThreadWaiting;

    std::unique_ptr<FEXCore::IR::OpDispatchBuilder> OpDispatcher;

    std::unique_ptr<FEXCore::CPU::CPUBackend> CPUBackend;
    std::unique_ptr<FEXCore::LookupCache> LookupCache;

    std::unordered_map<uintptr_t, DebugIREntry> DebugStore;

    std::unique_ptr<FEXCore::Frontend::Decoder> FrontendDecoder;
    std::unique_ptr<FEXCore::IR::PassManager> PassManager;
    FEXCore::HLE::ThreadManagement ThreadManager;

    RuntimeStats Stats{};
    
    // TODO: This really doesn't belong here
    uint64_t InitialSignalMask;

    int StatusCode{};
    FEXCore::Context::ExitReason ExitReason {FEXCore::Context::ExitReason::EXIT_WAITING};

    bool DestroyedByParent{false};  // Should the parent destroy this thread, or it destroy itself

    uint64_t LastGuestObjCacheImport = 0;
    uint64_t LastGuestIRCacheImport = 0;
    
    jmp_buf ExitJump;
  };
  // static_assert(std::is_standard_layout<InternalThreadState>::value, "This needs to be standard layout");
}


