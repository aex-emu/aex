
#include "GuestCPU/Context/Context.h"
#include "GuestOS/Misc/MContext.h"
#include "GuestCPU/CodeDispatcher/Dispatcher.h"
#include "GuestOS/Misc/X86HelperGen.h"

#include "Config/Config.h"
#include "GuestCPU/Context/GuestState.h"
#include "SignalDelegator/SignalDelegator.h"
#include "GuestOS/Misc/UContext.h"
#include "GuestCPU/Context/X86Enums.h"
#include "GuestCPU/Context/InternalThreadState.h"
#include "Threading/Event.h"
#include "LogMgr/LogManager.h"
#include "Common/Utils.h"

#include <atomic>
#include <condition_variable>
#include <csignal>
#include <cstring>
#include <signal.h>

namespace FEXCore::CPU {

/*
  - Store host state from HostUContext to the (guest) stack
  - Alter frame to resume execution from the restore entrypoint 

  How can we validate we are in the guest stack? -> Frame var, maybe?
*/
void Dispatcher::GenerateThunkReentryFrame(FEXCore::Core::CpuStateFrame *Frame, void *HostUContext) {
  //auto ContextBackup = StoreThreadState(Thread, Signal, ucontext);
}
}
