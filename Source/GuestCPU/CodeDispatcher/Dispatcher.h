#pragma once

#include "GuestCPU/Translator/CodeGen/CPUBackend.h"
#include "GuestOS/Misc/MContext.h"

#include <cstdint>
#include <signal.h>
#include <stddef.h>
#include <stack>
#include <tuple>
#include <vector>

#include "Common/ForwardDeclarations.h"

namespace FEXCore::CPU {

struct DispatcherConfig {
  bool StaticRegisterAllocation = false;
};

enum DispatcherState {
  DS_JIT,       // In dispatcher, jit or helper code
  DS_BREAK,     // In dispatcher or guest break handler
  DS_SYSCALL,   // In dispatcher or a guest system call handler
  DS_THUNK      // In dispatcher or a host thunk
};

class Dispatcher {
public:
  virtual ~Dispatcher() = default;
  
  /**
   * @name Dispatch Helper functions
   * @{ */
  uint64_t ThreadStopHandlerAddress{};
  uint64_t ThreadStopHandlerAddressSpillSRA{};
  uint64_t AbsoluteLoopTopAddress{};
  uint64_t AbsoluteLoopTopAddressFillSRA{};
  uint64_t ExitFunctionLinkerAddress{};
  uint64_t DispatchBreakHandler{};
  uint64_t DispatchSyscallHandler{};
  uint64_t DispatchThunkHandler{};
  uint64_t IntCallbackReturnAddress{};

  uint64_t PauseReturnInstruction{};

  /**  @} */

  uint64_t Start{};
  uint64_t End{};

  bool IsAddressInDispatcher(uint64_t Address) const {
    return Address >= Start && Address < End;
  }

  virtual void InitThreadPointers(FEXCore::Core::InternalThreadState *Thread) = 0;

  // These are across all arches for now
  static constexpr size_t MaxGDBPauseCheckSize = 128;
  static constexpr size_t MaxInterpreterTrampolineSize = 128;

  virtual size_t GenerateGDBPauseCheck(uint8_t *CodeBuffer, uint64_t GuestRIP) = 0;
  virtual size_t GenerateInterpreterTrampoline(uint8_t *CodeBuffer) = 0;
  virtual void GenerateThunkReentryFrame(FEXCore::Core::CpuStateFrame *Frame, void *HostUContext);

  static std::unique_ptr<Dispatcher> CreateX86(FEXCore::Context::Context *CTX, const DispatcherConfig &Config);
  static std::unique_ptr<Dispatcher> CreateArm64(FEXCore::Context::Context *CTX, const DispatcherConfig &Config);
  
  void ExecuteDispatch(FEXCore::Core::CpuStateFrame *Frame) {
    DispatchPtr(Frame);
  }

  void ExecuteJITCallback(FEXCore::Core::CpuStateFrame *Frame, uint64_t RIP) {
    CallbackPtr(Frame, RIP);
  }

protected:
  Dispatcher(FEXCore::Context::Context *ctx, const DispatcherConfig &Config)
    : CTX {ctx}
    , config {Config}
    {}

  ArchHelpers::Context::ContextBackup* StoreThreadState(FEXCore::Core::InternalThreadState *Thread, int Signal, void *ucontext);
  void RestoreThreadState(FEXCore::Core::InternalThreadState *Thread, void *ucontext);
  std::stack<uint64_t, std::vector<uint64_t>> SignalFrames;

  virtual void SpillSRA(FEXCore::Core::InternalThreadState *Thread, void *ucontext, uint32_t IgnoreMask) {}

  FEXCore::Context::Context *CTX;
  DispatcherConfig config;

  using AsmDispatch = void(*)(FEXCore::Core::CpuStateFrame *Frame);
  using JITCallback = void(*)(FEXCore::Core::CpuStateFrame *Frame, uint64_t RIP);

  AsmDispatch DispatchPtr;
  JITCallback CallbackPtr;
};

}
