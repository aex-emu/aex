#include "Arm64Dispatcher.h"
#include "GuestCPU/LookupCache/LookupCache.h"

#include "GuestOS/Misc/MContext.h"
#include "GuestCPU/Translator/CodeGen/Interpreter/InterpreterClass.h"

#include "GuestCPU/Context/Context.h"
#include "GuestOS/Misc/X86HelperGen.h"

#include "GuestCPU/Translator/CodeGen/CPUBackend.h"
#include "GuestCPU/Context/GuestState.h"
#include "GuestCPU/Context/X86Enums.h"
#include "GuestCPU/Context/InternalThreadState.h"
#include "Common/HostSyscalls.h"

#include <array>
#include <bit>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <memory>

#include <aarch64/assembler-aarch64.h>
#include <aarch64/constants-aarch64.h>
#include <aarch64/cpu-aarch64.h>
#include <aarch64/operands-aarch64.h>
#include <code-buffer-vixl.h>
#include <platform-vixl.h>

#include <sys/syscall.h>
#include <unistd.h>

#define STATE_PTR(STATE_TYPE, FIELD) \
  MemOperand(STATE, offsetof(FEXCore::Core::STATE_TYPE, FIELD))

namespace FEXCore::CPU {

using namespace vixl;
using namespace vixl::aarch64;

static constexpr size_t MAX_DISPATCHER_CODE_SIZE = 4096;
#define STATE x28

// This needs to be benchmarked, and maybe become a tunable
void Arm64Dispatcher::GenerateSignalCheck(int xreg) {
  Register Tmp(xreg, 64);

  //sub(Tmp, STATE, 1);
  //dc(aarch64::CVAC, Tmp);
}

Arm64Dispatcher::Arm64Dispatcher(FEXCore::Context::Context *ctx, const DispatcherConfig &config)
  : FEXCore::CPU::Dispatcher(ctx, config), Arm64Emitter(ctx, MAX_DISPATCHER_CODE_SIZE) {
  SetAllowAssembler(true);

  DispatchPtr = GetCursorAddress<AsmDispatch>();

  // while (true) {
  //    Ptr = FindBlock(RIP)
  //    if (!Ptr)
  //      Ptr = CTX->CompileBlock(RIP);
  //
  //    Ptr();
  // }

  Literal l_CTX {reinterpret_cast<uintptr_t>(CTX)};

  // Push all the register we need to save
  PushCalleeSavedRegisters();

  // Push our memory base to the correct register
  // Move our thread pointer to the correct register
  // This is passed in to parameter 0 (x0)
  mov(STATE, x0);

  // Save this stack pointer so we can cleanly shutdown the emulation with a long jump
  // regardless of where we were in the stack
  add(x0, sp, 0);
  str(x0, STATE_PTR(CpuStateFrame, ReturningStackLocation));

  AbsoluteLoopTopAddressFillSRA = GetCursorAddress<uint64_t>();

  if (config.StaticRegisterAllocation) {
    FillStaticRegs();
  }

  // We want to ensure that we are 16 byte aligned at the top of this loop
  Align16B();
  aarch64::Label FullLookup{};
  aarch64::Label CallBlock{};
  aarch64::Label LoopTop{};
  aarch64::Label ExitSpillSRA{};

  bind(&LoopTop);
  AbsoluteLoopTopAddress = GetLabelAddress<uint64_t>(&LoopTop);

  // Load in our RIP
  // Don't modify x2 since it contains our RIP once the block doesn't exist
  ldr(x2, STATE_PTR(CpuStateFrame, State.rip));
  auto RipReg = x2;

  // L1 Cache
  ldr(x0, STATE_PTR(CpuStateFrame, Pointers.Common.L1Pointer));

  and_(x3, RipReg, LookupCache::L1_ENTRIES_MASK);
  add(x0, x0, Operand(x3, Shift::LSL, 4));
  ldp(x3, x0, MemOperand(x0));
  cmp(x0, RipReg);
  b(&FullLookup, Condition::ne);

  br(x3);

  // L1C check failed, do a full lookup
  bind(&FullLookup);

  // This is the block cache lookup routine
  // It matches what is going on it LookupCache.h::FindBlock
  ldr(x0, STATE_PTR(CpuStateFrame, Pointers.Common.L2Pointer));

  // Mask the address by the virtual address size so we can check for aliases
  uint64_t VirtualMemorySize = CTX->Config.VirtualMemSize;
  if (std::popcount(VirtualMemorySize) == 1) {
    and_(x3, RipReg, VirtualMemorySize - 1);
  }
  else {
    LoadConstant(x3, VirtualMemorySize);
    and_(x3, RipReg, x3);
  }

  aarch64::Label NoBlock;
  {
    // Offset the address and add to our page pointer
    lsr(x1, x3, 12);

    // Load the pointer from the offset
    ldr(x0, MemOperand(x0, x1, Shift::LSL, 3));

    // If page pointer is zero then we have no block
    cbz(x0, &NoBlock);

    // Steal the page offset
    and_(x1, x3, 0x0FFF);

    // Shift the offset by the size of the block cache entry
    add(x0, x0, Operand(x1, Shift::LSL, (int)log2(sizeof(FEXCore::LookupCache::LookupCacheEntry))));

    // Load the guest address first to ensure it maps to the address we are currently at
    // This fixes aliasing problems
    ldr(x1, MemOperand(x0, offsetof(FEXCore::LookupCache::LookupCacheEntry, GuestCode)));
    cmp(x1, RipReg);
    b(&NoBlock, Condition::ne);

    // Now load the actual host block to execute if we can
    ldr(x3, MemOperand(x0, offsetof(FEXCore::LookupCache::LookupCacheEntry, HostCode)));
    cbz(x3, &NoBlock);

    // If we've made it here then we have a real compiled block
    {
      // update L1 cache
      ldr(x0, STATE_PTR(CpuStateFrame, Pointers.Common.L1Pointer));

      and_(x1, RipReg, LookupCache::L1_ENTRIES_MASK);
      add(x0, x0, Operand(x1, Shift::LSL, 4));
      stp(x3, x2, MemOperand(x0));

      // Jump to the block
      br(x3);
    }
  }

  {
    bind(&ExitSpillSRA);
    ThreadStopHandlerAddressSpillSRA = GetCursorAddress<uint64_t>();
    if (config.StaticRegisterAllocation)
      SpillStaticRegs();

    ThreadStopHandlerAddress = GetCursorAddress<uint64_t>();

    PopCalleeSavedRegisters();

    // Return from the function
    // LR is set to the correct return location now
    ret();
  }

  {   
    ExitFunctionLinkerAddress = GetCursorAddress<uint64_t>();
    if (config.StaticRegisterAllocation)
      SpillStaticRegs();

    mov(x0, STATE);
    mov(x1, lr);

    ldr(x3, STATE_PTR(CpuStateFrame, Pointers.Common.ExitFunctionLink));
    blr(x3);

    if (config.StaticRegisterAllocation)
      FillStaticRegs();
    br(x0);
  }

  // Need to create the block
  {
    bind(&NoBlock);

    if (config.StaticRegisterAllocation)
      SpillStaticRegs();

    ldr(x0, &l_CTX);
    mov(x1, STATE);
    ldr(x3, STATE_PTR(CpuStateFrame, Pointers.Common.TranslateGuestCode));

    // X2 contains our guest RIP
    blr(x3); // { CTX, Frame, RIP}

    if (config.StaticRegisterAllocation)
      FillStaticRegs();

    b(&LoopTop);
  }

  {
    
    /*
      Guest break handler

      - Spill SRA
      - Disable Signals
      - Set mode to DS_BREAK
      - Deliver any pending GDS (may cause exit)

      - call HandleGuestBreak

      - Set mode to DS_JIT
      - Fill SRA
      - Dispatch next pc
    */
    DispatchBreakHandler = GetCursorAddress<uint64_t>();

    if (config.StaticRegisterAllocation)
      SpillStaticRegs();

    // Disable signals
    
    // X0: SETMASK
    // X1: Pointer to mask value (uint64_t)
    // X2: Pointer to old mask value (uint64_t)
    // X3: Size of mask, sizeof(uint64_t)
    // X8: Syscall

    // TODO: Redo this, using a literal pool, ldp(x0, x8, ...); adr(x1, ...) mov(x2, 0) svc(0)
    // Store mask to SP  
    LoadConstant(x0, ~(1 << (SIGSEGV - 1)));
    str(x0, MemOperand(sp, -8, PreIndex));

    LoadConstant(x0, SIG_SETMASK);
    add(x1, sp, 0);
    mov(x2, 0);
    mov(x3, 8);
    LoadConstant(x8, SYS_rt_sigprocmask);
    svc(0);
    add(sp, sp, 8);

    // A break is getting handled
    mov(x0, DS_BREAK);
    str(x0, STATE_PTR(CpuStateFrame, DispatcherState));

    // If there was any signal generated before this, deliver it now
    // The context is in a state that can be re-executed to generate the break
    GenerateSignalCheck(0);

    ldr(x0, &l_CTX);
    mov(x1, STATE);
    ldr(x2, STATE_PTR(CpuStateFrame, Pointers.Common.HandleGuestBreak));

    blr(x2); // { CTX, Frame }

    // HandleGuestBreak is responsible for unblocking signals
    // Any signals from here will get deferred to the GDS check

    // Normal jit execution resumes
    mov(x0, DS_JIT);
    str(x0, STATE_PTR(CpuStateFrame, DispatcherState));

    if (config.StaticRegisterAllocation)
      FillStaticRegs();

    b(&LoopTop);
  }

  {
    /*
      Guest syscall handler

      - Spill SRA

      - call HandleGuestSyscall, GDS check is taken care through the HostSyscall helpers

      - Fill SRA
      - Dispatch next pc
    */
    DispatchSyscallHandler = GetCursorAddress<uint64_t>();

    if (config.StaticRegisterAllocation)
      SpillStaticRegs();


    ldr(x0, &l_CTX);
    mov(x1, STATE);
    ldr(x2, STATE_PTR(CpuStateFrame, Pointers.Common.HandleGuestSyscall));

    blr(x2); // { CTX, Frame }

    if (config.StaticRegisterAllocation)
      FillStaticRegs();

    b(&LoopTop);
  }

  {
    /*
      Guest thunk handler

      enters in the guest pivoted stack

      - Set mode to DS_THUNK
      - Deliver any pending GDS (will generate a thunk re-entry frame)
      - Spill SRA (not strictly required - only caller saved regs)

      - Call the thunk host code

      - Set mode to DS_JIT
      - Fill SRA (see above note)
      - Pop RIP
      - Pivot to host stack
      - Dispatch next pc
    */
    DispatchThunkHandler = GetCursorAddress<uint64_t>();

    // Set Dispatcher state to thunk
    // Best to not use x0 here, as that hold the thunk arg
    mov(x3, DS_THUNK);
    str(x3, STATE_PTR(CpuStateFrame, DispatcherState));

    // Make sure there are no pending signals - if so handle them here
    // What about storing PC?
    GenerateSignalCheck(3);


    if (config.StaticRegisterAllocation)
      SpillStaticRegs();

    // x0, x1 have args
    // x2 is thunk host code ptr
    blr(x2);


    // Switch to JIT signal handling
    // Must be done before RIP gets pop'ed
    mov(x0, DS_JIT);
    str(x0, STATE_PTR(CpuStateFrame, DispatcherState));

    // Pop return address to x1
    if (config.StaticRegisterAllocation) {
      FillStaticRegs();
      if (CTX->Config.Is64BitMode) {
        ldr(x1, MemOperand(SRA64[X86State::REG_RSP]));
        add(SRA64[X86State::REG_RSP], SRA64[X86State::REG_RSP], 8);
      } else {
        ldr(w1, MemOperand(SRA64[X86State::REG_RSP]));
        add(SRA64[X86State::REG_RSP].W(), SRA64[X86State::REG_RSP].W(), 4);
      }
    } else {
      if (CTX->Config.Is64BitMode) {
        ldr(x0, STATE_PTR(CpuStateFrame, State.gregs[X86State::REG_RSP]));
        ldr(x1, MemOperand(x0));
        add(x2, x0, 8);
        str(x2, STATE_PTR(CpuStateFrame, State.gregs[X86State::REG_RSP]));
      } else {
        ldr(w0, STATE_PTR(CpuStateFrame, State.gregs[X86State::REG_RSP]));
        ldr(w1, MemOperand(x0));
        add(w2, w0, 4);
        str(x2, STATE_PTR(CpuStateFrame, State.gregs[X86State::REG_RSP]));
      }
    }

    // Store return to RIP
    str(x1, STATE_PTR(CpuStateFrame, State.rip));

    // Pivot back to host stack
    ldr(x0, STATE_PTR(CpuStateFrame, ReturningStackLocation));
    add(sp, x0, 0);
    
    // Dispatch
    b(&LoopTop);
  }

  {
    // The expectation here is that a thunked function needs to call back in to the JIT in a reentrant safe way
    // To do this safely we need to do some state tracking and register saving
    //
    // eg:
    // JIT Call->
    //  Thunk->
    //    Thunk callback->
    //
    // The thunk callback needs to execute JIT code and when it returns, it needs to safely return to the thunk rather than JIT space
    // This is handled by pushing a return address trampoline to the stack so when the guest address returns it hits our custom thunk return
    //  - This will safely return us to the thunk
    //
    // On return to the thunk, the thunk can get whatever its return value is from the thread context depending on ABI handling on its end
    // When the thunk itself returns, it'll do its regular return logic there
    // void ReentrantCallback(FEXCore::Core::InternalThreadState *Thread, uint64_t RIP);
    CallbackPtr = GetCursorAddress<JITCallback>();

    // TODO: Fix this, brk is here to break things if used
    // Also depends on CallbackReturn opcode (BranchOps.cpp)
    brk(19);

    // This should be running in the guest stack here
    
    // Push any regs
    PushCalleeSavedRegisters();

    // Move the thread state pointer back to the state reg
    mov(STATE, x0);

    // Store RIP to the context state
    str(x1, STATE_PTR(CpuStateFrame, State.rip));

    // Push the callback return trampoline to the guest stack
    // Guest will be misaligned because calling a thunk won't correct the guest's stack once we call the callback from the host
    {
      LoadConstant(x0, CTX->X86CodeGen.CallbackReturn);

      ldr(x2, STATE_PTR(CpuStateFrame, State.gregs[X86State::REG_RSP]));
      sub(x2, x2, 16);
      str(x2, STATE_PTR(CpuStateFrame, State.gregs[X86State::REG_RSP]));

      // Store the trampoline to the guest stack
      str(x0, MemOperand(x2));
    }

    // load static regs
    if (config.StaticRegisterAllocation)
      FillStaticRegs();

    // Pivot to the host stack
    {
      ldr(x2, STATE_PTR(CpuStateFrame, ReturningStackLocation));

      MemOperand PairOffset(x2, -16, PreIndex);
      // FIXME: THIS IS BROKEN
      // stp(sp, sp, PairOffset);
      brk(0);

      mov(sp, x2);
    }
    
    // Now go back to the regular dispatcher loop
    b(&LoopTop);
  }

  {
    LUDIVHandlerAddress = GetCursorAddress<uint64_t>();

    PushDynamicRegsAndLR();

    ldr(x3, STATE_PTR(CpuStateFrame, Pointers.AArch64.LUDIV));

    SpillStaticRegs();
    blr(x3);
    FillStaticRegs();

    // Result is now in x0
    // Fix the stack and any values that were stepped on
    PopDynamicRegsAndLR();

    // Go back to our code block
    ret();
  }

  {
    LDIVHandlerAddress = GetCursorAddress<uint64_t>();

    PushDynamicRegsAndLR();

    ldr(x3, STATE_PTR(CpuStateFrame, Pointers.AArch64.LDIV));

    SpillStaticRegs();
    blr(x3);
    FillStaticRegs();

    // Result is now in x0
    // Fix the stack and any values that were stepped on
    PopDynamicRegsAndLR();

    // Go back to our code block
    ret();
  }

  {
    LUREMHandlerAddress = GetCursorAddress<uint64_t>();

    PushDynamicRegsAndLR();

    ldr(x3, STATE_PTR(CpuStateFrame, Pointers.AArch64.LUREM));

    SpillStaticRegs();
    blr(x3);
    FillStaticRegs();

    // Result is now in x0
    // Fix the stack and any values that were stepped on
    PopDynamicRegsAndLR();

    // Go back to our code block
    ret();
  }

  {
    LREMHandlerAddress = GetCursorAddress<uint64_t>();

    PushDynamicRegsAndLR();

    ldr(x3, STATE_PTR(CpuStateFrame, Pointers.AArch64.LREM));

    SpillStaticRegs();
    blr(x3);
    FillStaticRegs();

    // Result is now in x0
    // Fix the stack and any values that were stepped on
    PopDynamicRegsAndLR();

    // Go back to our code block
    ret();
  }

  place(&l_CTX);

  FinalizeCode();
  Start = reinterpret_cast<uint64_t>(DispatchPtr);
  End = GetCursorAddress<uint64_t>();
  vixl::aarch64::CPU::EnsureIAndDCacheCoherency(reinterpret_cast<void*>(DispatchPtr), End - reinterpret_cast<uint64_t>(DispatchPtr));
  GetBuffer()->SetExecutable();

  if (CTX->Config.BlockJITNaming()) {
    std::string Name = "Dispatch_" + std::to_string(FHU::Syscalls::gettid());
    CTX->Symbols.Register(reinterpret_cast<void*>(DispatchPtr), End - reinterpret_cast<uint64_t>(DispatchPtr), Name);
  }
  if (CTX->Config.GlobalJITNaming()) {
    CTX->Symbols.RegisterJITSpace(reinterpret_cast<void*>(DispatchPtr), End - reinterpret_cast<uint64_t>(DispatchPtr));
  }
}

// Used by GenerateGDBPauseCheck, GenerateInterpreterTrampoline, destination buffer is set before use
static thread_local vixl::aarch64::Assembler emit((uint8_t*)&emit, 1);

size_t Arm64Dispatcher::GenerateGDBPauseCheck(uint8_t *CodeBuffer, uint64_t GuestRIP) {

  ERROR_AND_DIE_FMT("Unsupported");

  return 0;
}

size_t Arm64Dispatcher::GenerateInterpreterTrampoline(uint8_t *CodeBuffer) {
  LOGMAN_THROW_AA_FMT(!config.StaticRegisterAllocation, "GenerateInterpreterTrampoline dispatcher does not support SRA");
  
  *emit.GetBuffer() = vixl::CodeBuffer(CodeBuffer, MaxInterpreterTrampolineSize);

  vixl::CodeBufferCheckScope scope(&emit, MaxInterpreterTrampolineSize, vixl::CodeBufferCheckScope::kDontReserveBufferSpace, vixl::CodeBufferCheckScope::kNoAssert);

  aarch64::Label InlineIRData;

  emit.mov(x0, STATE);
  emit.adr(x1, &InlineIRData);

  emit.ldr(x3, STATE_PTR(CpuStateFrame, Pointers.Interpreter.FragmentExecuter));
  emit.blr(x3);

  emit.ldr(x0, STATE_PTR(CpuStateFrame, Pointers.Common.DispatcherLoopTop));
  emit.br(x0);

  emit.bind(&InlineIRData);

  emit.FinalizeCode();

  auto UsedBytes = emit.GetBuffer()->GetCursorOffset();
  vixl::aarch64::CPU::EnsureIAndDCacheCoherency(CodeBuffer, UsedBytes);
  return UsedBytes;
}

void Arm64Dispatcher::SpillSRA(FEXCore::Core::InternalThreadState *Thread, void *ucontext, uint32_t IgnoreMask) {
  for (size_t i = 0; i < SRA64.size(); i++) {
    if (IgnoreMask & (1U << SRA64[i].GetCode())) {
      // Skip this one, it's already spilled
      continue;
    }
    Thread->CurrentFrame->State.gregs[i] = ArchHelpers::Context::GetArmReg(ucontext, SRA64[i].GetCode());
  }

  if (EmitterCTX->HostFeatures.SupportsAVX) {
    for (size_t i = 0; i < SRAFPR.size(); i++) {
      auto FPR = ArchHelpers::Context::GetArmFPR(ucontext, SRAFPR[i].GetCode());
      memcpy(&Thread->CurrentFrame->State.xmm.avx.data[i][0], &FPR, sizeof(__uint128_t));
    }
  } else {
    for (size_t i = 0; i < SRAFPR.size(); i++) {
      auto FPR = ArchHelpers::Context::GetArmFPR(ucontext, SRAFPR[i].GetCode());
      memcpy(&Thread->CurrentFrame->State.xmm.sse.data[i][0], &FPR, sizeof(__uint128_t));
    }
  }
}

void Arm64Dispatcher::InitThreadPointers(FEXCore::Core::InternalThreadState *Thread) {
// Setup dispatcher specific pointers that need to be accessed from JIT code
  {
    auto &Common = Thread->CurrentFrame->Pointers.Common;

    Common.DispatcherLoopTop = AbsoluteLoopTopAddress;
    Common.DispatcherLoopTopFillSRA = AbsoluteLoopTopAddressFillSRA;
    Common.ExitFunctionLinker = ExitFunctionLinkerAddress;
    Common.DispatchSyscallHandler = DispatchSyscallHandler;
    Common.DispatchBreakHandler = DispatchBreakHandler;
    Common.DispatchThunkHandler = DispatchThunkHandler;

    auto &AArch64 = Thread->CurrentFrame->Pointers.AArch64;
    AArch64.LUDIVHandler = LUDIVHandlerAddress;
    AArch64.LDIVHandler = LDIVHandlerAddress;
    AArch64.LUREMHandler = LUREMHandlerAddress;
    AArch64.LREMHandler = LREMHandlerAddress;
  }
}

std::unique_ptr<Dispatcher> Dispatcher::CreateArm64(FEXCore::Context::Context *CTX, const DispatcherConfig &Config) {
  return std::make_unique<Arm64Dispatcher>(CTX, Config);
}

}
