#pragma once

#include <cstdint>
#include <vector>
#include "Common/CompilerDefs.h"

namespace FEXCore::Allocator {
  using MMAP_Hook = void*(*)(void*, size_t, int, int, int, off_t);
  using MUNMAP_Hook = int(*)(void*, size_t);

  using MALLOC_Hook = void*(*)(size_t);
  using REALLOC_Hook = void*(*)(void*, size_t);
  using FREE_Hook = void(*)(void*);

  FEX_DEFAULT_VISIBILITY extern MMAP_Hook hostfirst_mmap;
  FEX_DEFAULT_VISIBILITY extern MUNMAP_Hook hostfirst_munmap;
  FEX_DEFAULT_VISIBILITY extern MALLOC_Hook hostfirst_malloc;
  FEX_DEFAULT_VISIBILITY extern REALLOC_Hook hostfirst_realloc;
  FEX_DEFAULT_VISIBILITY extern FREE_Hook hostfirst_free;

  FEX_DEFAULT_VISIBILITY size_t DetermineVASize();

  struct MemoryRegion {
    void *Ptr;
    size_t Size;
  };

  FEX_DEFAULT_VISIBILITY void Init(uintptr_t GuestMemEnd);
  FEX_DEFAULT_VISIBILITY std::vector<MemoryRegion> ReserveMemoryRange(uintptr_t Begin, uintptr_t End);
  FEX_DEFAULT_VISIBILITY void ReleaseMemoryRegions(const std::vector<MemoryRegion> &Regions);
}
