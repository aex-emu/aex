#include "Allocator/HostAllocator.h"
#include "Allocator/Allocator.h"
#include "Common/CompilerDefs.h"
#include "LogMgr/LogManager.h"
#include "Common/HostSyscalls.h"
#include "Common/Utils.h"

#include <array>
#include <asm-generic/errno-base.h>
#include <cctype>
#include <cstdio>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/user.h>
#ifdef ENABLE_JEMALLOC
#include <jemalloc/jemalloc.h>
#endif
#include <errno.h>
#include <memory>
#include <stddef.h>
#include <stdint.h>

namespace FEXCore::Allocator {
  template<bool hostfirst>
  static void *setup_alloc64_fns(void*, size_t, int, int, int, off_t);
}

extern "C" {
  typedef void* (*mmap_hook_type)(
            void *addr, size_t length, int prot, int flags,
            int fd, off_t offset);
  typedef int (*munmap_hook_type)(void *addr, size_t length);
  mmap_hook_type __mmap_hook = &FEXCore::Allocator::setup_alloc64_fns<false>;
  munmap_hook_type __munmap_hook;
}


namespace FEXCore::Allocator {

  static std::unique_ptr<Alloc::HostAllocator> Alloc64;

  static void *FEX_mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset) {
    
    LogMan::Throw::AFmt(!!Alloc64, "Alloc64 must be initialized here");

    void *Result = Alloc64->Mmap(addr, length, prot, flags, fd, offset);
    if (Result >= (void*)-4096) {
      errno = -(uint64_t)Result;
      return (void*)-1;
    }
    return Result;
  }

  static int FEX_munmap(void *addr, size_t length) {
    LogMan::Throw::AFmt(!!Alloc64, "Alloc64 must be initialized here");

    int Result = Alloc64->Munmap(addr, length);

    if (Result != 0) {
      errno = -Result;
      return -1;
    }
    return Result;
  }

  MMAP_Hook hostfirst_mmap = &setup_alloc64_fns<true>;
  MUNMAP_Hook hostfirst_munmap = nullptr;

  static void *setup_jealloc_fns(size_t size);


#ifdef ENABLE_JEMALLOC
  MALLOC_Hook hostfirst_malloc = &setup_jealloc_fns;

  REALLOC_Hook hostfirst_realloc = [](void *p, size_t size) { 
    if (!p) return hostfirst_malloc(size);
    ERROR_AND_DIE_FMT("hostfirst_realloc without hostfirst_malloc");
  };

  FREE_Hook hostfirst_free = [](void *p) { 
    if (!p) return;
    ERROR_AND_DIE_FMT("hostfirst_free without hostfirst_malloc");
  };
#else
  MALLOC_Hook hostfirst_malloc { ::malloc };
  REALLOC_Hook hostfirst_realloc { ::realloc };
  FREE_Hook hostfirst_free { ::free };
#endif

#ifdef ENABLE_JEMALLOC
  void *setup_jealloc_fns(size_t size) {
    hostfirst_malloc = ::je_malloc;
    hostfirst_realloc = ::je_realloc;
    hostfirst_free = ::je_free;

    return hostfirst_malloc(size);
  }

  template<bool hostfirst>
  void *setup_alloc64_fns(void* a, size_t b, int c, int d, int e, off_t f) {
    // for now, hostfirst mmap is in guestmem
    hostfirst_mmap = &FEX_mmap;
    hostfirst_munmap = &FEX_munmap;


    // for now, hostfirst heap is in guestmem
    // TODO: #2007
    #if 0
    __mmap_hook = &FEX_mmap;
    __munmap_hook = &FEX_munmap;
    #else
      __mmap_hook = &mmap;
      __munmap_hook = &munmap;
    #endif

    if (hostfirst) {
      return hostfirst_mmap(a, b, c, d, e, f);
    } else {
      return __mmap_hook(a, b, c, d, e, f);
    }
  }
  #endif

  FEX_DEFAULT_VISIBILITY size_t DetermineVASize() {
    static constexpr std::array<uintptr_t, 7> TLBSizes = {
      57,
      52,
      48,
      47,
      42,
      39,
      36,
    };

    for (auto Bits : TLBSizes) {
      uintptr_t Size = 1ULL << Bits;
      // Just try allocating
      // We can't actually determine VA size on ARM safely
      auto Find = [](uintptr_t Size) -> bool {
        for (int i = 0; i < 64; ++i) {
          // Try grabbing a some of the top pages of the range
          // x86 allocates some high pages in the top end
          void *Ptr = ::mmap(reinterpret_cast<void*>(Size - FHU::FEX_PAGE_SIZE * i), FHU::FEX_PAGE_SIZE, PROT_NONE, MAP_FIXED_NOREPLACE | MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
          if (Ptr != (void*)~0ULL) {
            ::munmap(Ptr, FHU::FEX_PAGE_SIZE);
            if (Ptr == (void*)(Size - FHU::FEX_PAGE_SIZE * i)) {
              return true;
            }
          }
        }
        return false;
      };

      if (Find(Size)) {
        return Bits;
      }
    }

    LOGMAN_MSG_A_FMT("Couldn't determine host VA size");
    FEX_UNREACHABLE;
  }

  #define RESERVE_LOG(...) // fprintf(stderr, __VA_ARGS__)

  std::vector<MemoryRegion> ReserveMemoryRange(uintptr_t Begin, uintptr_t End) {
    std::vector<MemoryRegion> Regions;
    
    int MapsFD = open("/proc/self/maps", O_RDONLY);
    LogMan::Throw::AFmt(MapsFD != -1, "Failed to open /proc/self/maps");

    enum {ParseBegin, ParseEnd, ScanEnd} State = ParseBegin;

    uintptr_t RegionBegin = 0;
    uintptr_t RegionEnd = 0;

    char Buffer[2048];
    const char *Cursor;
    ssize_t Remaining = 0;

    for(;;) {

      if (Remaining == 0) {
        do { 
          Remaining = read(MapsFD, Buffer, sizeof(Buffer));
        } while ( Remaining == -1 && errno == EAGAIN);

        Cursor = Buffer;
      }

      if (Remaining == 0 && State == ParseBegin) {
        RESERVE_LOG("[%d] EndOfFile; RegionBegin: %016lX RegionEnd: %016lX\n", __LINE__, RegionBegin, RegionEnd);

        auto MapBegin = std::max(RegionEnd, Begin);
        auto MapEnd = End;

        RESERVE_LOG("     MapBegin: %016lX MapEnd: %016lX\n", MapBegin, MapEnd);

        if (MapEnd > MapBegin) {
          RESERVE_LOG("     Reserving\n");

          auto MapSize = MapEnd - MapBegin;
          auto Alloc = mmap((void*)MapBegin, MapSize, PROT_NONE, MAP_ANONYMOUS | MAP_NORESERVE | MAP_PRIVATE | MAP_FIXED_NOREPLACE, -1, 0);

          LogMan::Throw::AFmt(Alloc != MAP_FAILED, "mmap({:x},{:x}) failed", MapBegin, MapSize);
          LogMan::Throw::AFmt(Alloc == (void*)MapBegin, "mmap({},{:x}) returned {} instead of {:x}", Alloc, MapBegin);

          Regions.push_back({(void*)MapBegin, MapSize});
        }

        close(MapsFD);
        return Regions;
      }

      LogMan::Throw::AFmt(Remaining > 0, "Failed to parse /proc/self/maps");

      auto c = *Cursor++;
      Remaining--;

      if (State == ScanEnd) {
        if (c == '\n') {
          State = ParseBegin;
        }
        continue;
      }

      if (State == ParseBegin) {
        if (c == '-') {
          RESERVE_LOG("[%d] ParseBegin; RegionBegin: %016lX RegionEnd: %016lX\n", __LINE__, RegionBegin, RegionEnd);

          auto MapBegin = std::max(RegionEnd, Begin);
          auto MapEnd = std::min(RegionBegin, End);
          
          RESERVE_LOG("     MapBegin: %016lX MapEnd: %016lX\n", MapBegin, MapEnd);

          if (MapEnd > MapBegin) {
            RESERVE_LOG("     Reserving\n");

            auto MapSize = MapEnd - MapBegin;
            auto Alloc = mmap((void*)MapBegin, MapSize, PROT_NONE, MAP_ANONYMOUS | MAP_NORESERVE | MAP_PRIVATE | MAP_FIXED_NOREPLACE, -1, 0);

            LogMan::Throw::AFmt(Alloc != MAP_FAILED, "mmap({:x},{:x}) failed", MapBegin, MapSize);
            LogMan::Throw::AFmt(Alloc == (void*)MapBegin, "mmap({},{:x}) returned {} instead of {:x}", Alloc, MapBegin);

            Regions.push_back({(void*)MapBegin, MapSize});
          }
          RegionBegin = 0;
          RegionEnd = 0;
          State = ParseEnd;
          continue;
        } else {
          LogMan::Throw::AFmt(std::isalpha(c) || std::isdigit(c), "Unexpected char '{}' in ParseBegin", c);
          RegionBegin = (RegionBegin << 4) | (c <= '9' ? (c - '0') : (c - 'a' + 10));
        }
      }

      if (State == ParseEnd) {
        if (c == ' ') {
          RESERVE_LOG("[%d] ParseEnd; RegionBegin: %016lX RegionEnd: %016lX\n", __LINE__, RegionBegin, RegionEnd);

          State = ScanEnd;
          continue;
        } else {
          LogMan::Throw::AFmt(std::isalpha(c) || std::isdigit(c), "Unexpected char '{}' in ParseEnd", c);
          RegionEnd = (RegionEnd << 4) | (c <= '9' ? (c - '0') : (c - 'a' + 10));
        }
      }
    }

    ERROR_AND_DIE_FMT("unreachable");
  }

  void ReleaseMemoryRegions(const std::vector<MemoryRegion> &Regions) {
    for (const auto &Region: Regions) {
      ::munmap(Region.Ptr, Region.Size);
    }
  }

  void Init(uintptr_t GuestMemEnd) {
    LogMan::Throw::AFmt(!Alloc64, "Alloc64 must not be initialized here");
    
    size_t Bits = FEXCore::Allocator::DetermineVASize();
    uintptr_t HostMemEnd = 1ULL << Bits;

    #if _M_X86_64 // Last page cannot be allocated on x86
      HostMemEnd -= FHU::FEX_PAGE_SIZE;
    #endif

    auto Ranges = ReserveMemoryRange(GuestMemEnd, HostMemEnd);
    Alloc64 = Alloc::OSAllocator::Create64BitAllocator(Ranges, GuestMemEnd, HostMemEnd);
  }
}