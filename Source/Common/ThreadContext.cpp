
#include "Common/ThreadContext.h"
#include "GuestCPU/Context/InternalThreadState.h"

#include "GuestOS/LinuxKernel/Guest/GuestLinuxTypes.h"

static thread_local FEXCore::Core::InternalThreadState *CurrentThread;

// not static, needs to be accessible by the assembler for HostSyscalls
thread_local FEXCore::Core::CpuStateFrame *CurrentFrame;

static thread_local GuestOSLinuxTaskState CurrentLinuxTaskState;

void ThreadBind(FEXCore::Core::InternalThreadState *NewInternalThreadState) {

    if (NewInternalThreadState) {
        CurrentFrame = NewInternalThreadState->CurrentFrame;
        CurrentThread = NewInternalThreadState;
    } else {
        CurrentFrame = nullptr;
        CurrentThread = nullptr;
    }
}

template<>
__attribute__((visibility("default"))) FEXCore::Core::CpuStateFrame *GetThreadBound() { return CurrentFrame; }

template<>
__attribute__((visibility("default"))) FEXCore::Core::InternalThreadState *GetThreadBound() { return CurrentThread; }

template<>
__attribute__((visibility("default"))) FEXCore::Context::Context *GetThreadBound() { return CurrentThread->CTX; }

template<>
__attribute__((visibility("default"))) GuestOSLinuxTaskState *GetThreadBound()  { return &CurrentLinuxTaskState; }