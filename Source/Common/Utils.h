#pragma once

// Header for various utilities that operate on bits and bytes.

#include <bit>
#include <climits>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <type_traits>

namespace FEXCore {

// Determines the number of bits inside of a given type.
template <typename T>
[[nodiscard]] constexpr size_t BitSize() noexcept {
    return sizeof(T) * CHAR_BIT;
}

// Swaps the bytes of a 16-bit unsigned value.
[[nodiscard]] inline uint16_t BSwap16(uint16_t value) noexcept {
#ifdef __GNUC__
    return __builtin_bswap16(value);
#else
    return (value >> 8) | (value << 8);
#endif
}

// Swaps the bytes of a 32-bit unsigned value.
[[nodiscard]] inline uint32_t BSwap32(uint32_t value) noexcept {
#ifdef __GNUC__
    return __builtin_bswap32(value);
#else
    return ((value & 0xFF000000U) >> 24) | ((value & 0x00FF0000U) >> 8) |
           ((value & 0x0000FF00U) << 8) | ((value & 0x000000FFU) << 24);
#endif
}

// Swaps the bytes of a 64-bit unsigned value.
[[nodiscard]] inline uint64_t BSwap64(uint64_t value) noexcept {
#ifdef __GNUC__
    return __builtin_bswap64(value);
#else
    return ((value & 0xFF00000000000000ULL) >> 56) | ((value & 0x00FF000000000000ULL) >> 40) |
           ((value & 0x0000FF0000000000ULL) >> 24) | ((value & 0x000000FF00000000ULL) >> 8) |
           ((value & 0x00000000FF000000ULL) << 8) | ((value & 0x0000000000FF0000ULL) << 24) |
           ((value & 0x000000000000FF00ULL) << 40) | ((value & 0x00000000000000FFULL) << 56);
#endif
}

// Finds the first least-significant set bit within a given value.
// Note that all returned indices are 1-based, not 0-based.
template <typename T>
[[nodiscard]] constexpr int FindFirstSetBit(T value) noexcept {
    static_assert(std::is_unsigned_v<T>, "Type must be unsigned.");

    if (value == 0) {
        return 0;
    }

    const int trailing_zeroes = std::countr_zero(value);
    return trailing_zeroes + 1;
}

// Stand-in for std::bit_cast until libc++ implements it.
template <typename To, typename From>
[[nodiscard]] inline To BitCast(const From& source) noexcept
{
  static_assert(sizeof(From) == sizeof(To),
                "BitCast source and destination types must be equal in size.");
  static_assert(std::is_trivially_copyable_v<From>,
                "BitCast source type must be trivially copyable.");
  static_assert(std::is_trivially_copyable_v<To>,
                "BitCast destination type must be trivially copyable.");

  std::aligned_storage_t<sizeof(To), alignof(To)> storage;
  std::memcpy(&storage, &source, sizeof(storage));
  return reinterpret_cast<To&>(storage);
}

} // namespace FEXCore

namespace FEXCore {
[[nodiscard]] constexpr uint64_t AlignUp(uint64_t value, uint64_t size) {
  return value + (size - value % size) % size;
}

[[nodiscard]] constexpr uint64_t AlignDown(uint64_t value, uint64_t size) {
  return value - value % size;
}
} // namespace FEXCore



#include <string>

namespace FEXCore::StringUtils {
  // Trim the left side of the string of whitespace and new lines
  [[maybe_unused]] static std::string LeftTrim(std::string String, std::string TrimTokens = " \t\n\r") {
    size_t pos = std::string::npos;
    if ((pos = String.find_first_not_of(TrimTokens)) != std::string::npos) {
      String.erase(0, pos);
    }

    return String;
  }

  // Trim the right side of the string of whitespace and new lines
  [[maybe_unused]] static std::string RightTrim(std::string String, std::string TrimTokens = " \t\n\r") {
    size_t pos = std::string::npos;
    if ((pos = String.find_last_not_of(TrimTokens)) != std::string::npos) {
      String.erase(String.begin() + pos + 1, String.end());
    }

    return String;
  }

  // Trim both the left and right of the string of whitespace and new lines
  [[maybe_unused]] static std::string Trim(std::string String, std::string TrimTokens = " \t\n\r") {
    return RightTrim(LeftTrim(String, TrimTokens), TrimTokens);
  }
}

#include <cstdint>
#include <string>
#include <string_view>
#include <optional>

namespace FEXCore::StrConv {
  [[maybe_unused]] static bool Conv(std::string_view Value, bool *Result) {
    *Result = std::stoi(std::string(Value), nullptr, 0);
    return true;
  }

  [[maybe_unused]] static bool Conv(std::string_view Value, uint8_t *Result) {
    *Result = std::stoi(std::string(Value), nullptr, 0);
    return true;
  }

  [[maybe_unused]] static bool Conv(std::string_view Value, uint16_t *Result) {
    *Result = std::stoi(std::string(Value), nullptr, 0);
    return true;
  }

  [[maybe_unused]] static bool Conv(std::string_view Value, uint32_t *Result) {
    *Result = std::stoi(std::string(Value), nullptr, 0);
    return true;
  }

  [[maybe_unused]] static bool Conv(std::string_view Value, int32_t *Result) {
    *Result = std::stoi(std::string(Value), nullptr, 0);
    return true;
  }

  [[maybe_unused]] static bool Conv(std::string_view Value, uint64_t *Result) {
    *Result = std::stoull(std::string(Value), nullptr, 0);
    return true;
  }
  [[maybe_unused]] static bool Conv(std::string_view Value, std::string *Result) {
    *Result = Value;
    return true;
  }
  template <typename T,
    typename = std::enable_if<std::is_enum<T>::value, T>>
  [[maybe_unused]] static bool Conv(std::string_view Value, T *Result) {
    *Result = static_cast<T>(std::stoull(std::string(Value), nullptr, 0));
    return true;
  }

}

// TODO: This should be somewhere else
#include <cstddef>

namespace FHU {
  // FEX assumes an operating page size of 4096
  // To work around build systems that build on a 16k/64k page size, define our page size here
  // Don't use the system provided PAGE_SIZE define because of this.
  constexpr size_t FEX_PAGE_SIZE = 4096;
  constexpr size_t FEX_PAGE_SHIFT = 12;
  constexpr size_t FEX_PAGE_MASK = ~(FEX_PAGE_SIZE - 1);
}



#include "LogMgr/LogManager.h"

namespace FEXCore::Utils {

/**
 * @brief Casts a class's member function pointer to a raw pointer that we can JIT
 *
 * Has additional validation to ensure we aren't casting a class member that is invalid
 */
template <typename PointerToMemberType>
class MemberFunctionToPointerCast final {
  public:
    MemberFunctionToPointerCast(PointerToMemberType Function) {
      memcpy(&PMF, &Function, sizeof(PMF));

#ifdef _M_X86_64
      // Itanium C++ ABI (https://itanium-cxx-abi.github.io/cxx-abi/abi.html#member-function-pointers)
      // Low bit of ptr specifies if this Member function pointer is virtual or not
      // Throw an assert if we were trying to cast a virtual member
      LOGMAN_THROW_AA_FMT((PMF.ptr & 1) == 0, "C++ Pointer-To-Member representation didn't have low bit set to 0. Are you trying to cast a virtual member?");
#elif defined(_M_ARM_64 )
      // C++ ABI for the Arm 64-bit Architecture (IHI 0059E)
      // 4.2.1 Representation of pointer to member function
      // Differs from Itanium specification
      LOGMAN_THROW_AA_FMT(PMF.adj == 0, "C++ Pointer-To-Member representation didn't have adj == 0. Are you trying to cast a virtual member?");
#else
#error Don't know how to cast Member to function here. Likely just Itanium
#endif
    }

    uintptr_t GetConvertedPointer() const {
      return PMF.ptr;
    }

  private:
    struct PointerToMember {
      uintptr_t ptr;
      uintptr_t adj;
    };

    PointerToMember PMF;

    // Ensure the representation of PointerToMember matches
    static_assert(sizeof(PMF) == sizeof(PointerToMemberType));
};
}

// FEX_TODO_ISSUE(github ticket number, "comment")
#define FEX_TODO_ISSUE(github_ticket, comment)
// FEX_TODO("comment")
#define FEX_TODO(comment)

// For linking to tickets, non-todo
// FEX_TICKET(github ticket number) or FEX_TICKET(github ticket number, "comment")
#define FEX_TICKET(github_ticket, ...)