#pragma once
#include "Common/ForwardDeclarations.h"

void ThreadBind(FEXCore::Core::InternalThreadState *NewInternalThreadState);

template<typename T>
T *GetThreadBound() = delete;

template<>
FEXCore::Core::CpuStateFrame *GetThreadBound();

template<>
FEXCore::Core::InternalThreadState *GetThreadBound();

template<>
FEXCore::Context::Context *GetThreadBound();

template<>
GuestOSLinuxTaskState *GetThreadBound();