#pragma once

namespace FEXCore {
  class CodeCache;
  class CodeLoader;
  class ThunkHandler;
  class GdbServer;
  class LookupCache;
  
  struct GuestSigAction;
  struct ObjCacheFragment;
  struct ObjCacheRelocations;
  struct GuestCodeRange;

  namespace Allocator {
    struct MemoryRegion;
  }

  namespace Core {
    struct InternalThreadState;
    struct CPUState;
    struct CpuStateFrame;
    struct NamedRegion;

    struct NamedRegion;
    struct DebugData;
  }

  namespace Context {
    struct Context;
  }

  namespace CodeSerialize {
    class CodeObjectSerializeService;
  }

  namespace CPU {
    class Arm64JITCore;
    class X86JITCore;
    class InterpreterCore;
    class Dispatcher;
    class CPUBackend;
    class X86DispatchGenerator;
    class Arm64DispatchGenerator;

    struct DispatcherConfig;

    union Relocation;
  }

  namespace HLE {
    struct SyscallArguments;
    class LinuxSyscallHandlerBase;
    class SourcecodeResolver;
    struct SourcecodeMap;
  }

  namespace IR {
    class IRListView;
    class OrderedNode;
    class OpDispatchBuilder;
    class RegisterAllocationPass;
    class RegisterAllocationData;
    class Pass;
    class PassManager;
    class IRListView;
    class IREmitter;

    struct RegisterClassType;
    struct SHA256Sum;

    namespace Validation {
      class IRValidation;
    }

    // TODO: Move this somewhere else
    /**
     * @brief The IROp_Header is an dynamically sized array
     * At the end it contains a uint8_t for the number of arguments that Op has
     * Then there is an unsized array of NodeWrapper arguments for the number of arguments this op has
     * The op structures that are including the header must ensure that they pad themselves correctly to the number of arguments used
     */
    struct IROp_Header;
  }

  namespace X86Tables {
    struct X86InstInfo;
  }

  namespace x86 {
    struct siginfo_t;
  }

  namespace Frontend {
    class Decoder;
  }

  namespace Utils {
    class IntrusivePooledAllocator;
  }
}

namespace FEX {
  namespace HLE {
    class SyscallHandler;
    class SignalDelegator;
    
    struct open_how;
    struct ExecveAtArgs;

    namespace x64 {
      struct guest_stat;
    }
  }
}

struct GuestOSLinuxTaskState;