#pragma once

#include <cstdint>
#include <signal.h>

#include "Common/CompilerDefs.h"

namespace FEXCore {
  struct FEX_PACKED GuestSAMask {
    uint64_t Val;
  };

  struct FEX_PACKED GuestSigAction {
    union {
      void (*handler)(int);
      void (*sigaction)(int, siginfo_t *, void*);
    } sigaction_handler;

    uint64_t sa_flags;
    void (*restorer)(void);
    GuestSAMask sa_mask;
  };
}

struct GuestOSLinuxTaskState {
  // This is per task rather than per signal
  void *HostAltStackPtr;
  // This is per task rather than per signal
  stack_t GuestAltStack;

  // This is the task's current signal mask
  FEXCore::GuestSAMask CurrentSignalMask;
  // The mask prior to a suspend
  FEXCore::GuestSAMask PreviousSuspendMask;

  uint64_t PendingSignals;
};