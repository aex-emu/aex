#pragma once

#include "GuestOS/LinuxKernel/Host/HostSyscalls.h"


#include <linux/aio_abi.h>
#include <linux/capability.h>
#include <linux/types.h>
#include <poll.h>

#include "GuestOS/LinuxKernel/Meta/x86_32/SyscallsEnum.h"
#include "GuestOS/LinuxKernel/Meta/x86_64/SyscallsEnum.h"


#include "GuestOS/LinuxKernel/Types.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Types.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Types.h"

using GuestSyscallHandlerFunctionType = GuestSyscallResult(uint64_t a0, uint64_t a1, uint64_t a2, uint64_t a3, uint64_t a4, uint64_t a5);

namespace x86_64 {
  using namespace FEX::HLE;

  template<int GuestSyscallNumber>
  struct GuestSyscallHandler;

  #define DECL_SYSCALL_GUEST_x86_64(name) \
    template<> struct GuestSyscallHandler<SYSCALL_x86_64_##name>

  #include "Generated/SyscallGen-x86_64/SyscallTypes.inl"

  #undef DECL_SYSCALL_GUEST_x86_64
}

namespace x86_32 {
  using namespace FEX::HLE;
  using namespace FEX::HLE::x32;

  template<typename T>
  using compat_ptr = FEX::HLE::x32::compat_ptr<T>;

  template<int GuestSyscallNumber>
  struct GuestSyscallHandler;

  #define DECL_SYSCALL_GUEST_x86_32(name) \
    template<> struct GuestSyscallHandler<SYSCALL_x86_32_##name>

  #include "Generated/SyscallGen-x86_32/SyscallTypes.inl"

  #undef DECL_SYSCALL_GUEST_x86_32
}

#define GUEST_SYSCALL_x86_64(name) GuestSyscallResult x86_64::GuestSyscallHandler<SYSCALL_x86_64_##name>::handle() const
#define GUEST_SYSCALL_x86_32(name) GuestSyscallResult x86_32::GuestSyscallHandler<SYSCALL_x86_32_##name>::handle() const

#define GUEST_SYSCALL GUEST_SYSCALL_x86_64