#pragma once
#include <cstdint>

#if 0
struct GuestSyscallResult {
  uint64_t ValueOrErrno;

  bool IsComplete() {
    return ValueOrErrno != UINT64_MAX - 4095;
  }

  bool IsSuccessful() {
    return ValueOrErrno < (UINT64_MAX - 4095);
  }

  uint64_t Value() {
    return ValueOrErrno;
  }
};
#else
#define GuestSyscallResult uint64_t
#endif

// Augment host syscalls with incomplete marking (min errno value)
template<int HostSyscallNumber>
GuestSyscallResult HostSyscall(...);


#ifdef _M_X86_64
#include "GuestOS/LinuxKernel/Meta/x86_64/SyscallsEnum.h"
#define SYSCALL_ARCH_NAME x64
#elif _M_ARM_64
#include "GuestOS/LinuxKernel/Meta/Arm64/SyscallsEnum.h"
#define SYSCALL_ARCH_NAME Arm64
#endif


#define CONCAT_(a, b) a ## b
#define CONCAT(a, b) CONCAT_(a, b)
#define SYSCALL_DEF(name) ( SYSCALL_ARCH_NAME::CONCAT(CONCAT(SYSCALL_, SYSCALL_ARCH_NAME), _##name))

#define HOST_SYSCALL(SyscallName, ...) HostSyscall<SYSCALL_DEF(SyscallName)>(__VA_ARGS__)