#include "HostSyscalls.h"
#include "GuestCPU/Context/GuestState.h"

#define SignalNumberOffset 1144

// Make sure SignalNumber is where and as it is expected to be
static_assert(offsetof(FEXCore::Core::CpuStateFrame, DeferredSignal.SignalNumber) == SignalNumberOffset);
static_assert(sizeof(FEXCore::Core::CpuStateFrame::DeferredSignal.SignalNumber) == 4);

#define STRINGIFY_HELPER(x) #x
#define STRINGIFY(x) STRINGIFY_HELPER(x)

// Yes, this is particularly fun, it generates asm stuff with mangled C++ template names
// and also depends on TLS specifics
// .global <label>
// label:

#define DECL_SYSCALL(num) \
  ".balign 32 \n" \
  ".global _Z11HostSyscallILi" #num "EEmz\n" \
  "_Z11HostSyscallILi" #num "EEmz: \n" \
  "mrs x8, TPIDR_EL0 \n" \
  "add x8, x8, :tprel_hi12:CurrentFrame  \n" \
  "add x8, x8, :tprel_lo12_nc:CurrentFrame \n" \
  "ldr x8, [x8]  \n" \
  "ldr w8, [x8, #" STRINGIFY(SignalNumberOffset) "]  \n" \
  "cbnz w8, syscall_bail \n" \
  "mov x8, " #num "\n" \
  "svc 0 \n" \
  "ret \n" \
  "nop \n"



__asm__(
  // As seen in clang output
  // a -> no meaning (used to mean allocate)
  // x -> executable
  // @progbits -> ?
  ".section host_syscalls, \"ax\", @progbits \n"

  #include "SyscallDecls.inl"

  "syscall_bail: \n"
  "mov x0, #0xFFFFFFFFFFFFF000 \n"
  "ret \n"
);