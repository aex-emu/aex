/*
$info$
tags: LinuxSyscalls|common
desc: Handles host -> host and host -> guest signal routing, emulates procmask & co
$end_info$
*/

#include "GuestCPU/Context/Context.h"
#include "GuestCPU/Context/InternalThreadState.h"
#include"GuestOS/LinuxKernel/Helpers/SignalDelegator.h"

#include "GuestCPU/Context/GuestState.h"
#include "SignalDelegator/SignalDelegator.h"
#include "GuestCPU/Context/X86Enums.h"
#include "GuestOS/Misc/UContext.h"
#include "Common/Utils.h"

#include "GuestOS/Misc/ThreadManagement.h"
#include "Allocator/Allocator.h"
#include "LogMgr/LogManager.h"
#include "Common/HostSyscalls.h"
#include "Config/Config.h"

#include "Common/ThreadContext.h"

#include <atomic>
#include <cstdint>
#include <string.h>

#include <errno.h>
#include <exception>
#include <functional>
#include <linux/futex.h>
#include <signal.h>
#include <syscall.h>
#include <sys/mman.h>
#include <sys/signalfd.h>
#include <unistd.h>
#include <utility>

#include "Common/ThreadContext.h"

// For older build environments
#ifndef SS_AUTODISARM
#define SS_AUTODISARM (1U << 31)
#endif

using namespace FEXCore;

namespace FEX::HLE {
#ifdef _M_X86_64
  __attribute__((naked))
  static void sigrestore() {
    __asm volatile("syscall;"
        :: "a" (0xF)
        : "memory");
  }
#endif

  constexpr static uint32_t X86_MINSIGSTKSZ  = 0x2000U;

  // We can only have one delegator per process
  static SignalDelegator *GlobalDelegator{};


  static void SignalHandlerThunk(int Signal, siginfo_t *Info, void *UContext) {
    GlobalDelegator->HandleSignal(Signal, Info, UContext);
  }

  uint64_t SigIsMember(FEXCore::GuestSAMask *Set, int Signal) {
    // Signal 0 isn't real, so everything is offset by one inside the set
    Signal -= 1;
    return (Set->Val >> Signal) & 1;
  }

  uint64_t SetSignal(FEXCore::GuestSAMask *Set, int Signal) {
    // Signal 0 isn't real, so everything is offset by one inside the set
    Signal -= 1;
    return Set->Val | (1ULL << Signal);
  }

  static uint32_t ConvertSignalToError(int Signal, siginfo_t *HostSigInfo) {
    switch (Signal) {
      case SIGSEGV:
        if (HostSigInfo->si_code == SEGV_MAPERR ||
            HostSigInfo->si_code == SEGV_ACCERR) {
          // Protection fault
          // Always a user fault for us
          // XXX: PF_PROT and PF_WRITE
          return X86State::X86_PF_USER;
        }
        break;
    }

    // Not a page fault issue
    return 0;
  }

  static uint32_t ConvertSignalToTrapNo(int Signal, siginfo_t *HostSigInfo) {
    switch (Signal) {
      case SIGSEGV:
        if (HostSigInfo->si_code == SEGV_MAPERR ||
            HostSigInfo->si_code == SEGV_ACCERR) {
          // Protection fault
          return X86State::X86_TRAPNO_PF;
        }
        break;
    }

    // Unknown mapping, fall back to old behaviour and just pass signal
    return Signal;
  }

  template <typename T>
  static void SetXStateInfo(T* xstate, bool is_avx_enabled) {
    auto* fpstate = &xstate->fpstate;

    fpstate->sw_reserved.magic1 = x86_64::fpx_sw_bytes::FP_XSTATE_MAGIC;
    fpstate->sw_reserved.extended_size = is_avx_enabled ? sizeof(T) : 0;

    fpstate->sw_reserved.xfeatures |= x86_64::fpx_sw_bytes::FEATURE_FP |
                                      x86_64::fpx_sw_bytes::FEATURE_SSE;
    if (is_avx_enabled) {
      fpstate->sw_reserved.xfeatures |= x86_64::fpx_sw_bytes::FEATURE_YMM;
    }

    fpstate->sw_reserved.xstate_size = fpstate->sw_reserved.extended_size;

    if (is_avx_enabled) {
      xstate->xstate_hdr.xfeatures = 0;
    }
  }

  // TODO: This should not be here
  //FEX_CONFIG_OPT(Is64BitMode, IS64BIT_MODE);

  // TODO: Add AVX
  void SignalDelegator::RestoreSigFrame32(FEXCore::Core::CpuStateFrame *Frame, bool IsRTFrame) {
    auto LinuxTaskState = GetThreadBound<GuestOSLinuxTaskState>();

    auto GuestSP = (uintptr_t)(uint32_t)Frame->State.gregs[X86State::REG_RSP];

    auto UContextPtr = IsRTFrame ? (GuestSP + 4 + 4) : (GuestSP + 4);

    auto *guest_uctx = reinterpret_cast<FEXCore::x86::ucontext_t*>(*(uint32_t*)UContextPtr);

    // XXX: Full context setting
    // First 32-bytes of flags is EFLAGS broken out
    uint32_t eflags = guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_EFL];
    for (size_t i = 0; i < 32; ++i) {
      Frame->State.flags[i] = (eflags & (1U << i)) ? 1 : 0;
    }

    Frame->State.flags[1] = 1;
    Frame->State.flags[9] = 1;

    Frame->State.rip = guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_EIP];
    Frame->State.cs = guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_CS];
    Frame->State.ds = guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_DS];
    Frame->State.es = guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_ES];
    Frame->State.fs = guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_FS];
    Frame->State.gs = guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_GS];
    Frame->State.ss = guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_SS];
    
    #define COPY_REG(x) Frame->State.gregs[X86State::REG_##x] = guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_##x];
    COPY_REG(RDI);
    COPY_REG(RSI);
    COPY_REG(RBP);
    COPY_REG(RBX);
    COPY_REG(RDX);
    COPY_REG(RAX);
    COPY_REG(RCX);
    COPY_REG(RSP);
    #undef COPY_REG

    FEXCore::x86::_libc_fpstate *fpstate = reinterpret_cast<FEXCore::x86::_libc_fpstate*>(guest_uctx->uc_mcontext.fpregs);

    // Copy float registers
    for (size_t i = 0; i < 8; ++i) {
      // 32-bit st register size is only 10 bytes. Not padded to 16byte like x86-64
      memcpy(&Frame->State.mm[i], &fpstate->_st[i], 10);
    }

    // Extended XMM state
    memcpy(&Frame->State.xmm.sse, fpstate->_xmm, sizeof(Frame->State.xmm.sse));
    //TODO: Handle AVX here?

    // FCW store defaults
    Frame->State.FCW = fpstate->fcw;
    Frame->State.FTW = fpstate->ftw;

    // Deconstruct FSW
    Frame->State.flags[FEXCore::X86State::X87FLAG_C0_LOC] = (fpstate->fsw >> 8) & 1;
    Frame->State.flags[FEXCore::X86State::X87FLAG_C1_LOC] = (fpstate->fsw >> 9) & 1;
    Frame->State.flags[FEXCore::X86State::X87FLAG_C2_LOC] = (fpstate->fsw >> 10) & 1;
    Frame->State.flags[FEXCore::X86State::X87FLAG_C3_LOC] = (fpstate->fsw >> 14) & 1;
    Frame->State.flags[FEXCore::X86State::X87FLAG_TOP_LOC] = (fpstate->fsw >> 11) & 0b111;

    // Set the new signal mask
    LinuxTaskState->CurrentSignalMask.Val = guest_uctx->uc_sigmask.value[0];
    SetSignalMask(LinuxTaskState->CurrentSignalMask.Val);
  }

  // TODO: Add AVX
  void SignalDelegator::RestoreSigFrame64(FEXCore::Core::CpuStateFrame *Frame) {
    auto LinuxTaskState = GetThreadBound<GuestOSLinuxTaskState>();

    auto *guest_uctx = reinterpret_cast<FEXCore::x86_64::ucontext_t*>(Frame->State.gregs[X86State::REG_RSP]);

    Frame->State.rip = guest_uctx->uc_mcontext.gregs[FEXCore::x86_64::FEX_REG_RIP];

    uint32_t eflags = guest_uctx->uc_mcontext.gregs[FEXCore::x86_64::FEX_REG_EFL];
    for (size_t i = 0; i < 32; ++i) {
      Frame->State.flags[i] = (eflags & (1U << i)) ? 1 : 0;
    }

    Frame->State.flags[1] = 1;
    Frame->State.flags[9] = 1;

    #define COPY_REG(x) \
      Frame->State.gregs[X86State::REG_##x] = guest_uctx->uc_mcontext.gregs[FEXCore::x86_64::FEX_REG_##x];
      COPY_REG(R8);
      COPY_REG(R9);
      COPY_REG(R10);
      COPY_REG(R11);
      COPY_REG(R12);
      COPY_REG(R13);
      COPY_REG(R14);
      COPY_REG(R15);
      COPY_REG(RDI);
      COPY_REG(RSI);
      COPY_REG(RBP);
      COPY_REG(RBX);
      COPY_REG(RDX);
      COPY_REG(RAX);
      COPY_REG(RCX);
      COPY_REG(RSP);
    #undef COPY_REG

    FEXCore::x86_64::_libc_fpstate *fpstate = reinterpret_cast<FEXCore::x86_64::_libc_fpstate*>(guest_uctx->uc_mcontext.fpregs);
    // Copy float registers
    memcpy(Frame->State.mm, fpstate->_st, sizeof(Frame->State.mm));
    memcpy(&Frame->State.xmm.sse, fpstate->_xmm, sizeof(Frame->State.xmm.sse));

    // FCW store default
    Frame->State.FCW = fpstate->fcw;
    Frame->State.FTW = fpstate->ftw;

    // Deconstruct FSW
    Frame->State.flags[FEXCore::X86State::X87FLAG_C0_LOC] = (fpstate->fsw >> 8) & 1;
    Frame->State.flags[FEXCore::X86State::X87FLAG_C1_LOC] = (fpstate->fsw >> 9) & 1;
    Frame->State.flags[FEXCore::X86State::X87FLAG_C2_LOC] = (fpstate->fsw >> 10) & 1;
    Frame->State.flags[FEXCore::X86State::X87FLAG_C3_LOC] = (fpstate->fsw >> 14) & 1;
    Frame->State.flags[FEXCore::X86State::X87FLAG_TOP_LOC] = (fpstate->fsw >> 11) & 0b111;

    // Set the new signal mask
    LinuxTaskState->CurrentSignalMask.Val = guest_uctx->uc_sigmask.value[0];
    SetSignalMask(LinuxTaskState->CurrentSignalMask.Val);
  }

  // TODO: Thread->CurrentFrame->SynchronousFaultData.FaultToTopAndGeneratedException
  void SignalDelegator::GenerateSigFrame(bool Is64BitMode, FEXCore::Core::CpuStateFrame *Frame, void *Info, FEXCore::GuestSigAction *GuestAction, stack_t *GuestStack) {
    auto LinuxTaskState = GetThreadBound<GuestOSLinuxTaskState>();

    uint64_t OldGuestSP = Frame->State.gregs[X86State::REG_RSP];
    uint64_t NewGuestSP = OldGuestSP;

    // Pulling from context here
    //const bool Is64BitMode = true; //Is64BitMode;
    const bool IsAVXEnabled = false;

    siginfo_t *HostSigInfo = reinterpret_cast<siginfo_t*>(Info);

    // TODO: Check that the context is written in the altstack
    // altstack is only used if the signal handler was setup with SA_ONSTACK
    if (GuestAction->sa_flags & SA_ONSTACK) {
      // Additionally the altstack is only used if the enabled (SS_DISABLE flag is not set)
      if (!(GuestStack->ss_flags & SS_DISABLE)) {
        // If our guest is already inside of the alternative stack
        // Then that means we are hitting recursive signals and we need to walk back the stack correctly
        uint64_t AltStackBase = reinterpret_cast<uint64_t>(GuestStack->ss_sp);
        uint64_t AltStackEnd = AltStackBase + GuestStack->ss_size;
        if (OldGuestSP >= AltStackBase &&
            OldGuestSP <= AltStackEnd) {
          // We are already in the alt stack, the rest of the code will handle adjusting this
        }
        else {
          NewGuestSP = AltStackEnd;
        }
      }
    }

    if (Is64BitMode) {
      // Back up past the redzone, which is 128bytes
      // 32-bit doesn't have a redzone
      NewGuestSP -= 128;
    }

    // Setup ucontext
    if (Is64BitMode) {
      if (IsAVXEnabled) {
        NewGuestSP -= sizeof(x86_64::xstate);
        NewGuestSP = AlignDown(NewGuestSP, alignof(x86_64::xstate));
      } else {
        NewGuestSP -= sizeof(x86_64::_libc_fpstate);
        NewGuestSP = AlignDown(NewGuestSP, alignof(x86_64::_libc_fpstate));
      }
      uint64_t FPStateLocation = NewGuestSP;

      NewGuestSP -= sizeof(siginfo_t);
      NewGuestSP = AlignDown(NewGuestSP, alignof(siginfo_t));
      uint64_t SigInfoLocation = NewGuestSP;

      NewGuestSP -= sizeof(FEXCore::x86_64::ucontext_t);
      NewGuestSP = AlignDown(NewGuestSP, alignof(FEXCore::x86_64::ucontext_t));
      uint64_t UContextLocation = NewGuestSP;

      FEXCore::x86_64::ucontext_t *guest_uctx = reinterpret_cast<FEXCore::x86_64::ucontext_t*>(UContextLocation);
      siginfo_t *guest_siginfo = reinterpret_cast<siginfo_t*>(SigInfoLocation);

      // We have extended float information
      guest_uctx->uc_flags = FEXCore::x86_64::UC_FP_XSTATE;

      // Pointer to where the fpreg memory is
      guest_uctx->uc_mcontext.fpregs = reinterpret_cast<x86_64::_libc_fpstate*>(FPStateLocation);
      auto *xstate = reinterpret_cast<x86_64::xstate*>(FPStateLocation);
      SetXStateInfo(xstate, IsAVXEnabled);

      guest_uctx->uc_mcontext.gregs[FEXCore::x86_64::FEX_REG_RIP] = Frame->State.rip;
      guest_uctx->uc_mcontext.gregs[FEXCore::x86_64::FEX_REG_EFL] = 0;
      guest_uctx->uc_mcontext.gregs[FEXCore::x86_64::FEX_REG_CSGSFS] = 0;

      // Store previous sigmask
      guest_uctx->uc_sigmask.value[0] = LinuxTaskState->CurrentSignalMask.Val;

      // TODO: siginfo #2018
      if (!HostSigInfo) {
        memset(guest_siginfo, 0, sizeof(*guest_siginfo));

        guest_uctx->uc_mcontext.gregs[FEXCore::x86_64::FEX_REG_TRAPNO] = Frame->SynchronousFaultData.TrapNo;
        guest_uctx->uc_mcontext.gregs[FEXCore::x86_64::FEX_REG_ERR] = Frame->SynchronousFaultData.err_code;

        guest_siginfo->si_code = Frame->SynchronousFaultData.si_code;
        guest_siginfo->si_signo = Frame->SynchronousFaultData.Signal;
      } else {
        // aarch64 and x86_64 siginfo_t matches. We can just copy this over
        // SI_USER could also potentially have random data in it, needs to be bit perfect
        // For guest faults we don't have a real way to reconstruct state to a real guest RIP

        *guest_siginfo = *HostSigInfo;
        guest_uctx->uc_mcontext.gregs[FEXCore::x86_64::FEX_REG_TRAPNO] = ConvertSignalToTrapNo(guest_siginfo->si_signo, HostSigInfo);
        guest_uctx->uc_mcontext.gregs[FEXCore::x86_64::FEX_REG_ERR] = ConvertSignalToError(guest_siginfo->si_signo, HostSigInfo);
      }

      guest_uctx->uc_mcontext.gregs[FEXCore::x86_64::FEX_REG_OLDMASK] = 0;
      guest_uctx->uc_mcontext.gregs[FEXCore::x86_64::FEX_REG_CR2] = 0;

      #define COPY_REG(x) guest_uctx->uc_mcontext.gregs[FEXCore::x86_64::FEX_REG_##x] = Frame->State.gregs[X86State::REG_##x];

      COPY_REG(R8);
      COPY_REG(R9);
      COPY_REG(R10);
      COPY_REG(R11);
      COPY_REG(R12);
      COPY_REG(R13);
      COPY_REG(R14);
      COPY_REG(R15);
      COPY_REG(RDI);
      COPY_REG(RSI);
      COPY_REG(RBP);
      COPY_REG(RBX);
      COPY_REG(RDX);
      COPY_REG(RAX);
      COPY_REG(RCX);
      COPY_REG(RSP);

      #undef COPY_REG

      auto* fpstate = &xstate->fpstate;

      // Copy float registers
      memcpy(fpstate->_st, Frame->State.mm, sizeof(Frame->State.mm));

      if (IsAVXEnabled) {
        for (size_t i = 0; i < Core::CPUState::NUM_XMMS; i++) {
          memcpy(&fpstate->_xmm[i], &Frame->State.xmm.avx.data[i][0], sizeof(__uint128_t));
        }
        for (size_t i = 0; i < Core::CPUState::NUM_XMMS; i++) {
          memcpy(&xstate->ymmh.ymmh_space[i], &Frame->State.xmm.avx.data[i][2], sizeof(__uint128_t));
        }
      } else {
        memcpy(fpstate->_xmm, Frame->State.xmm.sse.data, sizeof(Frame->State.xmm.sse.data));
      }

      // FCW store default
      fpstate->fcw = Frame->State.FCW;
      fpstate->ftw = Frame->State.FTW;

      // Reconstruct FSW
      fpstate->fsw =
        (Frame->State.flags[FEXCore::X86State::X87FLAG_TOP_LOC] << 11) |
        (Frame->State.flags[FEXCore::X86State::X87FLAG_C0_LOC] << 8) |
        (Frame->State.flags[FEXCore::X86State::X87FLAG_C1_LOC] << 9) |
        (Frame->State.flags[FEXCore::X86State::X87FLAG_C2_LOC] << 10) |
        (Frame->State.flags[FEXCore::X86State::X87FLAG_C3_LOC] << 14);

      // Copy over signal stack information
      // TODO: Fix this
      guest_uctx->uc_stack.ss_flags = GuestStack->ss_flags;
      guest_uctx->uc_stack.ss_sp = GuestStack->ss_sp;
      guest_uctx->uc_stack.ss_size = GuestStack->ss_size;

      Frame->State.gregs[X86State::REG_RDI] = guest_siginfo->si_signo;
      Frame->State.gregs[X86State::REG_RSI] = SigInfoLocation;
      Frame->State.gregs[X86State::REG_RDX] = UContextLocation;
    } else {
      if (IsAVXEnabled) {
        NewGuestSP -= sizeof(x86::xstate);
        NewGuestSP = AlignDown(NewGuestSP, alignof(x86::xstate));
      } else {
        NewGuestSP -= sizeof(x86::_libc_fpstate);
        NewGuestSP = AlignDown(NewGuestSP, alignof(x86::_libc_fpstate));
      }
      uint64_t FPStateLocation = NewGuestSP;

      NewGuestSP -= sizeof(FEXCore::x86::siginfo_t);
      NewGuestSP = AlignDown(NewGuestSP, alignof(FEXCore::x86::siginfo_t));
      uint64_t SigInfoLocation = NewGuestSP;

      NewGuestSP -= sizeof(FEXCore::x86::ucontext_t);
      NewGuestSP = AlignDown(NewGuestSP, alignof(FEXCore::x86::ucontext_t));
      uint64_t UContextLocation = NewGuestSP;

      FEXCore::x86::ucontext_t *guest_uctx = reinterpret_cast<FEXCore::x86::ucontext_t*>(UContextLocation);
      FEXCore::x86::siginfo_t *guest_siginfo = reinterpret_cast<FEXCore::x86::siginfo_t*>(SigInfoLocation);

      // We have extended float information
      guest_uctx->uc_flags = FEXCore::x86::UC_FP_XSTATE;

      // Pointer to where the fpreg memory is
      guest_uctx->uc_mcontext.fpregs = static_cast<uint32_t>(FPStateLocation);
      auto *xstate = reinterpret_cast<x86::xstate*>(FPStateLocation);
      SetXStateInfo(xstate, IsAVXEnabled);

      guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_GS] = Frame->State.gs;
      guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_FS] = Frame->State.fs;
      guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_ES] = Frame->State.es;
      guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_DS] = Frame->State.ds;

      // Store previous sigmask
      guest_uctx->uc_sigmask.value[0] = LinuxTaskState->CurrentSignalMask.Val;

      // TODO: #2018
      memset(guest_siginfo, 0, sizeof(*guest_siginfo));

      if (!HostSigInfo) {
        guest_siginfo->si_signo = Frame->SynchronousFaultData.Signal;
        guest_siginfo->si_code = Frame->SynchronousFaultData.si_code;

        guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_TRAPNO] = Frame->SynchronousFaultData.TrapNo;
        guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_ERR] = Frame->SynchronousFaultData.err_code;
      } else {
        guest_siginfo->si_signo = HostSigInfo->si_signo;
        guest_siginfo->si_errno = HostSigInfo->si_errno;
        guest_siginfo->si_code = HostSigInfo->si_code;

        switch (guest_siginfo->si_signo) {
          case SIGSEGV:
            // Macro expansion to get the si_addr
            // This is the address trying to be accessed, not the RIP
            guest_siginfo->_sifields._sigfault.addr = static_cast<uint32_t>(reinterpret_cast<uintptr_t>(HostSigInfo->si_addr));
            break;
          // TEST: SIGBUS is mem address or rip for _sigfault.addr ?
          case SIGBUS:
          case SIGFPE:
          case SIGILL:
            // Macro expansion to get the si_addr
            // Can't really give a real result here. Pull from the context for now
            guest_siginfo->_sifields._sigfault.addr = Frame->State.rip;
            break;
          case SIGCHLD:
            guest_siginfo->_sifields._sigchld.pid = HostSigInfo->si_pid;
            guest_siginfo->_sifields._sigchld.uid = HostSigInfo->si_uid;
            guest_siginfo->_sifields._sigchld.status = HostSigInfo->si_status;
            guest_siginfo->_sifields._sigchld.utime = HostSigInfo->si_utime;
            guest_siginfo->_sifields._sigchld.stime = HostSigInfo->si_stime;
            break;
          case SIGALRM:
          case SIGVTALRM:
            guest_siginfo->_sifields._timer.tid = HostSigInfo->si_timerid;
            guest_siginfo->_sifields._timer.overrun = HostSigInfo->si_overrun;
            guest_siginfo->_sifields._timer.sigval.sival_int = HostSigInfo->si_int;
            break;
          default:
            LogMan::Msg::EFmt("Unhandled siginfo_t for signal: {}\n", guest_siginfo->si_signo);
            break;
        }

        guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_TRAPNO] = ConvertSignalToTrapNo(guest_siginfo->si_signo, HostSigInfo);
        guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_ERR] = ConvertSignalToError(guest_siginfo->si_signo, HostSigInfo);
      }

      guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_EIP] = Frame->State.rip;
      guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_CS] = Frame->State.cs;
      guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_EFL] = 0;
      guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_UESP] = 0;
      guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_SS] = Frame->State.ss;

#define COPY_REG(x) \
      guest_uctx->uc_mcontext.gregs[FEXCore::x86::FEX_REG_##x] = Frame->State.gregs[X86State::REG_##x];
      COPY_REG(RDI);
      COPY_REG(RSI);
      COPY_REG(RBP);
      COPY_REG(RBX);
      COPY_REG(RDX);
      COPY_REG(RAX);
      COPY_REG(RCX);
      COPY_REG(RSP);
#undef COPY_REG

      auto *fpstate = &xstate->fpstate;

      // Copy float registers
      for (size_t i = 0; i < Core::CPUState::NUM_MMS; ++i) {
        // 32-bit st register size is only 10 bytes. Not padded to 16byte like x86-64
        memcpy(&fpstate->_st[i], &Frame->State.mm[i], 10);
      }

      // Extended XMM state
      fpstate->status = FEXCore::x86::fpstate_magic::MAGIC_XFPSTATE;
      if (IsAVXEnabled) {
        for (size_t i = 0; i < std::size(Frame->State.xmm.avx.data); i++) {
          memcpy(&fpstate->_xmm[i], &Frame->State.xmm.avx.data[i][0], sizeof(__uint128_t));
        }
        for (size_t i = 0; i < std::size(Frame->State.xmm.avx.data); i++) {
          memcpy(&xstate->ymmh.ymmh_space[i], &Frame->State.xmm.avx.data[i][2], sizeof(__uint128_t));
        }
      } else {
        memcpy(fpstate->_xmm, Frame->State.xmm.sse.data, sizeof(Frame->State.xmm.sse.data));
      }

      // FCW store default
      fpstate->fcw = Frame->State.FCW;
      fpstate->ftw = Frame->State.FTW;
      // Reconstruct FSW
      fpstate->fsw =
        (Frame->State.flags[FEXCore::X86State::X87FLAG_TOP_LOC] << 11) |
        (Frame->State.flags[FEXCore::X86State::X87FLAG_C0_LOC] << 8) |
        (Frame->State.flags[FEXCore::X86State::X87FLAG_C1_LOC] << 9) |
        (Frame->State.flags[FEXCore::X86State::X87FLAG_C2_LOC] << 10) |
        (Frame->State.flags[FEXCore::X86State::X87FLAG_C3_LOC] << 14);

      // Copy over signal stack information
      guest_uctx->uc_stack.ss_flags = GuestStack->ss_flags;
      guest_uctx->uc_stack.ss_sp = static_cast<uint32_t>(reinterpret_cast<uint64_t>(GuestStack->ss_sp));
      guest_uctx->uc_stack.ss_size = GuestStack->ss_size;

      NewGuestSP -= 4;
      *(uint32_t*)NewGuestSP = UContextLocation;
      NewGuestSP -= 4;
      *(uint32_t*)NewGuestSP = SigInfoLocation;
      NewGuestSP -= 4;
      *(uint32_t*)NewGuestSP = guest_siginfo->si_signo;
    }

    if (GuestAction->sa_flags & SA_SIGINFO) {
      Frame->State.rip = reinterpret_cast<uint64_t>(GuestAction->sigaction_handler.sigaction);
    } else {
      Frame->State.rip = reinterpret_cast<uint64_t>(GuestAction->sigaction_handler.handler);
    }

    if (Is64BitMode) {
      // Set up the new SP for stack handling
      NewGuestSP -= 8;
      *(uint64_t*)NewGuestSP = (uint64_t)GuestAction->restorer;
      Frame->State.gregs[FEXCore::X86State::REG_RSP] = NewGuestSP;
    } else {
      NewGuestSP -= 4;
      *(uint32_t*)NewGuestSP = (uint32_t)(uint64_t)GuestAction->restorer;
      //LOGMAN_THROW_AA_FMT(SignalReturn < 0x1'0000'0000ULL, "This needs to be below 4GB");
      Frame->State.gregs[FEXCore::X86State::REG_RSP] = NewGuestSP;
    }

    // The guest starts its signal frame with a zero initialized FPU
    // Set that up now. Little bit costly but it's a requirement
    // This state will be restored on rt_sigreturn
    memset(Frame->State.xmm.avx.data, 0, sizeof(Frame->State.xmm));
    memset(Frame->State.mm, 0, sizeof(Frame->State.mm));
    Frame->State.FCW = 0x37F;
    Frame->State.FTW = 0xFFFF;
  }

  uint64_t SignalDelegator::HandleGuestSignalOrFault(bool Is64BitMode, FEXCore::Core::CpuStateFrame *Frame, void *SigInfo) {
    auto LinuxTaskState = GetThreadBound<GuestOSLinuxTaskState>();

    auto Signal = SigInfo ? ((siginfo_t*)SigInfo)->si_signo : Frame->SynchronousFaultData.Signal;

    SignalHandler &Handler = HostHandlers[Signal];

    // Remove the pending signal
    LinuxTaskState->PendingSignals &= ~(1ULL << (Signal - 1));

    // We have an emulation thread pointer, we can now modify its state
    if (Handler.GuestAction.sigaction_handler.handler == SIG_DFL) {
      if (Handler.DefaultBehaviour == DEFAULT_TERM ||
          Handler.DefaultBehaviour == DEFAULT_COREDUMP) {
        // Let the signal fall through to the unhandled path
        // This way the parent process can know it died correctly
        signal(Signal, SIG_DFL);
        pthread_kill(pthread_self(), Signal);
        SetSignalMask(~(1 << (Signal -1)));
        ERROR_AND_DIE_FMT("pthread_kill should term or coredump here? signal: {}", Signal);
      } else {
        return LinuxTaskState->CurrentSignalMask.Val;
      }
    }
    else if (Handler.GuestAction.sigaction_handler.handler == SIG_IGN) {
      // Keep the thread's signal mask
      return LinuxTaskState->CurrentSignalMask.Val;
    } else {
      GenerateSigFrame(Is64BitMode, Frame, SigInfo, &Handler.GuestAction, &LinuxTaskState->GuestAltStack);
      // Set up a new mask based on this signals signal mask
      uint64_t NewMask = Handler.GuestAction.sa_mask.Val;

      // If NODEFER then the new signal mask includes this signal
      if (!(Handler.GuestAction.sa_flags & SA_NODEFER)) {
        NewMask |= (1ULL << (Signal - 1));
      }

      // Walk our required signals and stop masking them if requested
      for (size_t i = 0; i < MAX_SIGNALS; ++i) {
        if (HostHandlers[i + 1].Required.load(std::memory_order_relaxed)) {
          // Never mask our required signals
          NewMask &= ~(1ULL << i);
        }
      }

      LinuxTaskState->CurrentSignalMask.Val = NewMask;
      // return the new signal mask
      return NewMask;
    }
  }

  bool SignalDelegator::InstallHostThunk(int Signal) {
    SignalHandler &SignalHandler = HostHandlers[Signal];
    // If the host thunk is already installed for this, just return
    if (SignalHandler.Installed) {
      return false;
    }

    // Default flags for us
    SignalHandler.HostAction.sa_flags = SA_SIGINFO | SA_ONSTACK;

    bool Result = UpdateHostThunk(Signal);

    SignalHandler.Installed = Result;
    return Result;
  }

  bool SignalDelegator::UpdateHostThunk(int Signal) {
    SignalHandler &SignalHandler = HostHandlers[Signal];

    // SA_NOCLDSTOP and SA_NOCLDWAIT are only meaningful for SIGCHLD
    auto PassthroughFlags = SA_NOCLDSTOP | SA_NOCLDWAIT | SA_RESTART;
    auto NewFlags = SA_SIGINFO | SA_ONSTACK;
    NewFlags |= SignalHandler.GuestAction.sa_flags & PassthroughFlags;


#ifdef _M_X86_64
  #define SA_RESTORER 0x04000000
    NewFlags |=  SA_RESTORER;
#endif

    // TODO: Move this to HostAction Initialization
    {
      #ifdef _M_X86_64
        SignalHandler.HostAction.restorer = sigrestore;
      #endif

      SignalHandler.HostAction.sa_mask = UINT64_MAX;

      // SIGSEGV (code write tracking) and SIGBUS (atomics emulation) are disabled as well for most signals
      //
      // *Watch* these must not be triggered from host signal handlers *Watch*
      //
      // SignalHandler.HostAction.sa_mask &= ~(1ULL << (SIGSEGV - 1));
      // SignalHandler.HostAction.sa_mask &= ~(1ULL <<  (SIGBUS - 1));

      // A special exception is made, SIGSEGV remains enabled during SIGBUS handling for arm64 to handle atomic emulation + SMC cases
      #if defined(_M_ARM_64)
        if (Signal == SIGBUS) {
          SignalHandler.HostAction.sa_mask &= ~(1ULL << (SIGSEGV - 1));
        }
      #endif
    }

    auto HostActionHandler = (sighandler_t)&SignalHandlerThunk;

    // Check for SIG_IGN
    if (SignalHandler.GuestAction.sigaction_handler.handler == SIG_IGN &&
        HostHandlers[Signal].Required.load(std::memory_order_relaxed) == false) {
      // We are ignoring this signal on the guest
      // Which means we need to ignore it on the host as well
      HostActionHandler = SIG_IGN;
    }

    // Check for SIG_DFL
    if (SignalHandler.GuestAction.sigaction_handler.handler == SIG_DFL &&
        HostHandlers[Signal].Required.load(std::memory_order_relaxed) == false) {
      // Default handler on guest and default handler on host
      // With coredump and terminate then expect fireworks, but that is what the guest wants
      HostActionHandler = SIG_DFL;
    }

    if (HostActionHandler != SignalHandler.HostAction.handler || SignalHandler.HostAction.sa_flags != NewFlags) {
      SignalHandler.HostAction.handler = HostActionHandler;
      SignalHandler.HostAction.sa_flags = NewFlags;
      const int Result = ::syscall(SYS_rt_sigaction, Signal, &SignalHandler.HostAction, nullptr, 8);
      if (Result < 0) {
        // Signal 32 and 33 are consumed by glibc. We don't handle this atm
        LogMan::Msg::AFmt("Failed to install host signal thunk for signal {}: {}", Signal, strerror(errno));
        return false;
      }
    }

    return true;
  }

  void SignalDelegator::UninstallHostHandler(int Signal) {
    kernel_sigaction ksig;
    memset(&ksig, 0, sizeof(ksig));
    ksig.handler = SIG_DFL;

    ::syscall(SYS_rt_sigaction, Signal, &ksig, nullptr, 8);
  }

  SignalDelegator::SignalDelegator() {
    // Register this delegate
    LOGMAN_THROW_AA_FMT(!GlobalDelegator, "Can't register global delegator multiple times!");
    GlobalDelegator = this;
    // Signal zero isn't real
    HostHandlers[0].Installed = true;

    // We can't capture SIGKILL or SIGSTOP
    HostHandlers[SIGKILL].Installed = true;
    HostHandlers[SIGSTOP].Installed = true;

    // Most signals default to termination
    // These ones are slightly different
    static constexpr std::array<std::pair<int, SignalDelegator::DefaultBehaviour>, 14> SignalDefaultBehaviours = {{
      {SIGQUIT,   DEFAULT_COREDUMP},
      {SIGILL,    DEFAULT_COREDUMP},
      {SIGTRAP,   DEFAULT_COREDUMP},
      {SIGABRT,   DEFAULT_COREDUMP},
      {SIGBUS,    DEFAULT_COREDUMP},
      {SIGFPE,    DEFAULT_COREDUMP},
      {SIGSEGV,   DEFAULT_COREDUMP},
      {SIGCHLD,   DEFAULT_IGNORE},
      {SIGCONT,   DEFAULT_IGNORE},
      {SIGURG,    DEFAULT_IGNORE},
      {SIGXCPU,   DEFAULT_COREDUMP},
      {SIGXFSZ,   DEFAULT_COREDUMP},
      {SIGSYS,    DEFAULT_COREDUMP},
      {SIGWINCH,  DEFAULT_IGNORE},
    }};

    for (const auto &[Signal, Behaviour] : SignalDefaultBehaviours) {
      HostHandlers[Signal].DefaultBehaviour = Behaviour;
    }
  }

  SignalDelegator::~SignalDelegator() {
    GlobalDelegator = nullptr;
  }

  void SignalDelegator::RegisterFrontendTLSState(FEXCore::Core::InternalThreadState *Thread) {
    auto GuestTaskState = GetThreadBound<GuestOSLinuxTaskState>();

    // Initialize for this thread
    {
      GuestTaskState->HostAltStackPtr = FEXCore::Allocator::hostfirst_mmap(nullptr, SIGSTKSZ * 16, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
      LOGMAN_THROW_AA_FMT(!!GuestTaskState->HostAltStackPtr, "Couldn't allocate stack pointer");

      GuestTaskState->GuestAltStack = stack_t {
        .ss_sp = nullptr,
        .ss_flags = SS_DISABLE, // By default the guest alt stack is disabled
        .ss_size = 0,
      };

      // Initialize to inherited signal mask
      GuestTaskState->CurrentSignalMask.Val = Thread->InitialSignalMask;
      GuestTaskState->PreviousSuspendMask = { 0 };

      // No signals are pending
      GuestTaskState->PendingSignals = 0;
    }

    // Apply to kernel (XID shenanigans might be in play here)
    {
      {
        stack_t altstack {
          .ss_sp = GuestTaskState->HostAltStackPtr,
          .ss_flags = 0,
          .ss_size = static_cast<size_t>(SIGSTKSZ * 16)
        };

        const int Result = sigaltstack(&altstack, nullptr);
        if (Result == -1) {
          LogMan::Msg::EFmt("Failed to install alternative signal stack {}", strerror(errno));
        }
      }

      SetSignalMask(GuestTaskState->CurrentSignalMask.Val);
    }
  }

  void SignalDelegator::UninstallFrontendTLSState(FEXCore::Core::InternalThreadState *Thread) {
    FEXCore::Allocator::hostfirst_munmap(GetThreadBound<GuestOSLinuxTaskState>()->HostAltStackPtr, SIGSTKSZ * 16);
    GetThreadBound<GuestOSLinuxTaskState>()->HostAltStackPtr = nullptr;

    stack_t altstack {
      .ss_flags = SS_DISABLE
    };

    // Uninstall the alt stack
    const int Result = sigaltstack(&altstack, nullptr);
    if (Result == -1) {
      LogMan::Msg::EFmt("Failed to uninstall alternative signal stack {}", strerror(errno));
    }
  }

  void SignalDelegator::FrontendRegisterHostSignalHandler(int Signal, FEXCore::HostSignalDelegatorFunction Func, bool Required) {
    // Linux signal handlers are per-process rather than per thread
    // Multiple threads could be calling in to this
    std::lock_guard lk(HostDelegatorMutex);
    HostHandlers[Signal].Required = Required;
    InstallHostThunk(Signal);
  }

  void SignalDelegator::FrontendRegisterFrontendHostSignalHandler(int Signal, FEXCore::HostSignalDelegatorFunction Func, bool Required) {
    // Linux signal handlers are per-process rather than per thread
    // Multiple threads could be calling in to this
    std::lock_guard lk(HostDelegatorMutex);
    HostHandlers[Signal].Required = Required;
    InstallHostThunk(Signal);
  }

  uint64_t SignalDelegator::GuestSigAction(int Signal, const FEXCore::GuestSigAction *Action, FEXCore::GuestSigAction *OldAction) {
    std::lock_guard lk(GuestDelegatorMutex);

    // Invalid signal specified
    if (Signal > MAX_SIGNALS) {
      return -EINVAL;
    }

    // If we have an old signal set then give it back
    if (OldAction) {
      *OldAction = HostHandlers[Signal].GuestAction;
    }

    // Now assign the new action
    if (Action) {
      // These signal dispositions can't be changed on Linux
      if (Signal == SIGKILL || Signal == SIGSTOP) {
        return -EINVAL;
      }

      HostHandlers[Signal].GuestAction = *Action;
      // Only attempt to install a new thunk handler if we were installing a new guest action
      if (!InstallHostThunk(Signal)) {
        UpdateHostThunk(Signal);
      }
    }

    return 0;
  }

  void SignalDelegator::CheckXIDHandler() {
    std::lock_guard lk(GuestDelegatorMutex);
    std::lock_guard lk2(HostDelegatorMutex);

    constexpr size_t SIGNAL_SETXID = 33;

    kernel_sigaction CurrentAction{};

    // Only update the old action if we haven't ever been installed
    const int Result = ::syscall(SYS_rt_sigaction, SIGNAL_SETXID, nullptr, &CurrentAction, 8);
    if (Result < 0) {
      LogMan::Msg::AFmt("Failed to get status of XID signal");
      return;
    }

    SignalHandler &HostHandler = HostHandlers[SIGNAL_SETXID];
    if (CurrentAction.handler != HostHandler.HostAction.handler) {
      // GLIBC overwrote our XID handler, reinstate our handler
      const int Result = ::syscall(SYS_rt_sigaction, SIGNAL_SETXID, &HostHandler.HostAction, nullptr, 8);
      if (Result < 0) {
        LogMan::Msg::AFmt("Failed to reinstate our XID signal handler {}", strerror(errno));
      }
    }

    auto LinuxTaskState = GetThreadBound<GuestOSLinuxTaskState>();
    uint64_t HostMask = LinuxTaskState->CurrentSignalMask.Val;
    SetSignalMask(HostMask);
  }

  uint64_t SignalDelegator::RegisterGuestSigAltStack(const stack_t *ss, stack_t *old_ss) {
    auto State = &GetThreadBound<FEXCore::Core::CpuStateFrame>()->State;
    auto LinuxTaskState = GetThreadBound<GuestOSLinuxTaskState>();
    
    bool UsingAltStack{};
    uint64_t AltStackBase = reinterpret_cast<uint64_t>(LinuxTaskState->GuestAltStack.ss_sp);
    uint64_t AltStackEnd = AltStackBase + LinuxTaskState->GuestAltStack.ss_size;
    uint64_t GuestSP = State->gregs[FEXCore::X86State::REG_RSP];

    if (!(LinuxTaskState->GuestAltStack.ss_flags & SS_DISABLE) &&
        GuestSP >= AltStackBase &&
        GuestSP <= AltStackEnd) {
      UsingAltStack = true;
    }

    // If we have an old signal set then give it back
    if (old_ss) {
      *old_ss = LinuxTaskState->GuestAltStack;

      if (UsingAltStack) {
        // We are currently operating on the alt stack
        // Let the guest know
        old_ss->ss_flags |= SS_ONSTACK;
      }
      else {
        old_ss->ss_flags |= SS_DISABLE;
      }
    }

    // Now assign the new action
    if (ss) {
      // If we tried setting the alt stack while we are using it then throw an error
      if (UsingAltStack) {
        return -EPERM;
      }

      // We need to check for invalid flags
      // The only flag that can be passed is SS_AUTODISARM and SS_DISABLE
      if ((ss->ss_flags & ~SS_ONSTACK) & // SS_ONSTACK is ignored
          ~(SS_AUTODISARM | SS_DISABLE)) {
        // A flag remained that isn't one of the supported ones?
        return -EINVAL;
      }

      if (ss->ss_flags & SS_DISABLE) {
        // If SS_DISABLE Is specified then the rest of the details are ignored
        LinuxTaskState->GuestAltStack = *ss;
        return 0;
      }

      // stack size needs to be MINSIGSTKSZ (0x2000)
      if (ss->ss_size < X86_MINSIGSTKSZ) {
        return -ENOMEM;
      }

      LinuxTaskState->GuestAltStack = *ss;
    }

    return 0;
  }

  static void CheckForPendingSignals() {
    auto LinuxTaskState = GetThreadBound<GuestOSLinuxTaskState>();
    auto Thread = GetThreadBound<FEXCore::Core::InternalThreadState>();

    // Do we have any pending signals that became unmasked?
    uint64_t PendingSignals = ~LinuxTaskState->CurrentSignalMask.Val & LinuxTaskState->PendingSignals;
    if (PendingSignals != 0) {
      for (int i = 0; i < 64; ++i) {
        if (PendingSignals & (1ULL << i)) {
          FHU::Syscalls::tgkill(Thread->ThreadManager.PID, Thread->ThreadManager.TID, i + 1);
          // We might not even return here which is spooky
        }
      }
    }
  }

  uint64_t SignalDelegator::GuestSigProcMask(int how, const uint64_t *set, uint64_t *oldset, size_t sigsetsize) {
    auto LinuxTaskState = GetThreadBound<GuestOSLinuxTaskState>();

    if (sigsetsize > sizeof(uint64_t)) {
      return -EINVAL;
    }
    
    // The order in which we handle signal mask setting is important here
    // old and new can point to the same location in memory.
    // Even if the pointers are to same memory location, we must store the original signal mask
    // coming in to the syscall.
    // 1) Store old mask
    // 2) Set mask to new mask if exists
    // 3) Give old mask back
    auto OldSet = LinuxTaskState->CurrentSignalMask.Val;

    if (!!set) {
      uint64_t IgnoredSignalsMask = ~((1ULL << (SIGKILL - 1)) | (1ULL << (SIGSTOP - 1)));
      if (how == SIG_BLOCK) {
        LinuxTaskState->CurrentSignalMask.Val |= *set & IgnoredSignalsMask;
      }
      else if (how == SIG_UNBLOCK) {
        LinuxTaskState->CurrentSignalMask.Val &= ~(*set & IgnoredSignalsMask);
      }
      else if (how == SIG_SETMASK) {
        LinuxTaskState->CurrentSignalMask.Val = *set & IgnoredSignalsMask;
      }
      else {
        return -EINVAL;
      }

      uint64_t HostMask = LinuxTaskState->CurrentSignalMask.Val;
      // Now actually set the host mask
      // This will hide from the guest that we are not actually setting all of the masks it wants
      for (size_t i = 0; i < MAX_SIGNALS; ++i) {
        if (HostHandlers[i + 1].Required.load(std::memory_order_relaxed)) {
          // If it is a required host signal then we can't mask it
          HostMask &= ~(1ULL << i);
        }
      }
      SetSignalMask(HostMask);
    }

    if (!!oldset) {
      *oldset = OldSet;
    }

    CheckForPendingSignals();

    return 0;
  }

  uint64_t SignalDelegator::GuestSigPending(uint64_t *set, size_t sigsetsize) {
    auto LinuxTaskState = GetThreadBound<GuestOSLinuxTaskState>();

    if (sigsetsize > sizeof(uint64_t)) {
      return -EINVAL;
    }

    *set = LinuxTaskState->PendingSignals;

    sigset_t HostSet{};
    if (sigpending(&HostSet) == 0) {
      uint64_t HostSignals{};
      for (size_t i = 0; i < MAX_SIGNALS; ++i) {
        if (sigismember(&HostSet, i + 1)) {
          HostSignals |= (1ULL << i);
        }
      }

      // Merge the real pending signal mask as well
      *set |= HostSignals;
    }
    return 0;
  }

  uint64_t SignalDelegator::GuestSigSuspend(uint64_t *set, size_t sigsetsize) {
    auto LinuxTaskState = GetThreadBound<GuestOSLinuxTaskState>();

    if (sigsetsize > sizeof(uint64_t)) {
      return -EINVAL;
    }

    uint64_t IgnoredSignalsMask = ~((1ULL << (SIGKILL - 1)) | (1ULL << (SIGSTOP - 1)));

    // Backup the mask
    LinuxTaskState->PreviousSuspendMask = LinuxTaskState->CurrentSignalMask;
    // Set the new mask
    LinuxTaskState->CurrentSignalMask.Val = *set & IgnoredSignalsMask;
    sigset_t HostSet{};

    sigemptyset(&HostSet);

    for (int32_t i = 0; i < MAX_SIGNALS; ++i) {
      if (*set & (1ULL << i)) {
        sigaddset(&HostSet, i + 1);
      }
    }

    // Additionally we must always listen to SIGNAL_FOR_PAUSE
    // This technically forces us in to a race but should be fine
    // SIGBUS and SIGILL can't happen so we don't need to listen for them
    //sigaddset(&HostSet, SIGNAL_FOR_PAUSE);

    // Spin this in a loop until we aren't sigsuspended
    // This can happen in the case that the guest has sent signal that we can't block
    uint64_t Result = sigsuspend(&HostSet);

    // Restore Previous signal mask we are emulating
    // XXX: Might be unsafe if the signal handler adjusted the thread's signal mask
    // But since we don't support the guest adjusting the mask through the context object
    // then this is safe-ish
    LinuxTaskState->CurrentSignalMask = LinuxTaskState->PreviousSuspendMask;

    CheckForPendingSignals();

    return Result == -1 ? -errno : Result;

  }

  uint64_t SignalDelegator::GuestSigTimedWait(uint64_t *set, siginfo_t *info, const struct timespec *timeout, size_t sigsetsize) {
    if (sigsetsize > sizeof(uint64_t)) {
      return -EINVAL;
    }

    uint64_t Result = ::syscall(SYS_rt_sigtimedwait, set, info, timeout, sigsetsize);

    return Result == -1 ? -errno : Result;
  }

  uint64_t SignalDelegator::GuestSignalFD(int fd, const uint64_t *set, size_t sigsetsize, int flags) {
    if (sigsetsize > sizeof(uint64_t)) {
      return -EINVAL;
    }

    sigset_t HostSet{};
    sigemptyset(&HostSet);

    for (size_t i = 0; i < MAX_SIGNALS; ++i) {
      if (HostHandlers[i + 1].Required.load(std::memory_order_relaxed)) {
        // For now skip our internal signals
        continue;
      }

      if (*set & (1ULL << i)) {
        sigaddset(&HostSet, i + 1);
      }
    }

    // XXX: This is a barebones implementation just to get applications that listen for SIGCHLD to work
    // In the future we need our own listern thread that forwards the result
    // Thread is necessary to prevent deadlocks for a thread that has signaled on the same thread listening to the FD and blocking is enabled
    uint64_t Result = signalfd(fd, &HostSet, flags);

    return Result == -1 ? -errno : Result;
  }

}
