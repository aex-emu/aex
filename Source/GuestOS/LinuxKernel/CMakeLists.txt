add_compile_options(-fno-operator-names)

set(SYSCALLGEN "${CMAKE_SOURCE_DIR}/Scripts/GenerateGuestSyscalls.py")
set(SYSCALL_JSON "${CMAKE_CURRENT_SOURCE_DIR}/GuestOS/LinuxKernel/Meta/Syscalls.json")

set(GuestArchList "x86_32" "x86_64")

foreach(GUEST_ARCH ${GuestArchList})
  set(OUTFOLDER "${CMAKE_CURRENT_BINARY_DIR}/generated/SyscallGen-${GUEST_ARCH}")

  set(SYSCALL_${GUEST_ARCH}_AUTO "${OUTFOLDER}/SyscallAuto.inl")
  set(SYSCALL_${GUEST_ARCH}_ROUTER "${OUTFOLDER}/SyscallRouter.inl")
  set(SYSCALL_${GUEST_ARCH}_TYPES "${OUTFOLDER}/SyscallTypes.inl")

  add_custom_command(
    OUTPUT "${SYSCALL_${GUEST_ARCH}_AUTO}"
    DEPENDS "${SYSCALLGEN}"
    DEPENDS "${SYSCALL_JSON}"
    DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/Templates/SyscallAuto.inl.in"
    COMMAND "${SYSCALLGEN}" "${SYSCALL_JSON}" "${OUTFOLDER}" "${GUEST_ARCH}" "SyscallAuto.inl" "${WHAT}" -- "${CMAKE_CURRENT_SOURCE_DIR}/Templates"
    VERBATIM
    COMMAND_EXPAND_LISTS
  )

  add_custom_command(
    OUTPUT "${SYSCALL_${GUEST_ARCH}_ROUTER}"
    DEPENDS "${SYSCALLGEN}"
    DEPENDS "${SYSCALL_JSON}"
    DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/Templates/SyscallRouter.inl.in"
    COMMAND "${SYSCALLGEN}" "${SYSCALL_JSON}" "${OUTFOLDER}" "${GUEST_ARCH}" "SyscallRouter.inl" "${WHAT}" -- "${CMAKE_CURRENT_SOURCE_DIR}/Templates"
    VERBATIM
    COMMAND_EXPAND_LISTS
  )

  add_custom_command(
    OUTPUT "${SYSCALL_${GUEST_ARCH}_TYPES}"
    DEPENDS "${SYSCALLGEN}"
    DEPENDS "${SYSCALL_JSON}"
    DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/Templates/SyscallTypes.inl.in"
    COMMAND "${SYSCALLGEN}" "${SYSCALL_JSON}" "${OUTFOLDER}" "${GUEST_ARCH}" "SyscallTypes.inl" "${WHAT}" -- "${CMAKE_CURRENT_SOURCE_DIR}/Templates"
    VERBATIM
    COMMAND_EXPAND_LISTS
  )

  add_custom_target("SyscallGen-${GUEST_ARCH}" DEPENDS "${SYSCALL_${GUEST_ARCH}_AUTO}" "${SYSCALL_${GUEST_ARCH}_ROUTER}" "${SYSCALL_${GUEST_ARCH}_TYPES}")
endforeach()

add_library(LinuxEmulation STATIC
    Helpers/EmulatedFiles.cpp
    Helpers/FileManagement.cpp
    Helpers/LinuxAllocator.cpp
    Helpers/SignalDelegator.cpp
    # Helpers/GuestSyscalls.cpp
    Host/HostSyscalls.cpp
    Syscalls.cpp
    SyscallsSMCTracking.cpp
    SyscallsVMATracking.cpp
    Emulation/x86_32/EPoll.cpp
    Emulation/x86_32/Syscalls.cpp
    Emulation/x86_32/FD.cpp
    Emulation/x86_32/FS.cpp
    Emulation/x86_32/Info.cpp
    Emulation/x86_32/IO.cpp
    Emulation/x86_32/Memory.cpp
    Emulation/x86_32/Msg.cpp
    Emulation/x86_32/Semaphore.cpp
    Emulation/x86_32/Sched.cpp
    Emulation/x86_32/Signals.cpp
    Emulation/x86_32/Socket.cpp
    Emulation/x86_32/Thread.cpp
    Emulation/x86_32/Timer.cpp
    Emulation/x86_32/Time.cpp
    Emulation/ioctl32/IoctlEmulation.cpp
    Emulation/x86_64/FD.cpp
    Emulation/x86_64/EPoll.cpp
    Emulation/x86_64/Memory.cpp
    Emulation/x86_64/Semaphore.cpp
    Emulation/x86_64/Signals.cpp
    Emulation/x86_64/Thread.cpp
    Emulation/x86_64/Syscalls.cpp
    Emulation/x86_64/Time.cpp
    Emulation/x86/FD.cpp
    Emulation/x86/FS.cpp
    Emulation/x86/Info.cpp
    Emulation/x86/Memory.cpp
    Emulation/x86/Sched.cpp
    Emulation/x86/Signals.cpp
    Emulation/x86/Thread.cpp
    Emulation/x86/Timer.cpp
    Emulation/x86/Time.cpp
)

foreach(GUEST_ARCH ${GuestArchList})
  add_dependencies(LinuxEmulation "SyscallGen-${GUEST_ARCH}")
endforeach()

target_compile_options(LinuxEmulation
PRIVATE
  -Wall
  -Werror=cast-qual
  -Werror=ignored-qualifiers
  -Werror=implicit-fallthrough

  -Wno-trigraphs
  -fwrapv
)

target_include_directories(LinuxEmulation
PRIVATE
  ${CMAKE_BINARY_DIR}/generated
  ${CMAKE_CURRENT_BINARY_DIR}/generated
  ${PROJECT_SOURCE_DIR}/External/drm-headers/include/
)

target_link_libraries(LinuxEmulation
PRIVATE
  FEXCore
  FEX_Utils
)

set(HEADERS_TO_VERIFY
  Emulation/x86_32/Types.h          x86_32 # This needs to match structs to 32bit structs
  Emulation/ioctl32/Ioctl/asound.h   x86_32 # This needs to match structs to 32bit structs
  Emulation/ioctl32/Ioctl/drm.h      x86_32 # This needs to match structs to 32bit structs
  Emulation/ioctl32/Ioctl/streams.h  x86_32 # This needs to match structs to 32bit structs
  Emulation/ioctl32/Ioctl/usbdev.h   x86_32 # This needs to match structs to 32bit structs
  Emulation/ioctl32/Ioctl/input.h    x86_32 # This needs to match structs to 32bit structs
  Emulation/ioctl32/Ioctl/sockios.h  x86_32 # This needs to match structs to 32bit structs
  Emulation/ioctl32/Ioctl/joystick.h x86_32 # This needs to match structs to 32bit structs
  Emulation/x86_64/Types.h          x86_64 # This needs to match structs to 64bit structs
)

list(LENGTH HEADERS_TO_VERIFY ARG_COUNT)
math(EXPR ARG_COUNT "${ARG_COUNT}-1")

set (ARGS
  "-x" "c++"
  "-std=c++20"
  "-fno-operator-names"
  "-I${PROJECT_SOURCE_DIR}/External/drm-headers/include/")
# Global include directories
get_directory_property (INC_DIRS INCLUDE_DIRECTORIES)
list(TRANSFORM INC_DIRS PREPEND "-I")
list(APPEND ARGS ${INC_DIRS})

# FEXCore directories
get_target_property(INC_DIRS FEXCore INTERFACE_INCLUDE_DIRECTORIES)
list(TRANSFORM INC_DIRS PREPEND "-I")
list(APPEND ARGS ${INC_DIRS})

foreach(Index RANGE 0 ${ARG_COUNT} 2)
  math(EXPR TEST_TYPE_INDEX "${Index}+1")

  list(GET HEADERS_TO_VERIFY ${Index} HEADER)
  list(GET HEADERS_TO_VERIFY ${TEST_TYPE_INDEX} TEST_TYPE)

  file(RELATIVE_PATH REL_HEADER ${CMAKE_BINARY_DIR} "${CMAKE_CURRENT_SOURCE_DIR}/${HEADER}")
  set(TEST_NAME "${TEST_DESC}/Test_verify_${HEADER}")
  set(TEST_NAME_ARCH "${TEST_DESC}/Test_verify_arch_${HEADER}")

  add_test(
    NAME ${TEST_NAME}_x86_64
    WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
    COMMAND "python3" "${CMAKE_SOURCE_DIR}/Scripts/StructPackVerifier.py" "-c1" "x86_64" "${REL_HEADER}" ${ARGS})

  add_test(
    NAME ${TEST_NAME}_aarch64
    WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
    COMMAND "python3" "${CMAKE_SOURCE_DIR}/Scripts/StructPackVerifier.py" "-c1" "aarch64" "${REL_HEADER}" ${ARGS})

  add_test(
    NAME ${TEST_NAME_ARCH}_x86_64
    WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
    COMMAND "python3" "${CMAKE_SOURCE_DIR}/Scripts/StructPackVerifier.py" "-c1" "x86_64" "-c2" "${TEST_TYPE}" "${REL_HEADER}" ${ARGS})

  add_test(
    NAME ${TEST_NAME_ARCH}_aarch64
    WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
    COMMAND "python3" "${CMAKE_SOURCE_DIR}/Scripts/StructPackVerifier.py" "-c1" "aarch64" "-c2" "${TEST_TYPE}" "${REL_HEADER}" ${ARGS})

  set_property(TEST ${TEST_NAME}_x86_64 APPEND PROPERTY DEPENDS "${HEADER}")
  set_property(TEST ${TEST_NAME}_aarch64 APPEND PROPERTY DEPENDS "${HEADER}")
  set_property(TEST ${TEST_NAME_ARCH}_x86_64 APPEND PROPERTY DEPENDS "${HEADER}")
  set_property(TEST ${TEST_NAME_ARCH}_aarch64 APPEND PROPERTY DEPENDS "${HEADER}")
endforeach()

execute_process(COMMAND "nproc" OUTPUT_VARIABLE CORES)
string(STRIP ${CORES} CORES)

add_custom_target(
  struct_verifier
  WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
  USES_TERMINAL
  COMMAND "ctest" "--timeout" "302" "-j${CORES}" "-R" "Test_verify*")
