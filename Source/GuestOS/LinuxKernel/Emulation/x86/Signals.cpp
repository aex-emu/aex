/*
$info$
tags: LinuxSyscalls|syscalls-shared
$end_info$
*/

#include"GuestOS/LinuxKernel/Helpers/SignalDelegator.h"
#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86/Thread.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"

#include "GuestCPU/Context/X86Enums.h"
#include "SignalDelegator/SignalDelegator.h"

#include <signal.h>
#include <sys/syscall.h>
#include <unistd.h>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

#include "Common/ForwardDeclarations.h"

GUEST_SYSCALL(rt_sigprocmask) {
  return FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSigProcMask(how, set, oldset, sigsetsize);
}

GUEST_SYSCALL(rt_sigpending) {
  return FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSigPending(set, sigsetsize);
}

GUEST_SYSCALL(rt_sigsuspend) {
  return FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSigSuspend(unewset, sigsetsize);
}

GUEST_SYSCALL(signalfd) {
  return FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSignalFD(fd, mask, sigsetsize, 0);
}

GUEST_SYSCALL(signalfd4) {
  return FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSignalFD(fd, mask, sigsetsize, flags);
}
