/*
$info$
tags: LinuxSyscalls|syscalls-shared
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"

#include "Common/Utils.h"

#include "GuestCPU/IR/IR.h"

#include <stdint.h>
#include <sched.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/syscall.h>
#include <unistd.h>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

GUEST_SYSCALL(sched_setaffinity) {
  return 0;
}

GUEST_SYSCALL(sched_getaffinity) {
  uint64_t Cores = FEX::HLE::_SyscallHandler->ThreadsConfig();

  // Bytes need to round up to size of uint64_t
  uint64_t Bytes = FEXCore::AlignUp(Cores, sizeof(uint64_t));

  // cpusetsize needs to be 8byte aligned
  if (cpusetsize & (sizeof(uint64_t) - 1)) {
    return -EINVAL;
  }

  // If we don't have enough bytes to store the resulting structure
  // then we need to return -EINVAL
  if (cpusetsize < Bytes) {
    return -EINVAL;
  }

  memset(mask, 0, Bytes);

  for (uint64_t i = 0; i < Cores; ++i) {
    mask[i / 8] |= (1 << (i % 8));
  }

  // Returns the number of bytes written in to mask
  return Bytes;
}
