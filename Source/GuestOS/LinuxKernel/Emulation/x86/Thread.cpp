/*
$info$
tags: LinuxSyscalls|syscalls-shared
$end_info$
*/

#include "GuestCPU/IR/IR.h"
#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86/Thread.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Thread.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Thread.h"

#include "GuestCPU/Context/Context.h"
#include "GuestCPU/Context/X86Enums.h"
#include "GuestCPU/Context/InternalThreadState.h"
#include "GuestCPU/IR/IR.h"

#include "Common/HostSyscalls.h"

#include <grp.h>
#include <limits.h>
#include <linux/futex.h>
#include <stdint.h>
#include <sched.h>
#include <sys/personality.h>
#include <sys/prctl.h>
#include <sys/resource.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/fsuid.h>

#include "Common/ThreadContext.h"

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

ARG_TO_STR(idtype_t, "%u")

namespace FEX::HLE {
  FEXCore::Core::InternalThreadState *CreateNewThread(FEXCore::Context:: Context *CTX, FEXCore::Core::CpuStateFrame *Frame, FEX::HLE::clone3_args *args) {
    uint64_t flags = args->args.flags;
    FEXCore::Core::CPUState NewThreadState{};
    // Clone copies the parent thread's state
    memcpy(&NewThreadState, Frame, sizeof(FEXCore::Core::CPUState));

    NewThreadState.gregs[FEXCore::X86State::REG_RAX] = 0;
    NewThreadState.gregs[FEXCore::X86State::REG_RBX] = 0;
    NewThreadState.gregs[FEXCore::X86State::REG_RBP] = 0;
    if (args->Type == TYPE_CLONE3) {
      // stack pointer points to the lowest address to the stack
      // set RSP to stack + size
      NewThreadState.gregs[FEXCore::X86State::REG_RSP] = args->args.stack + args->args.stack_size;
    }
    else {
      NewThreadState.gregs[FEXCore::X86State::REG_RSP] = args->args.stack;
    }

    auto NewThread = CTX->CreateThread(&NewThreadState, args->args.parent_tid);
    NewThread->InitialSignalMask = GetThreadBound<GuestOSLinuxTaskState>()->CurrentSignalMask.Val;
    CTX->InitializeThread(NewThread);

    if (FEX::HLE::_SyscallHandler->Is64BitMode()) {
      if (flags & CLONE_SETTLS) {
        x64::SetThreadArea(NewThread->CurrentFrame, reinterpret_cast<void*>(args->args.tls));
      }
      // Set us to start just after the syscall instruction
      x64::AdjustRipForNewThread(NewThread->CurrentFrame);
    }
    else {
      if (flags & CLONE_SETTLS) {
        x32::SetThreadArea(NewThread->CurrentFrame, reinterpret_cast<void*>(args->args.tls));
      }
      x32::AdjustRipForNewThread(NewThread->CurrentFrame);
    }

    // Return the new threads TID
    uint64_t Result = NewThread->ThreadManager.GetTID();

    // Sets the child TID to pointer in ParentTID
    if (flags & CLONE_PARENT_SETTID) {
      *reinterpret_cast<pid_t*>(args->args.parent_tid) = Result;
    }

    // Sets the child TID to the pointer in ChildTID
    if (flags & CLONE_CHILD_SETTID) {
      NewThread->ThreadManager.set_child_tid = reinterpret_cast<int32_t*>(args->args.child_tid);
      *reinterpret_cast<pid_t*>(args->args.child_tid) = Result;
    }

    // When the thread exits, clear the child thread ID at ChildTID
    // Additionally wakeup a futex at that address
    // Address /may/ be changed with SET_TID_ADDRESS syscall
    if (flags & CLONE_CHILD_CLEARTID) {
      NewThread->ThreadManager.clear_child_tid = reinterpret_cast<int32_t*>(args->args.child_tid);
    }

    // clone3 flag
    if (flags & CLONE_PIDFD) {
      // Use pidfd_open to emulate this flag
      const int pidfd = ::syscall(SYSCALL_DEF(pidfd_open), Result, 0);
      if (Result == ~0ULL) {
        LogMan::Msg::EFmt("Couldn't get pidfd of TID {}\n", Result);
      }
      else {
        *reinterpret_cast<int*>(args->args.pidfd) = pidfd;
      }
    }

    return NewThread;
  }

  uint64_t HandleNewClone(FEXCore::Core::InternalThreadState *Thread, FEXCore::Context::Context *CTX, FEXCore::Core::CpuStateFrame *Frame, FEX::HLE::clone3_args *CloneArgs) {
    auto GuestArgs = &CloneArgs->args;
    uint64_t flags = GuestArgs->flags;
    auto NewThread = Thread;
    if (flags & CLONE_THREAD) {
      FEXCore::Core::CPUState NewThreadState{};
      // Clone copies the parent thread's state
      memcpy(&NewThreadState, Frame, sizeof(FEXCore::Core::CPUState));

      NewThreadState.gregs[FEXCore::X86State::REG_RAX] = 0;
      NewThreadState.gregs[FEXCore::X86State::REG_RBX] = 0;
      NewThreadState.gregs[FEXCore::X86State::REG_RBP] = 0;
      if (GuestArgs->stack == 0) {
        // Copies in the original thread's stack
      }
      else {
        NewThreadState.gregs[FEXCore::X86State::REG_RSP] = GuestArgs->stack;
      }

      // Overwrite thread
      NewThread =  CTX->CreateThread(&NewThreadState, GuestArgs->parent_tid);
      NewThread->InitialSignalMask = GetThreadBound<GuestOSLinuxTaskState>()->CurrentSignalMask.Val;

      // CLONE_PARENT_SETTID, CLONE_CHILD_SETTID, CLONE_CHILD_CLEARTID, CLONE_PIDFD will be handled by kernel
      // Call execution thread directly since we already are on the new thread
      NewThread->StartRunning.NotifyAll(); // Clear the start running flag
    }
    else{
      // If we don't have CLONE_THREAD then we are effectively a fork
      // Clear all the other threads that are being tracked
      // Frame->Thread is /ONLY/ safe to access when CLONE_THREAD flag is not set
      CTX->CleanupAfterFork(Frame->Thread);

      Thread->CurrentFrame->State.gregs[FEXCore::X86State::REG_RAX] = 0;
      Thread->CurrentFrame->State.gregs[FEXCore::X86State::REG_RBX] = 0;
      Thread->CurrentFrame->State.gregs[FEXCore::X86State::REG_RBP] = 0;
      if (GuestArgs->stack == 0) {
        // Copies in the original thread's stack
      }
      else {
        Thread->CurrentFrame->State.gregs[FEXCore::X86State::REG_RSP] = GuestArgs->stack;
      }
    }

    if (CloneArgs->Type == TYPE_CLONE3) {
      // If we are coming from a clone3 handler then we need to adjust RSP.
      Thread->CurrentFrame->State.gregs[FEXCore::X86State::REG_RSP] += CloneArgs->args.stack_size;
    }

    if (FEX::HLE::_SyscallHandler->Is64BitMode()) {
      if (flags & CLONE_SETTLS) {
        x64::SetThreadArea(NewThread->CurrentFrame, reinterpret_cast<void*>(GuestArgs->tls));
      }
      // Set us to start just after the syscall instruction
      x64::AdjustRipForNewThread(NewThread->CurrentFrame);
    }
    else {
      if (flags & CLONE_SETTLS) {
        x32::SetThreadArea(NewThread->CurrentFrame, reinterpret_cast<void*>(GuestArgs->tls));
      }
      x32::AdjustRipForNewThread(NewThread->CurrentFrame);
    }

    // Depending on clone settings, our TID and PID could have changed
    Thread->ThreadManager.TID = FHU::Syscalls::gettid();
    Thread->ThreadManager.PID = ::getpid();
    FEX::HLE::_SyscallHandler->FM.UpdatePID(Thread->ThreadManager.PID);

    // Start executing the thread directly
    // Our host clone starts in a new stack space, so it can't return back to the JIT space
    return CTX->ExecutionThread(Thread);

    // The rest of the context remains as is and the thread will continue executing
    // TODO: This was wrong, DestroyThread may delete Thread
    //return Thread->StatusCode;
  }

  uint64_t ForkGuest(FEXCore::Core::InternalThreadState *Thread, FEXCore::Core::CpuStateFrame *Frame, uint32_t flags, void *stack, size_t StackSize, pid_t *parent_tid, pid_t *child_tid, void *tls) {
    // Just before we fork, we lock all syscall mutexes so that both processes will end up with a locked mutex
    FEX::HLE::_SyscallHandler->LockBeforeFork();
    
    pid_t Result{};
    if (flags & CLONE_VFORK) {
      // XXX: We don't currently support a vfork as it causes problems.
      // Currently behaves like a fork, which isn't correct. Need to find where the problem is
      Result = fork();
    }
    else {
      Result = fork();
    }

    // Unlock the mutexes on both sides of the fork
    FEX::HLE::_SyscallHandler->UnlockAfterFork();

    if (Result == 0) {
      // Child
      // update the internal TID
      Thread->ThreadManager.TID = FHU::Syscalls::gettid();
      Thread->ThreadManager.PID = ::getpid();
      FEX::HLE::_SyscallHandler->FM.UpdatePID(Thread->ThreadManager.PID);
      Thread->ThreadManager.clear_child_tid = nullptr;

      // Clear all the other threads that are being tracked
      Thread->CTX->CleanupAfterFork(Frame->Thread);

      // only a  single thread running so no need to remove anything from the thread array

      // Handle child setup now
      if (stack != nullptr) {
        // use specified stack
        Frame->State.gregs[FEXCore::X86State::REG_RSP] = reinterpret_cast<uint64_t>(stack) + StackSize;
      } else {
        // In the case of fork and nullptr stack then the child uses the same stack space as the parent
        // Same virtual address, different address space
      }

      if (FEX::HLE::_SyscallHandler->Is64BitMode()) {
        if (flags & CLONE_SETTLS) {
          x64::SetThreadArea(Frame, tls);
        }
      }
      else {
        // 32bit TLS doesn't just set the fs register
        if (flags & CLONE_SETTLS) {
          x32::SetThreadArea(Frame, tls);
        }
      }

      // Sets the child TID to the pointer in ChildTID
      if (flags & CLONE_CHILD_SETTID) {
        Thread->ThreadManager.set_child_tid = child_tid;
        *child_tid = Thread->ThreadManager.TID;
      }

      // When the thread exits, clear the child thread ID at ChildTID
      // Additionally wakeup a futex at that address
      // Address /may/ be changed with SET_TID_ADDRESS syscall
      if (flags & CLONE_CHILD_CLEARTID) {
        Thread->ThreadManager.clear_child_tid = child_tid;
      }

      // the rest of the context remains as is, this thread will keep executing
      return 0;
    } else {
      if (Result != -1) {
        if (flags & CLONE_PARENT_SETTID) {
          *parent_tid = Result;
        }
      }
      // Parent
      SYSCALL_ERRNO();
    }
  }
}

GUEST_SYSCALL(vfork) {
  return ForkGuest(GetThreadBound<FEXCore::Core::InternalThreadState>(), GetThreadBound<FEXCore::Core::CpuStateFrame>(), CLONE_VFORK, 0, 0, 0, 0, 0);
}

GUEST_SYSCALL(clone3) {
  FEX::HLE::clone3_args args{};
  args.Type = TypeOfClone::TYPE_CLONE3;
  memcpy(&args.args, cl_args, std::min(sizeof(FEX::HLE::kernel_clone3_args), size));
  return CloneHandler(GetThreadBound<FEXCore::Core::CpuStateFrame>(), &args);
}

GUEST_SYSCALL(exit) {
  auto Thread = GetThreadBound<FEXCore::Core::InternalThreadState>();
  if (Thread->ThreadManager.clear_child_tid) {
    std::atomic<uint32_t> *Addr = reinterpret_cast<std::atomic<uint32_t>*>(Thread->ThreadManager.clear_child_tid);
    Addr->store(0);
    syscall(SYSCALL_DEF(futex),
      Thread->ThreadManager.clear_child_tid,
      FUTEX_WAKE,
      ~0ULL,
      0,
      0,
      0);
  }

  Thread->StatusCode = status;
  Thread->CTX->ExitCurrentThread(Thread);
}

GUEST_SYSCALL(prctl) {
  uint64_t Result{};
  switch (option) {
  case PR_SET_SECCOMP:
  case PR_GET_SECCOMP:
    // FEX doesn't support seccomp
    return -EINVAL;
    break;
  default:
    Result = ::prctl(option, arg2, arg3, arg4, arg5);
  break;
  }
  SYSCALL_ERRNO();
}

GUEST_SYSCALL(arch_prctl) {
  constexpr uint64_t TASK_MAX = (1ULL << 48); // 48-bits until we can query the host side VA sanely. AArch64 doesn't expose this in cpuinfo

  auto Frame = GetThreadBound<FEXCore::Core::CpuStateFrame>();

  uint64_t Result{};
  switch (code) {
    case 0x1001: // ARCH_SET_GS
      if (addr >= TASK_MAX) {
        // Ignore a non-canonical address
        return -EPERM;
      }
      Frame->State.gs = addr;
      Result = 0;
    break;
    case 0x1002: // ARCH_SET_FS
      if (addr >= TASK_MAX) {
        // Ignore a non-canonical address
        return -EPERM;
      }
      Frame->State.fs = addr;
      Result = 0;
    break;
    case 0x1003: // ARCH_GET_FS
      *reinterpret_cast<uint64_t*>(addr) = Frame->State.fs;
      Result = 0;
    break;
    case 0x1004: // ARCH_GET_GS
      *reinterpret_cast<uint64_t*>(addr) = Frame->State.gs;
      Result = 0;
    break;
    case 0x3001: // ARCH_CET_STATUS
      Result = -EINVAL; // We don't support CET, return EINVAL
    break;
    case 0x1011: // ARCH_GET_CPUID
      return 1;
    break;
    case 0x1012: // ARCH_SET_CPUID
      return -ENODEV; // Claim we don't support faulting on CPUID
    break;
    default:
      LogMan::Msg::EFmt("Unknown prctl: 0x{:x}", code);
      Result = -EINVAL;
    break;
  }
  SYSCALL_ERRNO();
}

GUEST_SYSCALL(gettid) {
  uint64_t Result = FHU::Syscalls::gettid();
  SYSCALL_ERRNO();
}

GUEST_SYSCALL(set_tid_address) {
  auto Thread = GetThreadBound<FEXCore::Core::InternalThreadState>();
  Thread->ThreadManager.clear_child_tid = tidptr;
  return Thread->ThreadManager.GetTID();
}

GUEST_SYSCALL(exit_group) {
  // TODO: Fix semantics for this
  // TODO: Refcount context and exit ?
  // TODO: Verify kernel semantics
  syscall(SYS_exit_group, status);

  // auto Thread = GetThreadBound<FEXCore::Core::InternalThreadState>();
  // Thread->StatusCode = status;
  // Thread->CTX->Stop(false);

  // This must never be reached
  std::terminate();
}


GUEST_SYSCALL(getpgrp) {
  return HOST_SYSCALL(getpgid, 0);
}

