/*
$info$
tags: LinuxSyscalls|syscalls-shared
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"

#include "GuestCPU/IR/IR.h"
#include "LogMgr/LogManager.h"

#include <cstring>
#include <linux/kcmp.h>
#include <linux/seccomp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <syslog.h>
#include <sys/random.h>
#include <sys/resource.h>
#include <sys/syscall.h>
#include <sys/utsname.h>
#include <sys/klog.h>
#include <unistd.h>

#include "Generated/git_version.h"

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

GUEST_SYSCALL(uname) {
  struct utsname Local{};
  if (::uname(&Local) == 0) {
    memcpy(buf->nodename, Local.nodename, sizeof(Local.nodename));
    static_assert(sizeof(Local.nodename) <= sizeof(buf->nodename));
    memcpy(buf->domainname, Local.domainname, sizeof(Local.domainname));
    static_assert(sizeof(Local.domainname) <= sizeof(buf->domainname));
  }
  else {
    strcpy(buf->nodename, "FEXCore");
    LogMan::Msg::EFmt("Couldn't determine host nodename. Defaulting to '{}'", buf->nodename);
  }
  strcpy(buf->sysname, "Linux");
  uint32_t GuestVersion = FEX::HLE::_SyscallHandler->GetGuestKernelVersion();
  snprintf(buf->release, sizeof(buf->release), "%d.%d.%d",
    FEX::HLE::SyscallHandler::KernelMajor(GuestVersion),
    FEX::HLE::SyscallHandler::KernelMinor(GuestVersion),
    FEX::HLE::SyscallHandler::KernelPatch(GuestVersion));

  const char version[] = "#" GIT_DESCRIBE_STRING " SMP " __DATE__ " " __TIME__;
  strcpy(buf->version, version);
  static_assert(sizeof(version) <= sizeof(buf->version), "uname version define became too large!");
  // Tell the guest that we are a 64bit kernel
  strcpy(buf->machine, "x86_64");
  return 0;
}

GUEST_SYSCALL(getcpu) {
  uint32_t LocalCPU{};
  uint32_t LocalNode{};
  // tcache is ignored
  auto Result = ::syscall(SYSCALL_DEF(getcpu), cpu ? &LocalCPU : nullptr, node ? &LocalNode : nullptr, nullptr);
  if (Result == 0) {
    if (cpu) {
      // Ensure we don't return a number over our number of emulated cores
      *cpu = LocalCPU % FEX::HLE::_SyscallHandler->ThreadsConfig();
    }

    if (node) {
      // Just claim we are part of node zero
      *node = 0;
    }
  }
  
  SYSCALL_ERRNO();
}
