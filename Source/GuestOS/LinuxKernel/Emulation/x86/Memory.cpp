/*
$info$
tags: LinuxSyscalls|syscalls-shared
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"

#include "GuestCPU/IR/IR.h"

#include <stddef.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <unistd.h>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

#include "Common/ThreadContext.h"

GUEST_SYSCALL(brk) {
  return FEX::HLE::_SyscallHandler->HandleBRK(GetThreadBound<FEXCore::Core::CpuStateFrame>(), addr);
}

GUEST_SYSCALL(madvise) {
  auto lk = FEX::HLE::_SyscallHandler->LockOnlyMman();

  auto Result = ::madvise(addr, length, advice);

  if (Result != -1) {
    FEX::HLE::_SyscallHandler->TrackMadvise((uintptr_t)addr, length, advice);
  }
  
  SYSCALL_ERRNO();
}
