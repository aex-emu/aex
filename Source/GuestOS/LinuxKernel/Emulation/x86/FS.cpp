/*
$info$
tags: LinuxSyscalls|syscalls-shared
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"

#include "GuestCPU/IR/IR.h"

#include <stddef.h>
#include <stdint.h>
#include <sys/mount.h>
#include <sys/swap.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/xattr.h>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

GUEST_SYSCALL(readlink) {
  auto Result = FEX::HLE::_SyscallHandler->FM.Readlink(pathname, buf, bufsiz);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(mknod) {
  auto Result = FEX::HLE::_SyscallHandler->FM.Mknod(pathname, mode, dev);

  SYSCALL_ERRNO();
}

// These don't map 1:1 so using glibc wrappers for now
GUEST_SYSCALL(creat) {
  auto Result = ::creat(pathname, mode);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(link) {
  auto Result = ::link(oldpath, newpath);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(rmdir) {
  auto Result = ::rmdir(pathname);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(mkdir) {
  auto Result = ::mkdir(pathname, mode);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(rename) {
  auto Result = ::rename(oldpath, newpath);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(symlink) {
  auto Result = ::symlink(target, linkpath);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(chmod) {
  auto Result = ::chmod(pathname, mode);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(unlink) {
  auto Result = ::unlink(pathname);

  SYSCALL_ERRNO();
}