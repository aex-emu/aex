/*
$info$
tags: LinuxSyscalls|syscalls-shared
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"

#include "GuestCPU/IR/IR.h"

#include "Common/HostSyscalls.h"

#include <fcntl.h>
#include <stdint.h>
#include <sys/file.h>
#include <sys/eventfd.h>
#include <sys/inotify.h>
#include <sys/mman.h>
#include <sys/timerfd.h>
#include <poll.h>
#include <stddef.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/eventfd.h>
#include <sys/syscall.h>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

GUEST_SYSCALL(open) {
  auto FlagsHost = FEX::HLE::RemapFromX86Flags(flags);
  auto Result = FEX::HLE::_SyscallHandler->FM.Open(pathname, FlagsHost, mode);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(close) {
  auto Result = FEX::HLE::_SyscallHandler->FM.Close(fd);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(access) {
  auto Result = FEX::HLE::_SyscallHandler->FM.Access(pathname, mode);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(openat) {
  auto FlagsHost = FEX::HLE::RemapFromX86Flags(flags);
  auto Result = FEX::HLE::_SyscallHandler->FM.Openat(dirfs, pathname, FlagsHost, mode);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(readlinkat) {
  auto Result = FEX::HLE::_SyscallHandler->FM.Readlinkat(dirfd, pathname, buf, bufsiz);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(faccessat) {
  auto Result = FEX::HLE::_SyscallHandler->FM.FAccessat(dirfd, pathname, mode);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(statx) {
  // Flags don't need remapped
  auto Result = FEX::HLE::_SyscallHandler->FM.Statx(dirfd, pathname, flags, mask, statxbuf);

  SYSCALL_ERRNO();
}

// kernel 5.8.0+
GUEST_SYSCALL(faccessat2) {
  auto Result = FEX::HLE::_SyscallHandler->FM.FAccessat2(dirfd, pathname, mode, flags);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(openat2) {
  open_how HostHow{};
  size_t HostSize = std::min(sizeof(open_how), usize);
  memcpy(&HostHow, how, HostSize);

  HostHow.flags = FEX::HLE::RemapFromX86Flags(HostHow.flags);
  auto Result = FEX::HLE::_SyscallHandler->FM.Openat2(dirfs, pathname, &HostHow, HostSize);

  SYSCALL_ERRNO();
}

// kernel 5.9.0+
GUEST_SYSCALL(close_range) {
  auto Result = FEX::HLE::_SyscallHandler->FM.CloseRange(first, last, flags);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(chown) {
  auto Result = ::chown(pathname, owner, group);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(lchown) {
  auto Result = ::lchown(pathname, owner, group);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(pipe) {
  auto Result = ::pipe(pipefd);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(fchmod) {
  auto Result = ::fchmod(fd, mode);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(symlinkat) {
  auto Result = ::symlinkat(target, newdirfd, linkpath);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(unlinkat) {
  auto Result = ::unlinkat(dirfd, pathname, flags);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(inotify_init) {
  auto Result = ::inotify_init();

  SYSCALL_ERRNO();
}

GUEST_SYSCALL(eventfd) {
  auto Result = ::eventfd(count, 0);

  SYSCALL_ERRNO();
}