#include "GuestOS/LinuxKernel/Emulation/x86_32/Types.h"
#include "HelperDefines.h"

#include <cstdint>
#include <linux/sockios.h>
#include <sys/ioctl.h>

namespace FEX::HLE::x32 {
namespace sockios {
#ifndef SIOCGSKNS
#define SIOCGSKNS	0x894C
#endif
#include "sockios.inl"
}
}

