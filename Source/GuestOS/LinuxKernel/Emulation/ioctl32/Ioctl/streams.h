#include "GuestOS/LinuxKernel/Emulation/x86_32/Types.h"
#include "HelperDefines.h"

#include <cstdint>
#include <sys/ioctl.h>

namespace FEX::HLE::x32 {
namespace streams {
#ifndef TIOCGPTPEER
#define TIOCGPTPEER     _IO('T', 0x41)
#endif
#include "streams.inl"
}

}
