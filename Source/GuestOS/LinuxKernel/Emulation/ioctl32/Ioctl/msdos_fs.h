#pragma once

#include "GuestOS/LinuxKernel/Emulation/x86_32/Types.h"
#include "HelperDefines.h"

#include <cstdint>
#include <linux/msdos_fs.h>
#include <sys/ioctl.h>

namespace FEX::HLE::x32 {

namespace msdos_fs {
#include "msdos_fs.inl"
}
}
