#include "GuestOS/LinuxKernel/Emulation/x86_32/Types.h"
#include "HelperDefines.h"

#include <cstdint>
#include <linux/wireless.h>
#include <sys/ioctl.h>

namespace FEX::HLE::x32 {
namespace wireless {
#include "wireless.inl"
}

}
