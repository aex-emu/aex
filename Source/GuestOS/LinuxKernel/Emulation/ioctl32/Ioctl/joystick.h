#pragma once

#include "GuestOS/LinuxKernel/Emulation/x86_32/Types.h"
#include "HelperDefines.h"

#include <cstdint>
#include <linux/joystick.h>
#include <sys/ioctl.h>

namespace FEX::HLE::x32 {

namespace joystick {
#include "joystick.inl"
}
}
