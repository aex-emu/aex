#pragma once

#include "GuestOS/LinuxKernel/Emulation/x86_32/Types.h"
#include "HelperDefines.h"

#include <cstdint>
#include <linux/blktrace_api.h>
#include <linux/fs.h>
#include <linux/fiemap.h>
#include <sys/ioctl.h>

namespace FEX::HLE::x32 {

namespace ext_fs {
#include "ext_fs.inl"
}
}
