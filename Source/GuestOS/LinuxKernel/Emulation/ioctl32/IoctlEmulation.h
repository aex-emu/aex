#pragma once
#include <cstdint>

#include "Common/ForwardDeclarations.h"

namespace FEX::HLE::x32 {
  void InitializeStaticIoctlHandlers();
  uint32_t ioctl32(int fd, uint32_t request, uint32_t args);
  void CheckAndAddFDDuplication(int fd, int NewFD);
}

