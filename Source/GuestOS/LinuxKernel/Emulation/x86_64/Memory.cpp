/*
$info$
tags: LinuxSyscalls|syscalls-x86-64
$end_info$
*/

#include "GuestOS/LinuxKernel/Helpers/LinuxAllocator.h"
#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestCPU/Context/Context.h"
#include "GuestCPU/Context/InternalThreadState.h"

#include "GuestCPU/IR/IR.h"

#include <sys/mman.h>
#include <sys/shm.h>
#include <map>
#include <unistd.h>

#include "GuestCPU/Context/Context.h"
#include "Config/Config.h"
#include "Allocator/Allocator.h"
#include <fstream>
#include <filesystem>

// the actual mappings don't have exec to prevent accidentally running code
// TODO: Don't duplicate this
static int MAP_MMAN_PROT(int prot) {
  return prot & ~PROT_EXEC;
}

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

namespace FEX::HLE::x64 {

  void *x64SyscallHandler::GuestMmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset) {
    auto lk = FEX::HLE::_SyscallHandler->LockMmanAndTranslation();

    uint64_t Result{};

    bool Map32Bit = flags & FEX::HLE::X86_64_MAP_32BIT;
    if (Map32Bit) {
      Result = (uint64_t)Get32BitAllocator()->mmap(addr, length, MAP_MMAN_PROT(prot),flags, fd, offset);
      if (FEX::HLE::HasSyscallError(Result)) {
        errno = -Result;
        Result = -1;
      }
    } else {
      Result = reinterpret_cast<uint64_t>(::mmap(reinterpret_cast<void*>(addr), length, MAP_MMAN_PROT(prot), flags, fd, offset));
    }

    if (Result != -1) {
      FEX::HLE::_SyscallHandler->TrackMmap((uintptr_t)Result, length, prot, flags, fd, offset);
    }

    return reinterpret_cast<void*>(Result);
  }

  int x64SyscallHandler::GuestMunmap(void *addr, uint64_t length) {
    auto lk = FEX::HLE::_SyscallHandler->LockMmanAndTranslation();

    uint64_t Result{};
    if (reinterpret_cast<uintptr_t>(addr) < 0x1'0000'0000ULL) {
      Result = Get32BitAllocator()->munmap(addr, length);

      if (FEX::HLE::HasSyscallError(Result)) {
        errno = -Result;
        Result = -1;
      }
    } else {
      Result = ::munmap(addr, length);
    }

    if (Result != -1) {
      FEX::HLE::_SyscallHandler->TrackMunmap(reinterpret_cast<uintptr_t>(addr), length);
    }

    return Result;
  }
}

GUEST_SYSCALL_x86_64(mmap) {
  uint64_t Result = (uint64_t) static_cast<FEX::HLE::x64::x64SyscallHandler*>(FEX::HLE::_SyscallHandler)->
    GuestMmap(addr, length, prot, flags, fd, offset);

  SYSCALL_ERRNO();
}


GUEST_SYSCALL_x86_64(munmap) {
  uint64_t Result = static_cast<FEX::HLE::x64::x64SyscallHandler*>(FEX::HLE::_SyscallHandler)->
    GuestMunmap(addr, length);

  SYSCALL_ERRNO();
}


GUEST_SYSCALL_x86_64(mremap) {
  auto lk = FEX::HLE::_SyscallHandler->LockMmanAndTranslation();

  uint64_t Result = reinterpret_cast<uint64_t>(::mremap(old_address, old_size, new_size, flags, new_address));

  if (Result != -1) {
    FEX::HLE::_SyscallHandler->TrackMremap((uintptr_t)old_address, old_size, new_size, flags, Result);
  }
  SYSCALL_ERRNO();
}


GUEST_SYSCALL_x86_64(mprotect) {
  auto lk = FEX::HLE::_SyscallHandler->LockMmanAndTranslation();

  uint64_t Result = ::mprotect(addr, len, MAP_MMAN_PROT(prot));

  if (Result != -1) {
    FEX::HLE::_SyscallHandler->TrackMprotect((uintptr_t)addr, len, prot);
  }
  SYSCALL_ERRNO();
}

GUEST_SYSCALL_x86_64(shmat) {
  auto lk = FEX::HLE::_SyscallHandler->LockMmanAndTranslation();

  uint64_t Result = reinterpret_cast<uint64_t>(shmat(shmid, shmaddr, shmflg));

  if (Result != -1) {
    FEX::HLE::_SyscallHandler->TrackShmat(shmid, Result, shmflg);
  }
  SYSCALL_ERRNO();
}

GUEST_SYSCALL_x86_64(shmdt) {
  auto lk = FEX::HLE::_SyscallHandler->LockMmanAndTranslation();

  uint64_t Result = ::shmdt(shmaddr);

  if (Result != -1) {
    FEX::HLE::_SyscallHandler->TrackShmdt((uintptr_t)shmaddr);
  }
  SYSCALL_ERRNO();
}
