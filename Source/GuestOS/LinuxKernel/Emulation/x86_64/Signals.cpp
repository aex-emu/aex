/*
$info$
tags: LinuxSyscalls|syscalls-x86-64
$end_info$
*/

#include"GuestOS/LinuxKernel/Helpers/SignalDelegator.h"
#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86/Thread.h"

#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"

#include "GuestCPU/Context/X86Enums.h"
#include "SignalDelegator/SignalDelegator.h"

#include <signal.h>
#include <sys/syscall.h>
#include <unistd.h>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

#include "GuestCPU/Context/GuestState.h"

#include "Common/ThreadContext.h"

GUEST_SYSCALL_x86_64(rt_sigaction) {
  if (sigsetsize != 8) {
    return -EINVAL;
  }

  return FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSigAction(signum, act, oldact);
}


GUEST_SYSCALL_x86_64(rt_sigtimedwait) {
  return FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSigTimedWait(set, info, timeout, sigsetsize);
}

GUEST_SYSCALL_x86_64(rt_sigreturn) {
  auto Frame = GetThreadBound<FEXCore::Core::CpuStateFrame>();

  _SyscallHandler->GetSignalDelegator()->RestoreSigFrame64(Frame);

  // TODO: DOCUMENT THIS BETTER
  // this is a magic value that might mean "incomplete", and was meant to be used for when a signal had interrupted
  // the syscall, and a handler was to be executed
  //
  // However, this works just fine for here as well, as we just want to re-dispatch without adjusting RIP or RAX
  // Make part of the spec? use another special value?
  //
  return UINT64_MAX - 4095;
}

