/*
$info$
tags: LinuxSyscalls|syscalls-x86-64
$end_info$
*/

#include"GuestOS/LinuxKernel/Helpers/SignalDelegator.h"
#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Thread.h"

#include "GuestCPU/Context/GuestState.h"
#include "GuestCPU/Context/InternalThreadState.h"
#include "GuestOS/Misc/ThreadManagement.h"
#include "GuestCPU/IR/IR.h"

#include <sched.h>
#include <signal.h>
#include <stddef.h>
#include <syscall.h>
#include <stdint.h>
#include <unistd.h>
#include <vector>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

#include "Common/ThreadContext.h"

namespace FEX::HLE::x64 {
  uint64_t SetThreadArea(FEXCore::Core::CpuStateFrame* Frame, void* tls) {
    Frame->State.fs = reinterpret_cast<uint64_t>(tls);
    return 0;
  }

  void AdjustRipForNewThread(FEXCore::Core::CpuStateFrame* Frame) {
    Frame->State.rip += 2;
  }
}


GUEST_SYSCALL_x86_64(clone) {
  FEX::HLE::clone3_args args{
    .Type = TypeOfClone::TYPE_CLONE2,
    .args = {
      .flags = flags, // CSIGNAL is contained in here
      .pidfd = 0, // For clone, pidfd is duplicated here
      .child_tid = reinterpret_cast<uint64_t>(child_tid),
      .parent_tid = reinterpret_cast<uint64_t>(parent_tid),
      .exit_signal = flags & CSIGNAL,
      .stack = reinterpret_cast<uint64_t>(stack),
      .stack_size = 0, // This syscall isn't able to see the stack size
      .tls = reinterpret_cast<uint64_t>(tls),
      .set_tid = 0, // This syscall isn't able to select TIDs
      .set_tid_size = 0,
      .cgroup = 0, // This syscall can't select cgroups
    },
  };
  return CloneHandler(GetThreadBound<FEXCore::Core::CpuStateFrame>(), &args);
}


GUEST_SYSCALL_x86_64(set_robust_list) {
  auto Thread = GetThreadBound<FEXCore::Core::InternalThreadState>();
  Thread->ThreadManager.robust_list_head = reinterpret_cast<uint64_t>(head);
#ifdef TERMUX_BUILD
  // Termux/Android doesn't support `set_robust_list` syscall.
  // The seccomp filter that the OS installs explicitly blocks this syscall from working
  // glibc uses this syscall for tls and thread data so almost every application uses it
  // Return success since we have stored the pointer ourselves.
  return 0;
#else
  uint64_t Result = ::syscall(SYSCALL_DEF(set_robust_list), head, len);
  SYSCALL_ERRNO();
#endif
}


GUEST_SYSCALL_x86_64(sigaltstack) {
  return FEX::HLE::_SyscallHandler->GetSignalDelegator()->RegisterGuestSigAltStack(ss, old_ss);
}

// launch a new process under fex
// currently does not propagate argv[0] correctly

GUEST_SYSCALL_x86_64(execve) {
  std::vector<const char*> Args;
  std::vector<const char*> Envp;

  if (argv) {
    for (int i = 0; argv[i]; i++) {
      Args.push_back(argv[i]);
    }

    Args.push_back(nullptr);
  }

  if (envp) {
    for (int i = 0; envp[i]; i++) {
      Envp.push_back(envp[i]);
    }

    Envp.push_back(nullptr);
  }

  auto* const* ArgsPtr = argv ? const_cast<char* const*>(Args.data()) : nullptr;
  auto* const* EnvpPtr = envp ? const_cast<char* const*>(Envp.data()) : nullptr;
  return FEX::HLE::ExecveHandler(pathname, ArgsPtr, EnvpPtr, nullptr);
}


GUEST_SYSCALL_x86_64(execveat) {
  std::vector<const char*> Args;
  std::vector<const char*> Envp;

  if (argv) {
    for (int i = 0; argv[i]; i++) {
      Args.push_back(argv[i]);
    }

    Args.push_back(nullptr);
  }

  if (envp) {
    for (int i = 0; envp[i]; i++) {
      Envp.push_back(envp[i]);
    }

    Envp.push_back(nullptr);
  }

  FEX::HLE::ExecveAtArgs AtArgs{
    .dirfd = dirfd,
    .flags = flags,
  };

  auto* const* ArgsPtr = argv ? const_cast<char* const*>(Args.data()) : nullptr;
  auto* const* EnvpPtr = envp ? const_cast<char* const*>(Envp.data()) : nullptr;
  return FEX::HLE::ExecveHandler(pathname, ArgsPtr, EnvpPtr, &AtArgs);
}
