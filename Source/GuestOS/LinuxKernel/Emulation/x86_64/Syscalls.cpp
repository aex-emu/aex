/*
$info$
tags: LinuxSyscalls|syscalls-x86-64
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestOS/LinuxKernel/Meta/x86_64/SyscallsEnum.h"

#include "GuestOS/Misc/SyscallHandler.h"

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

#include <map>

#include "Generated/SyscallGen-x86_64/SyscallAuto.inl"

namespace x86_64 {
  template<int GuestSyscallNumber>
  uint64_t GuestSyscallRouter(uint64_t a0, uint64_t a1, uint64_t a2, uint64_t a3, uint64_t a4, uint64_t a5) {
    const GuestSyscallHandler<GuestSyscallNumber> handler { a0, a1, a2, a3, a4, a5 };
    return handler.handle();
  }

  template<int GuestSyscallNumber>
  uint64_t UknownSyscallRouter(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t) {
    LogMan::Msg::EFmt("**** x86_32::UknownSyscallRouter {} ****", GuestSyscallNumber);
    return -ENOSYS;
  }

  template<int GuestSyscallNumber>
  uint64_t TooOldHostKernelRouter(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t) {
    LogMan::Msg::IFmt("**** TooOldHostKernelRouter **** {}", GuestSyscallNumber);
    return -ENOSYS;
  }

  template<int GuestSyscallNumber>
  uint64_t StubRouter(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t) {
    LogMan::Msg::IFmt("**** StubRouter ****", GuestSyscallNumber);
    return -ENOSYS;
  }
}

static constexpr std::array<GuestSyscallHandlerFunctionType *, SYSCALL_x86_64_MAX> InitializeUnknownSyscallRouter() {
  #define DECL_SYSCALL(num) &x86_64::UknownSyscallRouter<num>,
  return {
    #include "GuestOS/LinuxKernel/Host/SyscallDecls.inl"
  };
}

namespace FEX::HLE::x64 {

  x64SyscallHandler::x64SyscallHandler(FEXCore::Context::Context *ctx, FEX::HLE::SignalDelegator *_SignalDelegation)
    : SyscallHandler {ctx, _SignalDelegation} {
    OSABI = FEXCore::HLE::SyscallOSABI::OS_LINUX64;

    RegisterSyscallHandlers();
  }

  void x64SyscallHandler::RegisterSyscallHandlers() {
    Definitions.resize(SYSCALL_x86_64_MAX);
    HandlerFunctions.resize(SYSCALL_x86_64_MAX);

    {

      auto rv = InitializeUnknownSyscallRouter();
      memcpy(HandlerFunctions.data(), rv.data(), HandlerFunctions.size() * sizeof(HandlerFunctions[0]));

      auto Handler = this;

      #define REGISTER_GUEST_SYSCALL_x86_64(arch, syscall) \
        HandlerFunctions[SYSCALL_x86_64_##syscall] = &arch::GuestSyscallRouter<SYSCALL_x86_64_##syscall>;

      #define REGISTER_GUEST_SYSCALL_x86_64_OLDKERNEL(syscall) \
        HandlerFunctions[SYSCALL_x86_64_##syscall] = &x86_64::TooOldHostKernelRouter<SYSCALL_x86_64_##syscall>;

      #define REGISTER_GUEST_SYSCALL_x86_64_STUB(syscall) \
        HandlerFunctions[SYSCALL_x86_64_##syscall] = &x86_64::StubRouter<SYSCALL_x86_64_##syscall>;

      #include "Generated/SyscallGen-x86_64/SyscallRouter.inl"
    }

#if 0
    // x86-64 has a gap of syscalls in the range of [335, 424) where there aren't any
    // These are defined that these must return -ENOSYS
    // This allows x86-64 to start using the common syscall numbers
    // Fill the gap to ensure that FEX doesn't assert
    constexpr int SYSCALL_GAP_BEGIN = 335;
    constexpr int SYSCALL_GAP_END = 424;
    for (int SyscallNumber = SYSCALL_GAP_BEGIN; SyscallNumber < SYSCALL_GAP_END; ++SyscallNumber) {
      auto &Def = Definitions.at(SyscallNumber);
      Def.Ptr = cvt(&UnimplementedSyscallSafe);
      Def.NumArgs = 0;
      Def.Flags = FEXCore::IR::SyscallFlags::DEFAULT;
      // This will allow our syscall optimization code to make this code more optimal
      // Unlikely to hit a hot path though
      Def.HostSyscallNumber = SYSCALL_DEF(MAX);
#ifdef DEBUG_STRACE
      Def.StraceFmt = "Invalid";
#endif
    }
#endif

#if PRINT_MISSING_SYSCALLS
    for (auto &Syscall: SyscallNames) {
      if (Definitions[Syscall.first].Ptr == cvt(&UnimplementedSyscall)) {
        LogMan::Msg::DFmt("Unimplemented syscall: %s", Syscall.second);
      }
    }
#endif
  }

  std::unique_ptr<FEX::HLE::SyscallHandler> CreateHandler(FEXCore::Context::Context *ctx, FEX::HLE::SignalDelegator *_SignalDelegation) {
    return std::make_unique<x64SyscallHandler>(ctx, _SignalDelegation);
  }
}
