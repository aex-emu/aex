/*
$info$
meta: LinuxSyscalls|syscalls-x86-64 ~ x86-64 specific syscall implementations
tags: LinuxSyscalls|syscalls-x86-64
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Types.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Types.h"

#include <algorithm>
#include <cstdint>
#include <sys/epoll.h>
#include <syscall.h>
#include <vector>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

GUEST_SYSCALL_x86_64(epoll_wait) {
  std::vector<struct epoll_event> Events(std::max(0, maxevents));
  auto Result = HOST_SYSCALL(epoll_pwait, epfd, Events.data(), maxevents, timeout, nullptr, 8);

  if (Result != -1 /*.IsSuccessful()*/) {
    for (size_t i = 0; i < Result/*.Value()*/; ++i) {
      events[i] = Events[i];
    }
  }
  
  return Result;
}

GUEST_SYSCALL_x86_64(epoll_ctl) {
  struct epoll_event Event;
  struct epoll_event *EventPtr{};

  if (event) {
    Event = *event;
    EventPtr = &Event;
  }
  auto Result = HOST_SYSCALL(epoll_ctl, epfd, op, fd, EventPtr);

  if (Result != -1 /*.IsSuccessful()*/ && event) {
    *event = Event;
  }

  return Result;
}

GUEST_SYSCALL_x86_64(epoll_pwait) {
  std::vector<struct epoll_event> Events(std::max(0, maxevents));

  auto Result = HOST_SYSCALL(epoll_pwait, epfd, Events.data(), maxevents, timeout, ssigmask, sigsetsize);

  if (Result != -1 /*.IsSuccessful()*/) {
    for (size_t i = 0; i < Result; ++i) {
      events[i] = Events[i];
    }
  }

  return Result;
}

GUEST_SYSCALL_x86_64(epoll_pwait2) {
  std::vector<struct epoll_event> Events(std::max(0, maxevents));

  auto Result = HOST_SYSCALL(epoll_pwait2, epfd, Events.data(), maxevents, timeout, ssigmask, sigsetsize);

  if (Result != -1 /*.IsSuccessful()*/) {
    for (size_t i = 0; i < Result; ++i) {
      events[i] = Events[i];
    }
  }

  return Result;
}

// These don't map 1:1 so glib wrappers it is, for now

GUEST_SYSCALL_x86_64(epoll_create) {
  uint64_t Result = ::epoll_create(size);
  SYSCALL_ERRNO();
}
