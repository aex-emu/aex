/*
$info$
tags: LinuxSyscalls|syscalls-x86-64
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Types.h"

#include "Common/HostSyscalls.h"

#include <stddef.h>
#include <stdint.h>

#include "Common/ForwardDeclarations.h"

ARG_TO_STR(FEX::HLE::x64::semun, "%lx")

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

GUEST_SYSCALL_x86_64(semctl) {
  uint64_t Result{};
  switch (cmd) {
  case IPC_SET: {
    struct semid64_ds buf {};
    buf = *semun.buf;
    Result = ::syscall(SYSCALL_DEF(semctl), semid, semnum, cmd, &buf);
    if (Result != -1) {
      *semun.buf = buf;
    }
    break;
  }
  case SEM_STAT:
  case SEM_STAT_ANY:
  case IPC_STAT: {
    struct semid64_ds buf {};
    Result = ::syscall(SYSCALL_DEF(semctl), semid, semnum, cmd, &buf);
    if (Result != -1) {
      *semun.buf = buf;
    }
    break;
  }
  case SEM_INFO:
  case IPC_INFO: {
    struct fex_seminfo si {};
    Result = ::syscall(SYSCALL_DEF(semctl), semid, semnum, cmd, &si);
    if (Result != -1) {
      memcpy(semun.__buf, &si, sizeof(si));
    }
    break;
  }
  case GETALL:
  case SETALL: {
    // ptr is just a int32_t* in this case
    Result = ::syscall(SYSCALL_DEF(semctl), semid, semnum, cmd, semun.array);
    break;
  }
  case SETVAL: {
    // ptr is just a int32_t in this case
    Result = ::syscall(SYSCALL_DEF(semctl), semid, semnum, cmd, semun.val);
    break;
  }
  case IPC_RMID:
  case GETPID:
  case GETNCNT:
  case GETZCNT:
  case GETVAL:
    Result = ::syscall(SYSCALL_DEF(semctl), semid, semnum, cmd, semun);
    break;
  default:
    LOGMAN_MSG_A_FMT("Unhandled semctl cmd: {}", cmd);
    return -EINVAL;
  }
  SYSCALL_ERRNO();
}
