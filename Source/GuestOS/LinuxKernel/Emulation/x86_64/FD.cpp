/*
$info$
tags: LinuxSyscalls|syscalls-x86-64
$end_info$
*/

#include "GuestOS/LinuxKernel/Helpers/FileManagement.h"
#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Types.h"

#include "Common/CompilerDefs.h"
#include "Common/Utils.h"

#include <fcntl.h>
#include <poll.h>
#include <stdint.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/statfs.h>
#include <sys/time.h>
#include <sys/uio.h>
#include <sys/sendfile.h>
#include <sys/timerfd.h>
#include <syscall.h>
#include <time.h>
#include <unistd.h>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

GUEST_SYSCALL_x86_64(fcntl)
{
  uint64_t Result{};
  switch (cmd)
  {
  case F_GETFL:
    Result = ::fcntl(fd, cmd, arg);
    if (Result != -1)
    {
      Result = FEX::HLE::RemapToX86Flags(Result);
    }
    break;
  case F_SETFL:
    Result = ::fcntl(fd, cmd, FEX::HLE::RemapFromX86Flags(arg));
    break;
  default:
    Result = ::fcntl(fd, cmd, arg);
    break;
  }
  SYSCALL_ERRNO();
}

GUEST_SYSCALL_x86_64(stat)
{
  struct stat host_stat;
  uint64_t Result = FEX::HLE::_SyscallHandler->FM.Stat(pathname, &host_stat);
  if (Result != -1)
  {
    *buf = host_stat;
  }
  SYSCALL_ERRNO();
}

GUEST_SYSCALL_x86_64(fstat)
{
  struct stat host_stat;
  uint64_t Result = ::fstat(fd, &host_stat);
  if (Result != -1)
  {
    *buf = host_stat;
  }
  SYSCALL_ERRNO();
}

GUEST_SYSCALL_x86_64(lstat)
{
  struct stat host_stat;
  uint64_t Result = FEX::HLE::_SyscallHandler->FM.Lstat(path, &host_stat);
  if (Result != -1)
  {
    *buf = host_stat;
  }
  SYSCALL_ERRNO();
}

GUEST_SYSCALL_x86_64(newfstatat)
{
  struct stat host_stat;
  uint64_t Result = FEX::HLE::_SyscallHandler->FM.NewFSStatAt(dirfd, pathname, &host_stat, flag);
  if (Result != -1)
  {
    *buf = host_stat;
  }
  SYSCALL_ERRNO();
}

GUEST_SYSCALL_x86_64(getdents)
{
  return GetDentsEmulation<false>(fd, reinterpret_cast<FEX::HLE::x64::linux_dirent *>(dirp), count);
}

// These don't map 1:1, so glibc helpers for now
GUEST_SYSCALL_x86_64(poll)
{
  uint64_t Result = ::poll(fds, nfds, timeout);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL_x86_64(select)
{
  uint64_t Result = ::select(nfds, readfds, writefds, exceptfds, timeout);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL_x86_64(futimesat)
{
  uint64_t Result = ::futimesat(dirfd, pathname, times);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL_x86_64(dup2) {
  uint64_t Result = ::dup2(oldfd, newfd);
  SYSCALL_ERRNO();
}

GUEST_SYSCALL_x86_64(dup3) {
  auto FlagsHost = FEX::HLE::RemapFromX86Flags(flags);
  uint64_t Result = ::dup3(oldfd, newfd, FlagsHost);
  SYSCALL_ERRNO();
}