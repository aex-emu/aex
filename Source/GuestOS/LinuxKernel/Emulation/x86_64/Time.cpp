/*
$info$
tags: LinuxSyscalls|syscalls-x86-64
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Types.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"

#include <stddef.h>
#include <stdint.h>
#include <time.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/timex.h>
#include <unistd.h>
#include <utime.h>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

// These don't map 1:1 so glib wrappers it is, for now

GUEST_SYSCALL_x86_64(utime)
{
  uint64_t Result = ::utime(filename, times_);

  SYSCALL_ERRNO();
}

GUEST_SYSCALL_x86_64(time)
{
  uint64_t Result = ::time(tloc);
  SYSCALL_ERRNO();
}

GUEST_SYSCALL_x86_64(utimes)
{
  uint64_t Result = ::utimes(filename, times_);
  SYSCALL_ERRNO();
}