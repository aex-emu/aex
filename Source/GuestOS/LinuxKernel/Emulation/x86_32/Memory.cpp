/*
$info$
tags: LinuxSyscalls|syscalls-x86-32
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestCPU/Context/Context.h"
#include "GuestCPU/Context/GuestState.h"
#include "GuestCPU/Context/InternalThreadState.h"

#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <system_error>
#include <filesystem>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

// the actual mappings don't have exec to prevent accidentally running code
// TODO: Don't duplicate this
static int MAP_MMAN_PROT(int prot) {
  return prot & ~PROT_EXEC;
}

namespace FEX::HLE::x32 {

  void *x32SyscallHandler::GuestMmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset) {
    auto lk = FEX::HLE::_SyscallHandler->LockMmanAndTranslation();

    LOGMAN_THROW_AA_FMT((length >> 32) == 0, "values must fit to 32 bits");

    auto Result = (uint64_t)GetAllocator()->mmap((void*)addr, length, MAP_MMAN_PROT(prot), flags, fd, offset);

    LOGMAN_THROW_AA_FMT((Result >> 32) == 0|| (Result >> 32) == 0xFFFFFFFF, "values must fit to 32 bits");

    if (!FEX::HLE::HasSyscallError(Result)) {
      FEX::HLE::_SyscallHandler->TrackMmap(Result, length, prot, flags, fd, offset);
      return (void *)Result;
    } else {
      errno = -Result;
      return MAP_FAILED;
    }
  }

  int x32SyscallHandler::GuestMunmap(void *addr, uint64_t length) {
    auto lk = FEX::HLE::_SyscallHandler->LockMmanAndTranslation();
    
    LOGMAN_THROW_AA_FMT((uintptr_t(addr) >> 32) == 0, "values must fit to 32 bits");
    LOGMAN_THROW_AA_FMT((length >> 32) == 0, "values must fit to 32 bits");

    auto Result = GetAllocator()->munmap(addr, length);

    if (Result == 0) {
      FEX::HLE::_SyscallHandler->TrackMunmap((uintptr_t)addr, length);
      return Result;
    } else {
      errno = -Result;
      return -1;
    }
  }
}

//@REGEX_DECL_SYSCALL_IMPL_X32("mmap old_mmap_struct const* arg")@
GUEST_SYSCALL_x86_32(mmap) {
  uint64_t Result = (uint64_t)static_cast<FEX::HLE::x32::x32SyscallHandler*>(FEX::HLE::_SyscallHandler)->
    GuestMmap(reinterpret_cast<void*>(arg->addr), arg->len, arg->prot, arg->flags, arg->fd, arg->offset);

  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("mmap2 uint32_t addr, uint32_t length, int prot, int flags, int fd, uint32_t pgoffset")@
GUEST_SYSCALL_x86_32(mmap2) {
  uint64_t Result = (uint64_t)static_cast<FEX::HLE::x32::x32SyscallHandler*>(FEX::HLE::_SyscallHandler)->
    GuestMmap(reinterpret_cast<void*>(addr), length, prot, flags, fd, (uint64_t)pgoffset * 0x1000);

  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("munmap void *addr, size_t length")@
GUEST_SYSCALL_x86_32(munmap) {
  uint64_t Result = (uint64_t)static_cast<FEX::HLE::x32::x32SyscallHandler*>(FEX::HLE::_SyscallHandler)->
    GuestMunmap(addr, length);

  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("mprotect void *addr, uint32_t len, int prot")@
GUEST_SYSCALL_x86_32(mprotect) {
  auto lk = FEX::HLE::_SyscallHandler->LockMmanAndTranslation();

  uint64_t Result = ::mprotect(addr, len, MAP_MMAN_PROT(prot));
  if (Result != -1) {
    FEX::HLE::_SyscallHandler->TrackMprotect((uintptr_t)addr, len, prot);
  }

  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("mremap void *old_address, size_t old_size, size_t new_size, int flags, void *new_address")@
GUEST_SYSCALL_x86_32(mremap) {
  auto lk = FEX::HLE::_SyscallHandler->LockMmanAndTranslation();

  uint64_t Result = reinterpret_cast<uint64_t>(static_cast<FEX::HLE::x32::x32SyscallHandler*>(FEX::HLE::_SyscallHandler)->GetAllocator()->
    mremap(old_address, old_size, new_size, flags, new_address));

  if (!FEX::HLE::HasSyscallError(Result)) {
    FEX::HLE::_SyscallHandler->TrackMremap((uintptr_t)old_address, old_size, new_size, flags, Result);
  }

  return Result;
}


//@REGEX_DECL_SYSCALL_IMPL_X32("mlockall int flags")@
GUEST_SYSCALL_x86_32(mlockall) {
  uint64_t Result = ::syscall(SYSCALL_DEF(mlock2), reinterpret_cast<void*>(0x1'0000), 0x1'0000'0000ULL - 0x1'0000, flags);
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("shmat int shmid, const void *shmaddr, int shmflg")@
GUEST_SYSCALL_x86_32(shmat) {
  auto lk = FEX::HLE::_SyscallHandler->LockMmanAndTranslation();

  // also implemented in ipc:OP_SHMAT
  uint32_t ResultAddr{};
  uint64_t Result = static_cast<FEX::HLE::x32::x32SyscallHandler*>(FEX::HLE::_SyscallHandler)->GetAllocator()->
    shmat(shmid, reinterpret_cast<const void*>(shmaddr), shmflg, &ResultAddr);

  if (!FEX::HLE::HasSyscallError(Result)) {
    FEX::HLE::_SyscallHandler->TrackShmat(shmid, ResultAddr, shmflg);
    return ResultAddr;
  } else {
    return Result;
  }
}


//@REGEX_DECL_SYSCALL_IMPL_X32("shmdt const void *shmaddr")@
GUEST_SYSCALL_x86_32(shmdt) {
  auto lk = FEX::HLE::_SyscallHandler->LockMmanAndTranslation();

  // also implemented in ipc:OP_SHMDT
  uint64_t Result = static_cast<FEX::HLE::x32::x32SyscallHandler*>(FEX::HLE::_SyscallHandler)->GetAllocator()->
    shmdt(shmaddr);

  if (!FEX::HLE::HasSyscallError(Result)) {
    FEX::HLE::_SyscallHandler->TrackShmdt((uintptr_t)shmaddr);
  }

  return Result;
}
