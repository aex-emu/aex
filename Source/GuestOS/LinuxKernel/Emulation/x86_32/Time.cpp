/*
$info$
tags: LinuxSyscalls|syscalls-x86-32
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Types.h"

#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"

#include <stdint.h>
#include <syscall.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/timex.h>
#include <time.h>
#include <unistd.h>
#include <utime.h>

ARG_TO_STR(FEX::HLE::x32::compat_ptr<FEX::HLE::x32::timespec32>, "%lx")
ARG_TO_STR(FEX::HLE::x32::compat_ptr<FEX::HLE::x32::timex32>, "%lx")

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

#include "Common/ForwardDeclarations.h"


//@REGEX_DECL_SYSCALL_IMPL_X32("time FEX::HLE::x32::old_time32_t *tloc")@
GUEST_SYSCALL_x86_32(time) {
  time_t Host{};
  uint64_t Result = ::time(&Host);

  if (tloc) {
    // On 32-bit this truncates
    *tloc = (FEX::HLE::x32::old_time32_t)Host;
  }

  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("times struct FEX::HLE::x32::compat_tms *buf")@
GUEST_SYSCALL_x86_32(times) {
  struct tms Host {};
  uint64_t Result = ::times(&Host);
  if (buf) {
    *buf = Host;
  }
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("utime char* filename, const FEX::HLE::x32::old_utimbuf32* times")@
GUEST_SYSCALL_x86_32(utime) {
  struct utimbuf Host {};
  struct utimbuf* Host_p{};
  if (times_) {
    Host = *times_;
    Host_p = &Host;
  }
  uint64_t Result = ::utime(filename, Host_p);
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("gettimeofday timeval32 *tv, struct timezone *tz")@
GUEST_SYSCALL_x86_32(gettimeofday) {
  struct timeval tv64 {};
  struct timeval* tv_ptr{};
  if (tv) {
    tv_ptr = &tv64;
  }

  uint64_t Result = ::gettimeofday(tv_ptr, tz);

  if (tv) {
    *tv = tv64;
  }
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("settimeofday const timeval32 *tv, const struct timezone *tz")@
GUEST_SYSCALL_x86_32(settimeofday) {
  struct timeval tv64 {};
  struct timeval* tv_ptr{};
  if (tv) {
    tv64 = *tv;
    tv_ptr = &tv64;
  }

  const uint64_t Result = ::settimeofday(tv_ptr, tz);
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("nanosleep const timespec32 *req, timespec32 *rem")@
GUEST_SYSCALL_x86_32(nanosleep) {
  struct timespec rem64 {};
  struct timespec* rem64_ptr{};

  if (rem) {
    rem64 = *rem;
    rem64_ptr = &rem64;
  }

  uint64_t Result = 0;
  if (req) {
    const struct timespec req64 = *req;
    Result = ::nanosleep(&req64, rem64_ptr);
  } else {
    Result = ::nanosleep(nullptr, rem64_ptr);
  }

  if (rem) {
    *rem = rem64;
  }
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("clock_gettime clockid_t clk_id, timespec32 *tp")@
GUEST_SYSCALL_x86_32(clock_gettime) {
  struct timespec tp64 {};
  uint64_t Result = ::clock_gettime(clk_id, &tp64);
  if (tp) {
    *tp = tp64;
  }
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("clock_getres clockid_t clk_id, timespec32 *tp")@
GUEST_SYSCALL_x86_32(clock_getres) {
  struct timespec tp64 {};
  uint64_t Result = ::clock_getres(clk_id, &tp64);
  if (tp) {
    *tp = tp64;
  }
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("clock_nanosleep clockid_t clockid, int flags, const timespec32 *request, timespec32 *remain")@
GUEST_SYSCALL_x86_32(clock_nanosleep) {
  struct timespec rem64 {};
  struct timespec* rem64_ptr{};

  if (remain) {
    rem64 = *remain;
    rem64_ptr = &rem64;
  }

  uint64_t Result = 0;
  if (request) {
    const struct timespec req64 = *request;
    Result = ::clock_nanosleep(clockid, flags, &req64, rem64_ptr);
  } else {
    Result = ::clock_nanosleep(clockid, flags, nullptr, rem64_ptr);
  }

  if (remain) {
    *remain = rem64;
  }
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("clock_settime clockid_t clockid, const timespec32 *tp")@
GUEST_SYSCALL_x86_32(clock_settime) {
  uint64_t Result = 0;
  if (tp) {
    const struct timespec tp64 = *tp;
    Result = ::clock_settime(clockid, &tp64);
  } else {
    Result = ::clock_settime(clockid, nullptr);
  }
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("futimesat int dirfd, const char *pathname, const timeval32 times[2]")@
GUEST_SYSCALL_x86_32(futimesat) {
  uint64_t Result = 0;
  if (times2) {
    struct timeval times64[2]{};
    times64[0] = times2[0];
    times64[1] = times2[1];
    
    Result = futimesat(dirfd, pathname, times64); //::syscall(SYSCALL_DEF(futimesat), dirfd, pathname, times64);
  } else {
    Result = futimesat(dirfd, pathname, nullptr); //::syscall(SYSCALL_DEF(futimesat), dirfd, pathname, nullptr);
  }
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("utimensat int dirfd, const char *pathname, const compat_ptr<timespec32> times_, int flags")@
GUEST_SYSCALL_x86_32(utimensat) {
  uint64_t Result = 0;
  if (times_) {
    timespec times64[2]{};
    times64[0] = times_[0];
    times64[1] = times_[1];
    Result = ::syscall(SYSCALL_DEF(utimensat), dirfd, pathname, times64, flags);
  } else {
    Result = ::syscall(SYSCALL_DEF(utimensat), dirfd, pathname, nullptr, flags);
  }
  SYSCALL_ERRNO();
}

//@REGEX_DECL_SYSCALL_IMPL_X32("utimes const char *filename, const timeval32 times[2]")@
GUEST_SYSCALL_x86_32(utimes) {
  uint64_t Result = 0;
  if (times2) {
    struct timeval times64[2]{};
    times64[0] = times2[0];
    times64[1] = times2[1];
    Result = ::utimes(filename, times64);
  } else {
    Result = ::utimes(filename, nullptr);
  }
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("adjtimex compat_ptr<FEX::HLE::x32::timex32> buf")@
GUEST_SYSCALL_x86_32(adjtimex) {
  struct timex Host {};
  Host = *buf;
  uint64_t Result = ::adjtimex(&Host);
  if (Result != -1) {
    *buf = Host;
  }
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("clock_adjtime clockid_t clk_id, compat_ptr<FEX::HLE::x32::timex32> buf")@
GUEST_SYSCALL_x86_32(clock_adjtime) {
  struct timex Host {};
  Host = *buf;
  uint64_t Result = ::clock_adjtime(clk_id, &Host);
  if (Result != -1) {
    *buf = Host;
  }
  SYSCALL_ERRNO();
}
