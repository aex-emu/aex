/*
$info$
tags: LinuxSyscalls|syscalls-x86-32
$end_info$
*/

#pragma once

#include "GuestOS/LinuxKernel/Syscalls.h"
#include <memory>
#include <stddef.h>
#include <stdint.h>
#include <sys/types.h>

#include "Common/ForwardDeclarations.h"

namespace FEX::HLE::x32 {

class x32SyscallHandler final : public FEX::HLE::SyscallHandler {
public:
  x32SyscallHandler(FEXCore::Context::Context *ctx, FEX::HLE::SignalDelegator *_SignalDelegation, std::unique_ptr<MemAllocator> Allocator);

  FEX::HLE::MemAllocator *GetAllocator() { return AllocHandler.get(); }
  void *GuestMmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset) override;
  int GuestMunmap(void *addr, uint64_t length) override;
#if 0
  void RegisterSyscall_32(int SyscallNumber,
    int32_t HostSyscallNumber,
    FEXCore::IR::SyscallFlags Flags,
#ifdef DEBUG_STRACE
    const std::string& TraceFormatString,
#endif
    void* SyscallHandler, int ArgumentCount) override {
    auto &Def = Definitions.at(SyscallNumber);
#if defined(ASSERTIONS_ENABLED) && ASSERTIONS_ENABLED
    auto cvt = [](auto in) {
      union {
        decltype(in) val;
        void *raw;
      } raw;
      raw.val = in;
      return raw.raw;
    };
    LOGMAN_THROW_A_FMT(Def.Ptr == cvt(&UnimplementedSyscall), "Oops overwriting syscall problem, {}", SyscallNumber);
#endif
    Def.Ptr = SyscallHandler;
    Def.NumArgs = ArgumentCount;
    Def.Flags = Flags;
    Def.HostSyscallNumber = HostSyscallNumber;
#ifdef DEBUG_STRACE
    Def.StraceFmt = TraceFormatString;
#endif
  }
  #endif

private:
  void RegisterSyscallHandlers();
  std::unique_ptr<MemAllocator> AllocHandler{};
};

std::unique_ptr<FEX::HLE::SyscallHandler> CreateHandler(FEXCore::Context::Context *ctx,
                                                        FEX::HLE::SignalDelegator *_SignalDelegation,
                                                        std::unique_ptr<MemAllocator> Allocator);
}