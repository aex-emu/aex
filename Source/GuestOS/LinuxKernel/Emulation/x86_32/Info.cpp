/*
$info$
tags: LinuxSyscalls|syscalls-x86-32
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Types.h"

#include <algorithm>
#include <asm/posix_types.h>
#include <limits>
#include <linux/utsname.h>
#include <stdint.h>
#include <sys/resource.h>
#include <sys/sysinfo.h>
#include <sys/utsname.h>

#include "Generated/git_version.h"

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

#include "Common/ForwardDeclarations.h"

ARG_TO_STR(FEX::HLE::x32::compat_ptr<FEX::HLE::x32::rlimit32<true>>, "%lx")
ARG_TO_STR(FEX::HLE::x32::compat_ptr<FEX::HLE::x32::rlimit32<false>>, "%lx")

//@REGEX_DECL_SYSCALL_IMPL_X32("oldolduname struct oldold_utsname *buf")@
GUEST_SYSCALL_x86_32(oldolduname) {
  struct utsname Local {};

  memset(buf, 0, sizeof(*buf));
  if (::uname(&Local) == 0) {
    memcpy(buf->nodename, Local.nodename, __OLD_UTS_LEN);
  } else {
    strncpy(buf->nodename, "FEXCore", __OLD_UTS_LEN);
    LogMan::Msg::EFmt("Couldn't determine host nodename. Defaulting to '{}'", buf->nodename);
  }
  strncpy(buf->sysname, "Linux", __OLD_UTS_LEN);
  uint32_t GuestVersion = FEX::HLE::_SyscallHandler->GetGuestKernelVersion();
  snprintf(buf->release, __OLD_UTS_LEN, "%d.%d.%d",
    FEX::HLE::SyscallHandler::KernelMajor(GuestVersion),
    FEX::HLE::SyscallHandler::KernelMinor(GuestVersion),
    FEX::HLE::SyscallHandler::KernelPatch(GuestVersion));

  const char version[] = "#" GIT_DESCRIBE_STRING " SMP " __DATE__ " " __TIME__;
  strncpy(buf->version, version, __OLD_UTS_LEN);
  // Tell the guest that we are a 64bit kernel
  strncpy(buf->machine, "x86_64", __OLD_UTS_LEN);
  return 0;
}


//@REGEX_DECL_SYSCALL_IMPL_X32("olduname struct old_utsname *buf")@
GUEST_SYSCALL_x86_32(olduname) {
  struct utsname Local {};

  memset(buf, 0, sizeof(*buf));
  if (::uname(&Local) == 0) {
    memcpy(buf->nodename, Local.nodename, __NEW_UTS_LEN);
  } else {
    strncpy(buf->nodename, "FEXCore", __NEW_UTS_LEN);
    LogMan::Msg::EFmt("Couldn't determine host nodename. Defaulting to '{}'", buf->nodename);
  }
  strncpy(buf->sysname, "Linux", __NEW_UTS_LEN);
  uint32_t GuestVersion = FEX::HLE::_SyscallHandler->GetGuestKernelVersion();
  snprintf(buf->release, __NEW_UTS_LEN, "%d.%d.%d",
    FEX::HLE::SyscallHandler::KernelMajor(GuestVersion),
    FEX::HLE::SyscallHandler::KernelMinor(GuestVersion),
    FEX::HLE::SyscallHandler::KernelPatch(GuestVersion));

  const char version[] = "#" GIT_DESCRIBE_STRING " SMP " __DATE__ " " __TIME__;
  strncpy(buf->version, version, __NEW_UTS_LEN);
  // Tell the guest that we are a 64bit kernel
  strncpy(buf->machine, "x86_64", __NEW_UTS_LEN);
  return 0;
}


//@REGEX_DECL_SYSCALL_IMPL_X32("getrlimit int resource, compat_ptr<FEX::HLE::x32::rlimit32<true>> rlim")@
GUEST_SYSCALL_x86_32(getrlimit) {
  struct rlimit rlim64 {};
  uint64_t Result = ::getrlimit(resource, &rlim64);
  *rlim = rlim64;
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("ugetrlimit int resource, compat_ptr<FEX::HLE::x32::rlimit32<false>> rlim")@
GUEST_SYSCALL_x86_32(ugetrlimit) {
  struct rlimit rlim64 {};
  uint64_t Result = ::getrlimit(resource, &rlim64);
  *rlim = rlim64;
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("setrlimit int resource, const compat_ptr<FEX::HLE::x32::rlimit32<false>> rlim")@
GUEST_SYSCALL_x86_32(setrlimit) {
  struct rlimit rlim64 {};
  rlim64 = *rlim;
  uint64_t Result = ::setrlimit(resource, &rlim64);
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("sysinfo struct sysinfo32 *info")@
GUEST_SYSCALL_x86_32(sysinfo) {
  struct sysinfo Host {};
  uint64_t Result = ::sysinfo(&Host);
  if (Result != -1) {
#define Copy(x) info->x = static_cast<decltype(info->x)>(std::min(Host.x, static_cast<decltype(Host.x)>(std::numeric_limits<decltype(info->x)>::max())));
    Copy(uptime);
    Copy(procs);
#define CopyShift(x) info->x = static_cast<decltype(info->x)>(Host.x >> ShiftAmount);

    info->loads[0] = std::min(Host.loads[0], static_cast<unsigned long>(std::numeric_limits<uint32_t>::max()));
    info->loads[1] = std::min(Host.loads[1], static_cast<unsigned long>(std::numeric_limits<uint32_t>::max()));
    info->loads[2] = std::min(Host.loads[2], static_cast<unsigned long>(std::numeric_limits<uint32_t>::max()));

    // If any result can't fit in to a uint32_t then we need to shift the mem_unit and all the members
    // Set the mem_unit to the pagesize
    uint32_t ShiftAmount{};
    if ((Host.totalram >> 32) != 0 ||
      (Host.totalswap >> 32) != 0) {

      while (Host.mem_unit < 4096) {
        Host.mem_unit <<= 1;
        ++ShiftAmount;
      }
    }

    CopyShift(totalram);
    CopyShift(sharedram);
    CopyShift(bufferram);
    CopyShift(totalswap);
    CopyShift(freeswap);
    CopyShift(totalhigh);
    CopyShift(freehigh);
    Copy(mem_unit);
  }
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("getrusage int who, rusage_32 *usage")@
GUEST_SYSCALL_x86_32(getrusage) {
  struct rusage usage64 = *usage;
  uint64_t Result = ::getrusage(who, &usage64);
  *usage = usage64;
  SYSCALL_ERRNO();
}
