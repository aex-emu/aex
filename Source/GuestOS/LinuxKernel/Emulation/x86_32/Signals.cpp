/*
$info$
tags: LinuxSyscalls|syscalls-x86-32
$end_info$
*/

#include"GuestOS/LinuxKernel/Helpers/SignalDelegator.h"
#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Types.h"

#include "SignalDelegator/SignalDelegator.h"
#include "GuestOS/Misc/UContext.h"
#include <errno.h>
#include <signal.h>
#include <stdint.h>
#include <sys/syscall.h>
#include <unistd.h>

#include <time.h>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

#include "Common/ForwardDeclarations.h"

#include "Common/ThreadContext.h"

ARG_TO_STR(FEX::HLE::x32::compat_ptr<FEXCore::x86::siginfo_t>, "%lx")

namespace FEX::HLE::x32 {
  void CopySigInfo(FEXCore::x86::siginfo_t *Info, siginfo_t const &Host) {
    // Copy the basic things first
    Info->si_signo = Host.si_signo;
    Info->si_errno = Host.si_errno;
    Info->si_code = Host.si_code;

    // Check si_code to determine how we need to interpret this
    if (Info->si_code == SI_TIMER) {
      // SI_TIMER means pid, uid, value
      Info->_sifields._timer.tid     = Host.si_timerid;
      Info->_sifields._timer.overrun = Host.si_overrun;
      Info->_sifields._timer.sigval.sival_int = Host.si_value.sival_int;
    }
    else {
      // Now we need to copy over the more complex things
      switch (Info->si_signo) {
        case SIGSEGV:
        case SIGBUS:
          // This is the address trying to be accessed, not the RIP
          Info->_sifields._sigfault.addr = static_cast<uint32_t>(reinterpret_cast<uintptr_t>(Host.si_addr));
          break;
        case SIGFPE:
        case SIGILL:
          // Can't really give a real result here. This is the RIP causing a sigill or sigfpe
          // Claim at RIP 0 for now
          Info->_sifields._sigfault.addr = 0;
          break;
        case SIGCHLD:
          Info->_sifields._sigchld.pid    = Host.si_pid;
          Info->_sifields._sigchld.uid    = Host.si_uid;
          Info->_sifields._sigchld.status = Host.si_status;
          Info->_sifields._sigchld.utime  = Host.si_utime;
          Info->_sifields._sigchld.stime  = Host.si_stime;
          break;
        case SIGALRM:
        case SIGVTALRM:
          Info->_sifields._timer.tid              = Host.si_timerid;
          Info->_sifields._timer.overrun          = Host.si_overrun;
          Info->_sifields._timer.sigval.sival_int = Host.si_int;
          break;
        default:
          LogMan::Msg::EFmt("Unhandled siginfo_t for sigtimedwait: {}", Info->si_signo);
          break;
      }
    }
  }
}

GUEST_SYSCALL_x86_32(rt_sigreturn) {
  auto Frame = GetThreadBound<FEXCore::Core::CpuStateFrame>();

  _SyscallHandler->GetSignalDelegator()->RestoreSigFrame32(Frame, true);

  // TODO: DOCUMENT THIS BETTER
  // this is a magic value that might mean "incomplete", and was meant to be used for when a signal had interrupted
  // the syscall, and a handler was to be executed
  //
  // However, this works just fine for here as well, as we just want to re-dispatch without adjusting RIP or RAX
  // Make part of the spec? use another special value?
  //
  return UINT64_MAX - 4095;
}

GUEST_SYSCALL_x86_32(sigreturn) {
  // TODO: Kernel uses different sigframe here
  //ERROR_AND_DIE_FMT("sigreturn not implemented");
    auto Frame = GetThreadBound<FEXCore::Core::CpuStateFrame>();

  _SyscallHandler->GetSignalDelegator()->RestoreSigFrame32(Frame, false);

  // TODO: DOCUMENT THIS BETTER
  // this is a magic value that might mean "incomplete", and was meant to be used for when a signal had interrupted
  // the syscall, and a handler was to be executed
  //
  // However, this works just fine for here as well, as we just want to re-dispatch without adjusting RIP or RAX
  // Make part of the spec? use another special value?
  //
  return UINT64_MAX - 4095;
}

// Only gets the lower 32-bits of the signal mask

//@REGEX_DECL_SYSCALL_IMPL_X32("sgetmask nullptr, &Set")@
GUEST_SYSCALL_x86_32(sgetmask) {
  uint64_t Set{};
  uint64_t NewSet = (~0ULL << 32) | newmask;
  FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSigProcMask(SIG_SETMASK, &NewSet, &Set, sizeof(NewSet));
  return Set & ~0U;
}

// Only masks the lower 32-bits of the signal mask
// The upper 32-bits are still active (unmasked) and can signal the program

//@REGEX_DECL_SYSCALL_IMPL_X32("sigsuspend uint32_t Mask")@
GUEST_SYSCALL_x86_32(sigsuspend) {
  uint64_t Mask64 = Mask;
  return FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSigSuspend(&Mask64, 8);
}


//@REGEX_DECL_SYSCALL_IMPL_X32("sigpending compat_old_sigset_t *set")@
GUEST_SYSCALL_x86_32(sigpending) {
  uint64_t HostSet{};
  uint64_t Result = FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSigPending(&HostSet, 8);
  if (Result == 0) {
    // This old interface only returns the lower signals
    *set = HostSet & ~0U;
  }
  return Result;
}


//@REGEX_DECL_SYSCALL_IMPL_X32("signal int signum, uint32_t handler")@
GUEST_SYSCALL_x86_32(signal) {
  FEXCore::GuestSigAction newact{};
  FEXCore::GuestSigAction oldact{};
  newact.sigaction_handler.handler = reinterpret_cast<decltype(newact.sigaction_handler.handler)>(handler);
  FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSigAction(signum, &newact, &oldact);
  return static_cast<uint32_t>(reinterpret_cast<uint64_t>(oldact.sigaction_handler.handler));
}


//@REGEX_DECL_SYSCALL_IMPL_X32("sigaction int signum, const OldGuestSigAction_32 *act, OldGuestSigAction_32 *oldact")@
GUEST_SYSCALL_x86_32(sigaction) {
  FEXCore::GuestSigAction* act64_p{};
  FEXCore::GuestSigAction* old64_p{};

  FEXCore::GuestSigAction act64{};
  if (act) {
    act64 = *act;
    act64_p = &act64;
  }
  FEXCore::GuestSigAction old64{};

  if (oldact) {
    old64_p = &old64;
  }

  uint64_t Result = FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSigAction(signum, act64_p, old64_p);
  if (Result == 0 && oldact) {
    *oldact = old64;
  }

  return Result;
}


//@REGEX_DECL_SYSCALL_IMPL_X32("rt_sigaction int signum, const GuestSigAction_32 *act, GuestSigAction_32 *oldact, size_t sigsetsize")@
GUEST_SYSCALL_x86_32(rt_sigaction) {
  if (sigsetsize != 8) {
    return -EINVAL;
  }

  FEXCore::GuestSigAction* act64_p{};
  FEXCore::GuestSigAction* old64_p{};

  FEXCore::GuestSigAction act64{};
  if (act) {
    act64 = *act;
    act64_p = &act64;
  }
  FEXCore::GuestSigAction old64{};

  if (oldact) {
    old64_p = &old64;
  }

  uint64_t Result = FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSigAction(signum, act64_p, old64_p);
  if (Result == 0 && oldact) {
    *oldact = old64;
  }

  return Result;
}


//@REGEX_DECL_SYSCALL_IMPL_X32("rt_sigtimedwait uint64_t *set, compat_ptr<FEXCore::x86::siginfo_t> info, const struct timespec32* timeout, size_t sigsetsize")@
GUEST_SYSCALL_x86_32(rt_sigtimedwait) {
  struct timespec* timeout_ptr{};
  struct timespec tp64 {};
  if (timeout) {
    tp64 = *timeout;
    timeout_ptr = &tp64;
  }

  siginfo_t HostInfo{};
  uint64_t Result = FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSigTimedWait(set, &HostInfo, timeout_ptr, sigsetsize);
  if (Result != -1) {
    // We need to translate the 64-bit siginfo_t to 32-bit siginfo_t
    CopySigInfo(info, HostInfo);
  }
  return Result;
}



//@REGEX_DECL_SYSCALL_IMPL_X32("rt_sigtimedwait_time64 uint64_t *set, compat_ptr<FEXCore::x86::siginfo_t> info, const struct timespec* timeout, size_t sigsetsize")@
GUEST_SYSCALL_x86_32(rt_sigtimedwait_time64) {
  siginfo_t HostInfo{};
  uint64_t Result = FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSigTimedWait(set, &HostInfo, timeout, sigsetsize);
  if (Result != -1) {
    // We need to translate the 64-bit siginfo_t to 32-bit siginfo_t
    CopySigInfo(info, HostInfo);
  }
  return Result;
}

//@REGEX_DECL_SYSCALL_IMPL_X32("pidfd_send_signal int pidfd, int sig, compat_ptr<FEXCore::x86::siginfo_t> info, unsigned int flags")@
GUEST_SYSCALL_x86_32(pidfd_send_signal) {
  siginfo_t* InfoHost_ptr{};
  siginfo_t InfoHost{};
  if (info) {
    InfoHost = *info;
    InfoHost_ptr = &InfoHost;
  }

  uint64_t Result = ::syscall(SYSCALL_DEF(pidfd_send_signal), pidfd, sig, InfoHost_ptr, flags);
  SYSCALL_ERRNO();
}

//@REGEX_DECL_SYSCALL_IMPL_X32("rt_sigqueueinfo pid_t pid, int sig, compat_ptr<FEXCore::x86::siginfo_t> info")@
GUEST_SYSCALL_x86_32(rt_sigqueueinfo) {
  siginfo_t info64{};
  siginfo_t* info64_p{};

  if (info) {
    info64_p = &info64;
  }

  uint64_t Result = ::syscall(SYSCALL_DEF(rt_sigqueueinfo), pid, sig, info64_p);
  SYSCALL_ERRNO();
}


// TODO: This was marked PASS for some odd reason?
//@REGEX_DECL_SYSCALL_IMPL_X32("rt_tgsigqueueinfo pid_t tgid, pid_t tid, int sig, compat_ptr<FEXCore::x86::siginfo_t> info")@
GUEST_SYSCALL_x86_32(rt_tgsigqueueinfo) {
  siginfo_t info64{};
  siginfo_t* info64_p{};

  if (info) {
    info64_p = &info64;
  }

  uint64_t Result = ::syscall(SYSCALL_DEF(rt_tgsigqueueinfo), tgid, tid, sig, info64_p);
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("sigprocmask int how, const uint64_t *set, uint64_t *oldset, size_t sigsetsize")@
GUEST_SYSCALL_x86_32(sigprocmask) {
  return FEX::HLE::_SyscallHandler->GetSignalDelegator()->GuestSigProcMask(how, set, oldset, sigsetsize);
}
