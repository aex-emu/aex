/*
$info$
tags: LinuxSyscalls|syscalls-x86-32
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/ioctl32/IoctlEmulation.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"
#include "GuestOS/LinuxKernel/Meta/x86_32/SyscallsEnum.h"

#include "GuestOS/Misc/SyscallHandler.h"

#include <map>
#include <mutex>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

#include "Generated/SyscallGen-x86_32/SyscallAuto.inl"

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

#include "Common/ForwardDeclarations.h"

namespace x86_32 {
  template<int GuestSyscallNumber>
  uint64_t GuestSyscallRouter(uint64_t a0, uint64_t a1, uint64_t a2, uint64_t a3, uint64_t a4, uint64_t a5) {
    const GuestSyscallHandler<GuestSyscallNumber> handler { a0, a1, a2, a3, a4, a5 };
    return handler.handle();
  }

  template<int GuestSyscallNumber>
  uint64_t UknownSyscallRouter(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t) {
    LogMan::Msg::EFmt("**** x86_32::UknownSyscallRouter {} ****", GuestSyscallNumber);
    return -ENOSYS;
  }

  template<int GuestSyscallNumber>
  uint64_t TooOldHostKernelRouter(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t) {
    LogMan::Msg::IFmt("**** TooOldHostKernelRouter **** {}", GuestSyscallNumber);
    return -ENOSYS;
  }

  template<int GuestSyscallNumber>
  uint64_t StubRouter(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t) {
    LogMan::Msg::IFmt("**** StubRouter ****", GuestSyscallNumber);
    return -ENOSYS;
  }
}

namespace x86_64 {
  template<int GuestSyscallNumber>
  uint64_t GuestSyscallRouter(uint64_t a0, uint64_t a1, uint64_t a2, uint64_t a3, uint64_t a4, uint64_t a5);
}


static constexpr std::array<GuestSyscallHandlerFunctionType *, SYSCALL_x86_64_MAX> InitializeUnknownSyscallRouter() {
  #define DECL_SYSCALL(num) &x86_32::UknownSyscallRouter<num>,
  return {
    #include "GuestOS/LinuxKernel/Host/SyscallDecls.inl"
  };
}

namespace FEX::HLE::x32 {


  x32SyscallHandler::x32SyscallHandler(FEXCore::Context::Context *ctx, FEX::HLE::SignalDelegator *_SignalDelegation, std::unique_ptr<MemAllocator> Allocator)
    : SyscallHandler{ctx, _SignalDelegation}, AllocHandler{std::move(Allocator)} {
    OSABI = FEXCore::HLE::SyscallOSABI::OS_LINUX32;
    RegisterSyscallHandlers();
  }

  void x32SyscallHandler::RegisterSyscallHandlers() {
    Definitions.resize(SYSCALL_x86_32_MAX);
    HandlerFunctions.resize(SYSCALL_x86_32_MAX);

    {

      auto rv = InitializeUnknownSyscallRouter();
      memcpy(HandlerFunctions.data(), rv.data(), HandlerFunctions.size() * sizeof(HandlerFunctions[0]));

      auto Handler = this;

      #define REGISTER_GUEST_SYSCALL_x86_32(arch, syscall) \
        HandlerFunctions[SYSCALL_x86_32_##syscall] = &arch::GuestSyscallRouter<SYSCALL_##arch##_##syscall>;

      #define REGISTER_GUEST_SYSCALL_x86_32_OLDKERNEL(syscall) \
        HandlerFunctions[SYSCALL_x86_32_##syscall] = &x86_32::TooOldHostKernelRouter<SYSCALL_x86_32_##syscall>;

      #define REGISTER_GUEST_SYSCALL_x86_32_STUB(syscall) \
        HandlerFunctions[SYSCALL_x86_32_##syscall] = &x86_32::StubRouter<SYSCALL_x86_32_##syscall>;

      #include "Generated/SyscallGen-x86_32/SyscallRouter.inl"
    }

    FEX::HLE::x32::InitializeStaticIoctlHandlers();

#if PRINT_MISSING_SYSCALLS
    for (auto &Syscall: SyscallNames) {
      if (Definitions[Syscall.first].Ptr == cvt(&UnimplementedSyscall)) {
        LogMan::Msg::DFmt("Unimplemented syscall: {}: {}", Syscall.first, Syscall.second);
      }
    }
#endif
  }

  std::unique_ptr<FEX::HLE::SyscallHandler> CreateHandler(FEXCore::Context::Context *ctx, FEX::HLE::SignalDelegator *_SignalDelegation, std::unique_ptr<MemAllocator> Allocator) {
    return std::make_unique<x32SyscallHandler>(ctx, _SignalDelegation, std::move(Allocator));
  }

}
