/*
$info$
tags: LinuxSyscalls|syscalls-x86-32
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Types.h"

#include <stdint.h>
#include <sched.h>
#include <time.h>
#include <unistd.h>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

#include "Common/ForwardDeclarations.h"

//@REGEX_DECL_SYSCALL_IMPL_X32("sched_rr_get_interval pid_t pid, struct timespec32 *tp")@
GUEST_SYSCALL_x86_32(sched_rr_get_interval) {
  struct timespec tp64 {};
  uint64_t Result = ::sched_rr_get_interval(pid, tp ? &tp64 : nullptr);
  if (tp) {
    *tp = tp64;
  }
  SYSCALL_ERRNO();
}
