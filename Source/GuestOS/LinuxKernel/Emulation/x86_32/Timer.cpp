/*
$info$
tags: LinuxSyscalls|syscalls-x86-32
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Types.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Types.h"

#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"

#include <stdint.h>
#include <syscall.h>
#include <sys/time.h>
#include <unistd.h>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

#include "Common/ForwardDeclarations.h"

ARG_TO_STR(FEX::HLE::x32::compat_ptr<FEX::HLE::x32::sigevent32>, "%lx")
  
//@REGEX_DECL_SYSCALL_IMPL_X32("timer_settime kernel_timer_t timerid, int flags, const FEX::HLE::x32::old_itimerspec32 *new_value, FEX::HLE::x32::old_itimerspec32 *old_value")@
GUEST_SYSCALL_x86_32(timer_settime) {
  itimerspec new_value_host{};
  itimerspec old_value_host{};
  itimerspec* old_value_host_p{};

  new_value_host = *new_value;
  if (old_value) {
    old_value_host_p = &old_value_host;
  }
  uint64_t Result = ::syscall(SYSCALL_DEF(timer_settime), timerid, flags, &new_value_host, old_value_host_p);
  if (Result != -1 && old_value) {
    *old_value = old_value_host;
  }
  SYSCALL_ERRNO();
}

//@REGEX_DECL_SYSCALL_IMPL_X32("timer_gettime kernel_timer_t timerid, FEX::HLE::x32::old_itimerspec32 *curr_value")@
GUEST_SYSCALL_x86_32(timer_gettime) {
  itimerspec curr_value_host{};
  uint64_t Result = ::syscall(SYSCALL_DEF(timer_gettime), timerid, curr_value_host);
  *curr_value = curr_value_host;
  SYSCALL_ERRNO();
}
    
//@REGEX_DECL_SYSCALL_IMPL_X32("getitimer int which, FEX::HLE::x32::itimerval32 *curr_value")@
GUEST_SYSCALL_x86_32(getitimer) {
  itimerval val{};
  itimerval* val_p{};
  if (curr_value) {
    val_p = &val;
  }
  uint64_t Result = ::getitimer(which, val_p);
  if (curr_value) {
    *curr_value = val;
  }
  SYSCALL_ERRNO();
}

    
//@REGEX_DECL_SYSCALL_IMPL_X32("setitimer int which, const FEX::HLE::x32::itimerval32 *new_value, FEX::HLE::x32::itimerval32 *old_value")@
GUEST_SYSCALL_x86_32(setitimer) {
  itimerval val{};
  itimerval old{};
  itimerval* val_p{};
  itimerval* old_p{};

  if (new_value) {
    val = *new_value;
    val_p = &val;
  }

  if (old_value) {
    old_p = &old;
  }

  uint64_t Result = ::setitimer(which, val_p, old_p);

  if (old_value) {
    *old_value = old;
  }
  SYSCALL_ERRNO();
}

    
//@REGEX_DECL_SYSCALL_IMPL_X32("timer_create clockid_t clockid, compat_ptr<FEX::HLE::x32::sigevent32> sevp, kernel_timer_t *timerid")@
GUEST_SYSCALL_x86_32(timer_create) {
  sigevent Host = *sevp;
  uint64_t Result = ::syscall(SYSCALL_DEF(timer_create), clockid, &Host, timerid);
  SYSCALL_ERRNO();
}
