/*
$info$
meta: LinuxSyscalls|syscalls-x86-32 ~ x86-32 specific syscall implementations
tags: LinuxSyscalls|syscalls-x86-32
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Types.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Types.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

#include <algorithm>
#include <cstdint>
#include <sys/epoll.h>
#include <syscall.h>
#include <time.h>
#include <unistd.h>
#include <vector>

#include "Common/ForwardDeclarations.h"

ARG_TO_STR(FEX::HLE::x32::compat_ptr<FEX::HLE::x32::epoll_event32>, "%lx")
ARG_TO_STR(FEX::HLE::x32::compat_ptr<FEX::HLE::x32::timespec32>, "%lx")



//@REGEX_DECL_SYSCALL_IMPL_X32("epoll_wait int epfd, compat_ptr<FEX::HLE::x32::epoll_event32> events, int maxevents, int timeout")@
GUEST_SYSCALL_x86_32(epoll_wait) {
  std::vector<struct epoll_event> Events(std::max(0, maxevents));
  uint64_t Result = ::syscall(SYSCALL_DEF(epoll_pwait), epfd, Events.data(), maxevents, timeout, nullptr, 8);

  if (Result != -1) {
    for (size_t i = 0; i < Result; ++i) {
      events[i] = Events[i];
    }
  }
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("epoll_ctl int epfd, int op, int fd, compat_ptr<FEX::HLE::x32::epoll_event32> event")@
GUEST_SYSCALL_x86_32(epoll_ctl) {
  struct epoll_event Event = *event;
  uint64_t Result = ::syscall(SYSCALL_DEF(epoll_ctl), epfd, op, fd, &Event);
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("epoll_pwait int epfd, compat_ptr<FEX::HLE::x32::epoll_event32> events, int maxevent, int timeout, const uint64_t* sigmask_, size_t sigsetsize")@
GUEST_SYSCALL_x86_32(epoll_pwait) {
  std::vector<struct epoll_event> Events(std::max(0, maxevent));

  uint64_t Result = ::syscall(SYSCALL_DEF(epoll_pwait),
    epfd,
    Events.data(),
    maxevent,
    timeout,
    sigmask_,
    sigsetsize);

  if (Result != -1) {
    for (size_t i = 0; i < Result; ++i) {
      events[i] = Events[i];
    }
  }

  SYSCALL_ERRNO();
}

//if (Handler->IsHostKernelVersionAtLeast(5, 11, 0)) {
//@REGEX_DECL_SYSCALL_IMPL_X32("epoll_pwait2 int epfd, compat_ptr<FEX::HLE::x32::epoll_event32> events, int maxevent, compat_ptr<timespec32> timeout, const uint64_t* sigmask_, size_t sigsetsize")@
GUEST_SYSCALL_x86_32(epoll_pwait2) {
  std::vector<struct epoll_event> Events(std::max(0, maxevent));

  struct timespec tp64 {};
  struct timespec* timed_ptr{};
  if (timeout) {
    tp64 = *timeout;
    timed_ptr = &tp64;
  }

  uint64_t Result = ::syscall(SYSCALL_DEF(epoll_pwait2),
    epfd,
    Events.data(),
    maxevent,
    timed_ptr,
    sigmask_,
    sigsetsize);

  if (Result != -1) {
    for (size_t i = 0; i < Result; ++i) {
      events[i] = Events[i];
    }
  }

  SYSCALL_ERRNO();
}
