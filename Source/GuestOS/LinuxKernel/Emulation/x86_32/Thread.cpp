/*
$info$
tags: LinuxSyscalls|syscalls-x86-32
$end_info$
*/

#include"GuestOS/LinuxKernel/Helpers/SignalDelegator.h"
#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Thread.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Types.h"

#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"

#include "GuestCPU/Context/GuestState.h"
#include "GuestOS/Misc/UContext.h"
#include "GuestCPU/Context/InternalThreadState.h"
#include "GuestOS/Misc/ThreadManagement.h"

#include <errno.h>
#include <grp.h>
#include <linux/futex.h>
#include <sched.h>
#include <signal.h>
#include <sys/fsuid.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <syscall.h>
#include <time.h>
#include <unistd.h>
#include <vector>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

#include "Common/ThreadContext.h"

ARG_TO_STR(FEX::HLE::x32::compat_ptr<FEX::HLE::x32::stack_t32>, "%x")
ARG_TO_STR(FEX::HLE::x32::compat_ptr<FEXCore::x86::siginfo_t>, "%x")

namespace FEX::HLE::x32 {
  // The kernel only gives 32-bit userspace 3 TLS segments
  // Depending on if the host kernel is 32-bit or 64-bit then the TLS index assigned is different
  //
  // Host kernel x86_64, valid TLS enries: 12,13,14
  // Host kernel x86, valid TLS enries: 6,7,8
  // Since we are claiming to be a 64-bit kernel, use the 64-bit range
  //
  // 6/12 = glibc
  // 7/13 = wine fs
  // 8/14 = etc
  constexpr uint32_t TLS_NextEntry = 12;
  constexpr uint32_t TLS_MaxEntry = TLS_NextEntry+3;

  uint64_t SetThreadArea(FEXCore::Core::CpuStateFrame *Frame, void *tls) {
    struct x32::user_desc* u_info = reinterpret_cast<struct x32::user_desc*>(tls);
    if (u_info->entry_number == -1) {
      for (uint32_t i = TLS_NextEntry; i < TLS_MaxEntry; ++i) {
        auto GDT = &Frame->State.gdt[i];
        if (GDT->base == 0) {
          // If the base is zero then it isn't present with our setup
          u_info->entry_number = i;
          break;
        }
      }

      if (u_info->entry_number == -1) {
        // Couldn't find a slot. Return empty handed
        return -ESRCH;
      }
    }

    // Now we need to update the thread's GDT to handle this change
    auto GDT = &Frame->State.gdt[u_info->entry_number];
    GDT->base = u_info->base_addr;
    return 0;
  }

  void AdjustRipForNewThread(FEXCore::Core::CpuStateFrame *Frame) {
    Frame->State.rip += 2;
  }
}

//@REGEX_DECL_SYSCALL_IMPL_X32("clone uint32_t flags, void *stack, pid_t *parent_tid, void *tls, pid_t *child_tid")@
GUEST_SYSCALL_x86_32(clone) {
  FEX::HLE::clone3_args args{
    .Type = TypeOfClone::TYPE_CLONE2,
    .args = {
      .flags = flags & ~CSIGNAL, // This no longer contains CSIGNAL
      .pidfd = reinterpret_cast<uint64_t>(parent_tid), // For clone, pidfd is duplicated here
      .child_tid = reinterpret_cast<uint64_t>(child_tid),
      .parent_tid = reinterpret_cast<uint64_t>(parent_tid),
      .exit_signal = flags & CSIGNAL,
      .stack = reinterpret_cast<uint64_t>(stack),
      .stack_size = 0, // This syscall isn't able to see the stack size
      .tls = reinterpret_cast<uint64_t>(tls),
      .set_tid = 0, // This syscall isn't able to select TIDs
      .set_tid_size = 0,
      .cgroup = 0, // This syscall can't select cgroups
    }
  };
  return CloneHandler(GetThreadBound<FEXCore::Core::CpuStateFrame>(), &args);
}


//@REGEX_DECL_SYSCALL_IMPL_X32("waitpid pid_t pid, int32_t *status, int32_t options")@
GUEST_SYSCALL_x86_32(waitpid) {
  uint64_t Result = ::waitpid(pid, status, options);
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("nice int inc")@
GUEST_SYSCALL_x86_32(nice) {
  uint64_t Result = ::nice(inc);
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("set_thread_area struct user_desc *u_info")@
GUEST_SYSCALL_x86_32(set_thread_area) {
  return SetThreadArea(GetThreadBound<FEXCore::Core::CpuStateFrame>(), u_info);
}


//@REGEX_DECL_SYSCALL_IMPL_X32("get_thread_area struct user_desc *u_info")@
GUEST_SYSCALL_x86_32(get_thread_area) {
  auto Frame = GetThreadBound<FEXCore::Core::CpuStateFrame>();

  // Index to fetch comes from the user_desc
  uint32_t Entry = u_info->entry_number;
  if (Entry < TLS_NextEntry || Entry > TLS_MaxEntry) {
    return -EINVAL;
  }

  const auto& GDT = &Frame->State.gdt[Entry];

  memset(u_info, 0, sizeof(*u_info));

  // FEX only stores base instead of the full GDT
  u_info->base_addr = GDT->base;

  // Fill the rest of the structure with expected data (even if wrong at the moment)
  if (u_info->base_addr) {
    u_info->limit = 0xF'FFFF;
    u_info->seg_32bit = 1;
    u_info->limit_in_pages = 1;
    u_info->useable = 1;
  } else {
    u_info->read_exec_only = 1;
    u_info->seg_not_present = 1;
  }
  return 0;
}


//@REGEX_DECL_SYSCALL_IMPL_X32("set_robust_list struct robust_list_head *head, size_t len")@
GUEST_SYSCALL_x86_32(set_robust_list) {
  if (len != 12) {
    return -EINVAL;
  }
  auto Thread = GetThreadBound<FEXCore::Core::InternalThreadState>();
  // Retain the robust list head but don't give it to the kernel
  // The kernel would break if it tried parsing a 32bit robust list from a 64bit process
  Thread->ThreadManager.robust_list_head = reinterpret_cast<uint64_t>(head);
  return 0;
}


//@REGEX_DECL_SYSCALL_IMPL_X32("get_robust_list int pid, struct robust_list_head **head, uint32_t *len_ptr")@
GUEST_SYSCALL_x86_32(get_robust_list) {
  auto Thread = GetThreadBound<FEXCore::Core::InternalThreadState>();
  // Give the robust list back to the application
  // Steam specifically checks to make sure the robust list is set
  *(uint32_t*)head = (uint32_t)Thread->ThreadManager.robust_list_head;
  *len_ptr = 12;
  return 0;
}


//@REGEX_DECL_SYSCALL_IMPL_X32("futex int *uaddr, int futex_op, int val, const timespec32 *timeout, int *uaddr2, uint32_t val3")@
GUEST_SYSCALL_x86_32(futex) {
  void* timeout_ptr = (void*)timeout;
  struct timespec tp64 {};
  int cmd = futex_op & FUTEX_CMD_MASK;
  if (timeout &&
    (cmd == FUTEX_WAIT ||
      cmd == FUTEX_LOCK_PI ||
      cmd == FUTEX_WAIT_BITSET ||
      cmd == FUTEX_WAIT_REQUEUE_PI)) {
    // timeout argument is only handled as timespec in these cases
    // Otherwise just an integer
    tp64 = *timeout;
    timeout_ptr = &tp64;
  }

  uint64_t Result = syscall(SYSCALL_DEF(futex),
    uaddr,
    futex_op,
    val,
    timeout_ptr,
    uaddr2,
    val3);
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("sigaltstack const compat_ptr<stack_t32> ss, compat_ptr<stack_t32> old_ss")@
GUEST_SYSCALL_x86_32(sigaltstack) {
  stack_t ss64{};
  stack_t old64{};

  stack_t* ss64_ptr{};
  stack_t* old64_ptr{};

  if (ss) {
    ss64 = *ss;
    ss64_ptr = &ss64;
  }

  if (old_ss) {
    old64 = *old_ss;
    old64_ptr = &old64;
  }
  uint64_t Result = FEX::HLE::_SyscallHandler->GetSignalDelegator()->RegisterGuestSigAltStack(ss64_ptr, old64_ptr);

  if (Result == 0 && old_ss) {
    *old_ss = old64;
  }
  return Result;
}

// launch a new process under fex
// currently does not propagate argv[0] correctly

//@REGEX_DECL_SYSCALL_IMPL_X32("execve const char *pathname, uint32_t *argv, uint32_t *envp")@
GUEST_SYSCALL_x86_32(execve) {
  std::vector<const char*> Args;
  std::vector<const char*> Envp;

  if (argv) {
    for (int i = 0; argv[i]; i++) {
      Args.push_back(reinterpret_cast<const char*>(static_cast<uintptr_t>(argv[i])));
    }

    Args.push_back(nullptr);
  }

  if (envp) {
    for (int i = 0; envp[i]; i++) {
      Envp.push_back(reinterpret_cast<const char*>(static_cast<uintptr_t>(envp[i])));
    }
    Envp.push_back(nullptr);
  }

  auto* const* ArgsPtr = argv ? const_cast<char* const*>(Args.data()) : nullptr;
  auto* const* EnvpPtr = envp ? const_cast<char* const*>(Envp.data()) : nullptr;
  return FEX::HLE::ExecveHandler(pathname, ArgsPtr, EnvpPtr, nullptr);
}


//@REGEX_DECL_SYSCALL_IMPL_X32("execveat int dirfd, const char *pathname, uint32_t *argv, uint32_t *envp, int flags")@
GUEST_SYSCALL_x86_32(execveat) {
  std::vector<const char*> Args;
  std::vector<const char*> Envp;

  if (argv) {
    for (int i = 0; argv[i]; i++) {
      Args.push_back(reinterpret_cast<const char*>(static_cast<uintptr_t>(argv[i])));
    }

    Args.push_back(nullptr);
  }

  if (envp) {
    for (int i = 0; envp[i]; i++) {
      Envp.push_back(reinterpret_cast<const char*>(static_cast<uintptr_t>(envp[i])));
    }
    Envp.push_back(nullptr);
  }

  FEX::HLE::ExecveAtArgs AtArgs{
    .dirfd = dirfd,
    .flags = flags,
  };

  auto* const* ArgsPtr = argv ? const_cast<char* const*>(Args.data()) : nullptr;
  auto* const* EnvpPtr = envp ? const_cast<char* const*>(Envp.data()) : nullptr;
  return FEX::HLE::ExecveHandler(pathname, ArgsPtr, EnvpPtr, &AtArgs);
}


//@REGEX_DECL_SYSCALL_IMPL_X32("wait4 pid_t pid, int *wstatus, int options, struct rusage_32 *rusage")@
GUEST_SYSCALL_x86_32(wait4) {
  struct rusage usage64 {};
  struct rusage* usage64_p{};

  if (rusage) {
    usage64 = *rusage;
    usage64_p = &usage64;
  }
  uint64_t Result = ::wait4(pid, wstatus, options, usage64_p);
  if (rusage) {
    *rusage = usage64;
  }
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("waitid int which, pid_t upid, compat_ptr<FEXCore::x86::siginfo_t> info, int options, struct rusage_32 *rusage")@
GUEST_SYSCALL_x86_32(waitid) {
  struct rusage usage64 {};
  struct rusage* usage64_p{};

  siginfo_t info64{};
  siginfo_t* info64_p{};

  if (rusage) {
    usage64 = *rusage;
    usage64_p = &usage64;
  }

  if (info) {
    info64_p = &info64;
  }

  uint64_t Result = ::syscall(SYSCALL_DEF(waitid), which, upid, info64_p, options, usage64_p);

  if (Result != -1) {
    if (rusage) {
      *rusage = usage64;
    }

    if (info) {
      *info = info64;
    }
  }

  SYSCALL_ERRNO();
}