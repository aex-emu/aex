/*
$info$
tags: LinuxSyscalls|syscalls-x86-32
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_64/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Types.h"

#include <linux/aio_abi.h>
#include <stdint.h>
#include <syscall.h>
#include <unistd.h>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"


//@REGEX_DECL_SYSCALL_IMPL_X32("io_getevents aio_context_t ctx_id, long min_nr, long nr, struct io_event *events, struct timespec32 *timeout")@
GUEST_SYSCALL_x86_32(io_getevents) {
  struct timespec* timeout_ptr{};
  struct timespec tp64 {};
  if (timeout) {
    tp64 = *timeout;
    timeout_ptr = &tp64;
  }

  uint64_t Result = ::syscall(SYSCALL_DEF(io_getevents), ctx_id, min_nr, nr, events, timeout_ptr);
  SYSCALL_ERRNO();
}


//@REGEX_DECL_SYSCALL_IMPL_X32("io_pgetevents aio_context_t ctx_id, long min_nr, long nr, struct io_event *events, struct timespec32 *timeout, const struct io_sigset  *usig")@
GUEST_SYSCALL_x86_32(io_pgetevents) {
  struct timespec* timeout_ptr{};
  struct timespec tp64 {};
  if (timeout) {
    tp64 = *timeout;
    timeout_ptr = &tp64;
  }

  uint64_t Result = ::syscall(SYSCALL_DEF(io_pgetevents), ctx_id, min_nr, nr, events, timeout_ptr, usig);
  SYSCALL_ERRNO();
}

