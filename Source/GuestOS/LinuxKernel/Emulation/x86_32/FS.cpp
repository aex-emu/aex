/*
$info$
tags: LinuxSyscalls|syscalls-x86-32
$end_info$
*/

#include "GuestOS/LinuxKernel/Syscalls.h"
#include "GuestOS/LinuxKernel/Emulation/x86_32/Syscalls.h"
#include"GuestOS/LinuxKernel/Helpers/SignalDelegator.h"

#include <stddef.h>
#include <stdint.h>
#include <sys/mount.h>
#include <unistd.h>

#include "GuestOS/LinuxKernel/Guest/GuestSyscalls.h"

//@REGEX_DECL_SYSCALL_IMPL_X32("umount const char *target")@
GUEST_SYSCALL_x86_32(umount) {
  uint64_t Result = ::umount(target);
  SYSCALL_ERRNO();
}

    
//@REGEX_DECL_SYSCALL_IMPL_X32("truncate64 const char *path, uint32_t offset_low, uint32_t offset_high")@
GUEST_SYSCALL_x86_32(truncate64) {
  uint64_t Offset = offset_high;
  Offset <<= 32;
  Offset |= offset_low;
  uint64_t Result = ::truncate(path, Offset);
  SYSCALL_ERRNO();
}

    
//@REGEX_DECL_SYSCALL_IMPL_X32("ftruncate64 int fd, uint32_t offset_low, uint32_t offset_high")@
GUEST_SYSCALL_x86_32(ftruncate64) {
  uint64_t Offset = offset_high;
  Offset <<= 32;
  Offset |= offset_low;
  uint64_t Result = ::ftruncate(fd, Offset);
  SYSCALL_ERRNO();
}
  