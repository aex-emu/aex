#pragma once

#define IMPL(Name) Name

extern "C" {
void IMPL(px11_RemoveGuestX11)(Display *Guest);
void IMPL(px11_XFree)(void *p);
}

#undef IMPL