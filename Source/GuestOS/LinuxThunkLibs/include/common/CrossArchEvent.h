#pragma once

#include <linux/futex.h>
#include <sys/syscall.h>
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <unistd.h>

struct CrossArchEvent final {
  std::atomic<uint32_t> Futex;
};

static inline void WaitForWorkFunc(CrossArchEvent *Event) {

  // Wait for Futex value to become 1
  while (true) {

    // First step compare it with 1 already and see if we can early out
    uint32_t One = 1;
    if (Event->Futex.compare_exchange_strong(One, 0)) {
      return;
    }

    int Op = FUTEX_WAIT | FUTEX_PRIVATE_FLAG;
    [[maybe_unused]] int Res = syscall(SYS_futex,
        &Event->Futex,
        Op,
        nullptr, // Timeout
        nullptr, // Addr
        0);
  }
}

static inline void NotifyWorkFunc(CrossArchEvent *Event) {
  uint32_t Zero = 0;
  if (Event->Futex.compare_exchange_strong(Zero, 1)) {
    int Op = FUTEX_WAKE | FUTEX_PRIVATE_FLAG;
    syscall(SYS_futex,
      &Event->Futex,
      Op,
      nullptr, // Timeout
      nullptr, // Addr
      0);
  }
}