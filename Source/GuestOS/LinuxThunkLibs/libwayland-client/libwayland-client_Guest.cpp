// Just a faux libwayland-client that refuses to do anything

#include <errno.h>
#include <stdio.h>

#define dbgf(...) fprintf(stderr, __VA_ARGS__)

extern "C" {

long long wl_buffer_interface[256];
long long wl_callback_interface[256];
long long wl_compositor_interface[256];
long long wl_data_device_interface[256];
long long wl_data_device_manager_interface[256];
long long wl_data_offer_interface[256];
long long wl_data_source_interface[256];
long long wl_display_interface[256];
long long wl_keyboard_interface[256];
long long wl_output_interface[256];
long long wl_pointer_interface[256];
long long wl_region_interface[256];
long long wl_registry_interface[256];
long long wl_seat_interface[256];
long long wl_shell_interface[256];
long long wl_shell_surface_interface[256];
long long wl_shm_interface[256];
long long wl_shm_pool_interface[256];
long long wl_subcompositor_interface[256];
long long wl_subsurface_interface[256];
long long wl_surface_interface[256];
long long wl_touch_interface[256];

long long wl_array_add() {
    errno = EACCES;
	return 0;
}
long long wl_array_copy() {
    errno = EACCES;
	return 0;
}
long long wl_array_init() {
    errno = EACCES;
	return 0;
}
long long wl_array_release() {
    errno = EACCES;
	return 0;
}
long long wl_display_cancel_read() {
    errno = EACCES;
	return 0;
}
long long wl_display_connect() {
    dbgf("wl_display_connect: Refusing connection\n");
    errno = EACCES; 
	return 0;
}
long long wl_display_connect_to_fd() {
    dbgf("wl_display_connect_to_fd: Refusing connection\n");
    errno = EACCES;
	return 0;
}
long long wl_display_create_queue() {
    errno = EACCES;
	return 0;
}
long long wl_display_disconnect() {
    errno = EACCES;
	return 0;
}
long long wl_display_dispatch() {
    errno = EACCES;
	return 0;
}
long long wl_display_dispatch_pending() {
    errno = EACCES;
	return 0;
}
long long wl_display_dispatch_queue() {
    errno = EACCES;
	return 0;
}
long long wl_display_dispatch_queue_pending() {
    errno = EACCES;
	return 0;
}
long long wl_display_flush() {
    errno = EACCES;
	return 0;
}
long long wl_display_get_error() {
    errno = EACCES;
	return 0;
}
long long wl_display_get_fd() {
    errno = EACCES;
	return 0;
}
long long wl_display_get_protocol_error() {
    errno = EACCES;
	return 0;
}
long long wl_display_prepare_read() {
    errno = EACCES;
	return 0;
}
long long wl_display_prepare_read_queue() {
    errno = EACCES;
	return 0;
}
long long wl_display_read_events() {
    errno = EACCES;
	return 0;
}
long long wl_display_roundtrip() {
    errno = EACCES;
	return 0;
}
long long wl_display_roundtrip_queue() {
    errno = EACCES;
	return 0;
}
long long wl_event_queue_destroy() {
    errno = EACCES;
	return 0;
}
long long wl_list_empty() {
    errno = EACCES;
	return 0;
}
long long wl_list_init() {
    errno = EACCES;
	return 0;
}
long long wl_list_insert() {
    errno = EACCES;
	return 0;
}
long long wl_list_insert_list() {
    errno = EACCES;
	return 0;
}
long long wl_list_length() {
    errno = EACCES;
	return 0;
}
long long wl_list_remove() {
    errno = EACCES;
	return 0;
}
long long wl_log_set_handler_client() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_add_dispatcher() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_add_listener() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_create() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_create_wrapper() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_destroy() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_get_class() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_get_id() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_get_listener() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_get_tag() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_get_user_data() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_get_version() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_marshal() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_marshal_array() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_marshal_array_constructor() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_marshal_array_constructor_versioned() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_marshal_array_flags() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_marshal_constructor() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_marshal_constructor_versioned() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_marshal_flags() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_set_queue() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_set_tag() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_set_user_data() {
    errno = EACCES;
	return 0;
}
long long wl_proxy_wrapper_destroy() {
    errno = EACCES;
	return 0;
}
}