{% macro arg_name(arg) -%}
a_{{arg.index}}
{%- endmacro -%}

{% macro append_if(cond, str) -%}
{% if cond %}{{str}}{% endif %}
{%- endmacro -%}

{% macro args_str(args, comma) -%}
{% for arg in args %}typed<{{arg.qual_type}}> {{arg_name(arg)}}{{append_if(not loop.last or comma, ", ")}}{% endfor %}
{%- endmacro -%}