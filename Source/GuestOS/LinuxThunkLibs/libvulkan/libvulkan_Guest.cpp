/*
$info$
tags: thunklibs|Vulkan
$end_info$
*/

#define VK_USE_PLATFORM_XLIB_XRANDR_EXT
#define VK_USE_PLATFORM_XLIB_KHR
#define VK_USE_PLATFORM_XCB_KHR
#define VK_USE_PLATFORM_WAYLAND_KHR
#include <vulkan/vulkan.h>

#include <cstdio>
#include <dlfcn.h>
#include <algorithm>
#include <functional>
#include <string_view>
#include <unordered_map>

#include <X11/Xmu/CloseHook.h> // for Display * definition

#define tracef(...) //fprintf(stderr, __VA_ARGS__);
#define marshalf(...) fprintf(stderr, __VA_ARGS__);

#include "common/Guest.h"

#include "thunks.inl"
#include "guest_structs.inl"

#if 0
template<>
struct pack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateInstance_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkCreateInstance_t>;
    argsrv_t *argsrv;
    pack_marshal_t(argsrv_t *argsrv) : argsrv(argsrv) { }
    ~pack_marshal_t() {
        argsrv->rv.value = 1;  //VK_NOT_READY
        argsrv->a_2.ptr()->value = 1;
    }
};
#endif

#define XCB_FLUSH(ENTRY, CONN) \
template<> \
struct pack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_##ENTRY##_t>> { \
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_##ENTRY##_t>;  \
    pack_marshal_t(argsrv_t *argsrv) {  \
        xcb_flush(argsrv->CONN);  \
        marshalf(#ENTRY ": xcb_flush'ed\n");  \
    } \
};

XCB_FLUSH(vkCreateXcbSurfaceKHR, a_1->connection)
XCB_FLUSH(vkGetPhysicalDeviceXcbPresentationSupportKHR, a_2)

#define XLIB_FLUSH(ENTRY, CONN) \
template<> \
struct pack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_##ENTRY##_t>> { \
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_##ENTRY##_t>;  \
    pack_marshal_t(argsrv_t *argsrv) {  \
        XFlush(argsrv->CONN);  \
        marshalf(#ENTRY ": xcb_flush'ed\n");  \
    } \
};

XLIB_FLUSH(vkCreateXlibSurfaceKHR, a_1->dpy)
XLIB_FLUSH(vkAcquireXlibDisplayEXT, a_1)
XLIB_FLUSH(vkGetPhysicalDeviceXlibPresentationSupportKHR, a_2)
XLIB_FLUSH(vkGetRandROutputDisplayEXT, a_1)

constexpr bool stub_unknown_functions = false;

// Fatally erroring function with a thunk-like interface. This is used as a placeholder for unknown Vulkan functions
[[noreturn]] static void FatalError(void* raw_args, void *called_function) {
    //auto called_function = reinterpret_cast<PackedArguments<void, uintptr_t>*>(raw_args)->a0;
    fprintf(stderr, "FATAL: Called unknown Vulkan function at address %p\n", reinterpret_cast<void*>(called_function));
    __builtin_trap();
}


static uintptr_t MakeGuestCallable(const char* origin, uintptr_t func, const char* name);

template<>
struct pack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkGetDeviceProcAddr_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkGetDeviceProcAddr_t>;
    argsrv_t *argsrv;

    pack_marshal_t(argsrv_t *argsrv) : argsrv(argsrv) { }

    ~pack_marshal_t() {
        if (argsrv->rv.value) {
            argsrv->rv.value = MakeGuestCallable("vkGetDeviceProcAdd", argsrv->rv.value, (const char*)argsrv->a_1.ptr());
        }
    }
};

template<>
struct pack_marshal_t<thunks::packed_argsrv_t<thunks::packed_argsrv_vkGetInstanceProcAddr_t>> {
    using argsrv_t = thunks::packed_argsrv_t<thunks::packed_argsrv_vkGetInstanceProcAddr_t>;
    argsrv_t *argsrv;

    pack_marshal_t(argsrv_t *argsrv) : argsrv(argsrv) { }

    ~pack_marshal_t() {
        if (argsrv->rv.value) {
            argsrv->rv.value = MakeGuestCallable("vkGetDeviceProcAdd", argsrv->rv.value, (const char*)argsrv->a_1.ptr());
        }
    }
};

#include "function_packs.inl"
#include "function_packs_public.inl"
#include "symbol_list.inl"

uintptr_t MakeGuestCallable(const char* origin, uintptr_t func, const char* name) {

    std::string_view name_s{name};

    auto Begin = std::begin(HostPtrInvokers);
    auto End = std::end(HostPtrInvokers);

    auto Invoker = std::lower_bound(Begin, End, name_s);

    if (Invoker == End || Invoker->name != name_s) {
        if (Invoker == End) {
            for (auto &In: HostPtrInvokers) {
                fprintf(stderr, "vulkan-thunks: %s, %d\n", In.name.data(), In.name < name_s);    
            }
            fprintf(stderr, "vulkan-thunks: %s: Unknown vulkan function at address %p: '%s'\n", origin, (void*)func, name);
        } else {
            fprintf(stderr, "vulkan-thunks: %s: missmatched vulkan function at address %p: '%s' != '%s'\n", origin, (void*)func, name, Invoker->name.data());
        }
        return 0;
    } else {
        tracef("Linking address %s(%p) to host invoker %p\n", name, (void*)func, Invoker->hostcall);
        LinkAddressToFunction(func, (uintptr_t)Invoker->hostcall);
        return func;
    }
}

LOAD_LIB(libvulkan)
