#pragma once
#include <cstdint>
#include <string>
#include <shared_mutex>

#include "GuestCPU/IR/IR.h"

#include "Common/ForwardDeclarations.h"

namespace FEXCore::HLE {
  struct SyscallArguments {
    static constexpr std::size_t MAX_ARGS = 7;
    uint64_t Argument[MAX_ARGS];
  };

  struct SyscallABI {
    // Expectation is that the backend will be aware of how to modify the arguments based on numbering
    // Only GPRs expected
    uint8_t NumArgs;
    // If the syscall has a return then it should be stored in the ABI specific syscall register
    // Linux = RAX
    bool HasReturn;

    int32_t HostSyscallNumber;
  };

  enum class SyscallOSABI {
    OS_UNKNOWN,
    OS_LINUX64,
    OS_LINUX32,
    OS_WIN64,
    OS_WIN32,
    OS_HANGOVER,
  };


  struct NamedRegionLookupResult {
    NamedRegionLookupResult(FEXCore::Core::NamedRegion *Entry, uintptr_t VAFileStart, std::shared_lock<std::shared_mutex> &&lk)
      : Entry(Entry), VAFileStart(VAFileStart), lk(std::move(lk))
    {

    }

    NamedRegionLookupResult(NamedRegionLookupResult&&) = default;

    FEXCore::Core::NamedRegion *Entry;
    uintptr_t VAFileStart;
    uintptr_t VAMin; // set to UINTPTR_MAX if not executable
    uintptr_t VAMax;

    friend class LinuxSyscallHandlerBase;
    protected:
    std::shared_lock<std::shared_mutex> lk;
  };

  class LinuxSyscallHandlerBase {
  public:
    using SyscallHandlerFunction = uint64_t (uint64_t Arg0, uint64_t Arg1, uint64_t Arg2, uint64_t Arg3, uint64_t Arg4, uint64_t Arg5);

    virtual ~LinuxSyscallHandlerBase() = default;
    
    std::vector<SyscallHandlerFunction *> HandlerFunctions;

    // 8 arguments, they -just- in the register arguments for aarch64
    uint64_t HandleSyscall(uint64_t Arg0, uint64_t Arg1, uint64_t Arg2, uint64_t Arg3, uint64_t Arg4, uint64_t Arg5, uint64_t SyscallNo) {
      if (SyscallNo >= HandlerFunctions.size()) {
        return (uint64_t)-ENOSYS;
      }

      auto Handler = HandlerFunctions[SyscallNo];
      return Handler(Arg0, Arg1, Arg2, Arg3, Arg4, Arg5);
    }

    virtual SyscallABI GetSyscallABI(uint64_t Syscall) = 0;
    SyscallOSABI GetOSABI() const { return OSABI; }

    virtual FEXCore::IR::SyscallFlags GetSyscallFlags(uint64_t Syscall) const { return FEXCore::IR::SyscallFlags::DEFAULT; }

    virtual FEXCore::CodeLoader *GetCodeLoader() const { return nullptr; }
    virtual void MarkGuestExecutableRange(uint64_t Start, uint64_t Length) { }
    virtual NamedRegionLookupResult LookupNamedRegionForCode(uint64_t GuestAddr) = 0;

    virtual SourcecodeResolver *GetSourcecodeResolver() { return nullptr; }

    // does a mmap as if done via a guest syscall
    virtual void *GuestMmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset)  { return (void*)UINTPTR_MAX; }

    // does a guest munmap as if done via a guest syscall
    virtual int GuestMunmap(void *addr, uint64_t length) { return -1; }

  protected:
    SyscallOSABI OSABI;
  };
}
