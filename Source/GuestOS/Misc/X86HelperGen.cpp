/*
$info$
tags: glue|x86-guest-code
desc: Guest-side assembly helpers used by the backends
$end_info$
*/

#include "GuestOS/Misc/X86HelperGen.h"
#include "GuestOS/Misc/SyscallHandler.h"

#include "Config/Config.h"
#include "Allocator/Allocator.h"
#include "Common/HostSyscalls.h"

#include <cstdint>
#include <cstring>
#include <vector>
#include <sys/mman.h>
#include "GuestCPU/Context/Context.h"

namespace FEXCore {
constexpr size_t CODE_SIZE = 0x1000;

void X86GeneratedCode::Init(FEXCore::Context::Context *CTX) {
  this->CTX = CTX;
  // Allocate a page for our emulated guest
  CodePtr = AllocateGuestCodeSpace(CTX, CODE_SIZE);

  SignalReturn = reinterpret_cast<uint64_t>(CodePtr);
  CallbackReturn = reinterpret_cast<uint64_t>(CodePtr) + 2;

  const std::vector<uint8_t> SignalReturnCode = {
    0x0F, 0x36, // SIGRET FEX instruction
    0x0F, 0x37, // CALLBACKRET FEX Instruction
  };

  memcpy(CodePtr, &SignalReturnCode.at(0), SignalReturnCode.size());
}

X86GeneratedCode::~X86GeneratedCode() {
  if (CodePtr) {
    CTX->SyscallHandler->GuestMunmap(CodePtr, CODE_SIZE);
  }
}

void* X86GeneratedCode::AllocateGuestCodeSpace(FEXCore::Context::Context *CTX, size_t Size) {

  if (CTX->Config.Is64BitMode()) {
    // 64bit mode can have its sigret handler anywhere
    return CTX->SyscallHandler->GuestMmap(nullptr, Size, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  }

  // First 64bit page
  constexpr uintptr_t LOCATION_MAX = 0x1'0000'0000;

  // 32bit mode
  // We need to have the sigret handler in the lower 32bits of memory space
  // Scan top down and try to allocate a location
  for (size_t Location = 0xFFFF'E000; Location != 0x0; Location -= 0x1000) {
    void *Ptr = CTX->SyscallHandler->GuestMmap(reinterpret_cast<void*>(Location), Size, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_FIXED_NOREPLACE | MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

    if (Ptr != MAP_FAILED &&
        reinterpret_cast<uintptr_t>(Ptr) >= LOCATION_MAX) {
      // Failed to map in the lower 32bits
      // Try again
      // Can happen in the case that host kernel ignores MAP_FIXED_NOREPLACE
      CTX->SyscallHandler->GuestMunmap(Ptr, Size);
      continue;
    }

    if (Ptr != MAP_FAILED) {
      return Ptr;
    }
  }

  // Can't do anything about this
  // Here's hoping the application doesn't use signals
  return MAP_FAILED;
}

}

