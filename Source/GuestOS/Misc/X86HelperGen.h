/*
$info$
tags: glue|x86-guest-code
$end_info$
*/

#pragma once

#include <stddef.h>
#include <stdint.h>

#include "Common/ForwardDeclarations.h"

namespace FEXCore {
class X86GeneratedCode final {
public:
  ~X86GeneratedCode();

  uint64_t SignalReturn{};
  uint64_t CallbackReturn{};

  void Init(FEXCore::Context::Context *CTX);
private:
  void *CodePtr = nullptr;
  FEXCore::Context::Context *CTX;
  void* AllocateGuestCodeSpace(FEXCore::Context::Context *CTX, size_t Size);
};
}
