#pragma once

#include "Common/CompilerDefs.h"

#include <array>
#include <atomic>
#include <cstdint>
#include <functional>
#include <utility>
#include <signal.h>
#include <stddef.h>
#include <vector>

#include "Common/ForwardDeclarations.h"

#include "GuestOS/LinuxKernel/Guest/GuestLinuxTypes.h"

namespace FEXCore {

  using HostSignalDelegatorFunction = std::function<bool(FEXCore::Core::InternalThreadState *Thread, int Signal, void *info, void *ucontext)>;
  using HostSignalDelegatorFunctionForGuest = std::function<bool(FEXCore::Core::InternalThreadState *Thread, int Signal, void *info, void *sigmask, void *ucontext, GuestSigAction *GuestAction, stack_t *GuestStack)>;

  class SignalDelegator {
  public:
    virtual ~SignalDelegator() = default;

    /**
     * @brief Registers an emulated thread's object to a TLS object
     *
     * Required to know which thread has received the signal when it occurs
     */
    void RegisterTLSState(FEXCore::Core::InternalThreadState *Thread);
    void UninstallTLSState(FEXCore::Core::InternalThreadState *Thread);

    /**
     * @brief Registers a signal handler for the host to handle a signal
     *
     * It's a process level signal handler so one must be careful
     */
    void RegisterHostSignalHandler(int Signal, HostSignalDelegatorFunction Func, bool Required);
    void RegisterFrontendHostSignalHandler(int Signal, HostSignalDelegatorFunction Func, bool Required);

    // Called from the thunk handler to handle the signal
    void HandleSignal(int Signal, void *Info, void *UContext);

    /**
     * @brief Check to ensure the XID handler is still set to the FEX handler
     *
     * On a new thread GLIBC will set the XID handler underneath us.
     * After the first thread is created check this.
     */
    virtual void CheckXIDHandler() = 0;

    constexpr static size_t MAX_SIGNALS {64};

    // wrapper for SYS_rt_sigprocmask
    static void SetSignalMask(uint64_t Mask);

    // returns new signal mask
    virtual uint64_t HandleGuestSignalOrFault(bool Is64BitMode, FEXCore::Core::CpuStateFrame *Frame, void *SigInfo) = 0;

  protected:

    /**
     * @brief Registers an emulated thread's object to a TLS object
     *
     * Required to know which thread has received the signal when it occurs
     */
    virtual void RegisterFrontendTLSState(FEXCore::Core::InternalThreadState *Thread) = 0;
    virtual void UninstallFrontendTLSState(FEXCore::Core::InternalThreadState *Thread) = 0;

    /**
     * @brief Registers a signal handler for the host to handle a signal
     *
     * It's a process level signal handler so one must be careful
     */
    virtual void FrontendRegisterHostSignalHandler(int Signal, HostSignalDelegatorFunction Func, bool Required) = 0;
    virtual void FrontendRegisterFrontendHostSignalHandler(int Signal, HostSignalDelegatorFunction Func, bool Required) = 0;

  private:
    struct HostSignalHandler {
      std::vector<FEXCore::HostSignalDelegatorFunction> Handlers{};
      FEXCore::HostSignalDelegatorFunction FrontendHandler{};
    };
    std::array<HostSignalHandler, MAX_SIGNALS + 1> HostHandlers{};

  protected:
    void SetHostSignalHandler(int Signal, HostSignalDelegatorFunction Func, bool Required) {
      HostHandlers[Signal].Handlers.push_back(std::move(Func));
    }
    void SetFrontendHostSignalHandler(int Signal, HostSignalDelegatorFunction Func, bool Required) {
      HostHandlers[Signal].FrontendHandler = std::move(Func);
    }
  };
}
