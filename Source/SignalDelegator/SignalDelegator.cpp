#include "SignalDelegator/SignalDelegator.h"
#include "LogMgr/LogManager.h"
#include "Common/HostSyscalls.h"

#include <atomic>
#include <unistd.h>
#include <signal.h>
#include <csetjmp>

#include <ucontext.h>

#include "GuestCPU/Context/Context.h"
#include "Common/ThreadContext.h"

#include "GuestCPU/Context/InternalThreadState.h"

static constexpr auto RAW_SIGSET_SIZE = 8;

#define HOST_DEFER_TRACE do { } while (false)
//#define HOST_DEFER_TRACE do { char str[512]; write(1, str, sprintf(str,"%*s%d %s\n", Previous, "", Previous, __func__)); } while (false)

namespace FEXCore {

  static bool IsSynchronous(int Signal) {
    switch (Signal) {
    case SIGBUS:
    case SIGFPE:
    case SIGILL:
    case SIGSEGV:
    case SIGTRAP:
      return true;
    default: break;
    };
    return false;
  }


  void SignalDelegator::RegisterTLSState(FEXCore::Core::InternalThreadState *Thread) {
    RegisterFrontendTLSState(Thread);
  }

  void SignalDelegator::UninstallTLSState(FEXCore::Core::InternalThreadState *Thread) {
    UninstallFrontendTLSState(Thread);
  }

  void SignalDelegator::RegisterHostSignalHandler(int Signal, HostSignalDelegatorFunction Func, bool Required) {
    SetHostSignalHandler(Signal, Func, Required);
    FrontendRegisterHostSignalHandler(Signal, Func, Required);
  }

  void SignalDelegator::RegisterFrontendHostSignalHandler(int Signal, HostSignalDelegatorFunction Func, bool Required) {
    SetFrontendHostSignalHandler(Signal, Func, Required);
    FrontendRegisterFrontendHostSignalHandler(Signal, Func, Required);
  }

  void SignalDelegator::SetSignalMask(uint64_t Mask) {
    Mask &= ~ ((1 << (SIGSEGV-1) | (1 << (SIGBUS-1))));

    ::syscall(SYS_rt_sigprocmask, SIG_SETMASK, &Mask, nullptr, 8);
  }

  extern "C" {
    extern char __start_host_syscalls[];
    extern char __stop_host_syscalls[];
  }

  void SignalDelegator::HandleSignal(int Signal, void *Info, void *UContext) {
    // TODO: Assert signal re-entrancy?

    // Let the host take first stab at handling the signal
    auto Thread = GetThreadBound<FEXCore::Core::InternalThreadState>();

    // Host handling of signals is never deferred
    HostSignalHandler &Handler = HostHandlers[Signal];

    if (!Thread) {
      LogMan::Msg::EFmt("[{}] Thread has received a signal and hasn't registered itself with the delegate! Programming error!", FHU::Syscalls::gettid());
    } else {

      // No signals must be delivered in this scope - should be guaranteed.

      for (auto &Handler : Handler.Handlers) {
        if (Handler(Thread, Signal, Info, UContext)) {
          // If the host handler handled the fault then we can continue now
          return;
        }
      }

      if (Handler.FrontendHandler &&
          Handler.FrontendHandler(Thread, Signal, Info, UContext)) {
        return;
      }
    }

    auto siginfo = (siginfo_t *)Info;
    auto ctx = (ucontext_t*)UContext;

    if (siginfo->si_code > 0) {
      if (
          Signal == SIGSEGV ||
          Signal == SIGFPE ||
          Signal == SIGILL ||
          Signal == SIGTRAP ||
          Signal == SIGBUS
          )
      {
        LogMan::Msg::EFmt("[{}] Thread has received a FATAL Kernel Generated Signal -> signal: {}, pc: {}, si_addr: {}!", FHU::Syscalls::gettid(), Signal, ctx->uc_mcontext.pc, reinterpret_cast<uintptr_t>(siginfo->si_addr));
        Context::LogCrashDump(ctx->uc_mcontext.pc, reinterpret_cast<uintptr_t>(siginfo->si_addr));
      } else if (Signal != SIGCHLD) {
        LogMan::Msg::EFmt("[{}] Thread has received a Kernel Generated Signal -> {}!", FHU::Syscalls::gettid(), Signal);
      }
    }

    if (ctx->uc_mcontext.pc >= uintptr_t(__start_host_syscalls) && ctx->uc_mcontext.pc < uintptr_t(__stop_host_syscalls)) {
      // Signal received in a syscall instr
      // either syscall was just about to be executed
      // or, it was interrupted and will 'automatically' re-execute
      //
      // either way, skip syscall and set rv (x0) to the internal error EINCOMPLETE
      // which will re-dispatch the guest context, deliver any pending guest signals, and let
      // the guest retry the syscall, if need be
      //
      auto Code = (uint32_t*)ctx->uc_mcontext.pc;

      // this assumes that the syscall epilogue is
      // svc 0
      // ret

      // was the syscall just completed?
      if (Code[-1] != 0xd4000001) {
        // if not, bail and return with INCOMPLETE
        ctx->uc_mcontext.regs[0] = UINT64_MAX - 4095;
        ctx->uc_mcontext.pc = ctx->uc_mcontext.regs[30];
      }
    }

    /*
      The signal needs to be delivered to the guest

      The host can be one of 4 frame types when interrupted by a signal
      - Inside a Thunk frame
        generate a thunk re-entry signal frame in the guest stack; redirect flow to guest signal dispatcher
      - Inside a Translated Code frame
        if (async signal) { guest defer } else { redirect execution to exception block }
      - Inside a Dispatch Code frame
        if (aysnc signal) { guest defer } else { emulator error, report to host os }
      - Inside an Emulator frame
        if (aysnc signal) { guest defer } else { emulator error, report to host os }
    */

    // TODO: what if synchronous signal while async is pending?
    if (Thread->CurrentFrame->DeferredSignal.SignalNumber != 0) {
      LogMan::Msg::EFmt("Nested guest signals?! Deferred: {}, Current: {}, calling SIG_DLF for Current", Thread->CurrentFrame->DeferredSignal.SignalNumber, Signal);
      signal(Signal, SIG_DFL);
      pthread_kill(pthread_self(), Signal);
      SetSignalMask(~(1 << (Signal -1)));
      ERROR_AND_DIE_FMT("pthread_kill should term or coredump here? signal: {}", Signal);
    }

    Thread->CurrentFrame->DeferredSignal.SignalNumber = Signal;
    memcpy(&Thread->CurrentFrame->DeferredSignal.siginfo, Info, sizeof(Thread->CurrentFrame->DeferredSignal.siginfo));

    
    uint64_t sigmask_raw = UINT64_MAX;

    // TODO: Need to not mask synchronous signals the guest wants to handle here
    // Also, need to not mask SIGSEGV (smc tracking) and SIGBUS (unaligned atomics for arm handling)
    sigmask_raw &= ~(1ULL << (SIGSEGV - 1));
    sigmask_raw &= ~(1ULL << (SIGBUS - 1));

    // Set the new sigmask on signal exit
    memcpy(&ctx->uc_sigmask, &sigmask_raw, RAW_SIGSET_SIZE);
  }
}
