#/bin/env python3
import os
import sys
import contextlib

# if symlink -> resolve one level, re-root, and write
# if directory -> mkdir dest
# if file -> link source / dest

def symlink_path(new_root: str, path: str):
	if path[0] == "/":
		return new_root + path
	else:
		return path

def reroot(root: str, new_root: str, path: str):
	for f in os.scandir(path):
		new_path = f.path.replace(root, new_root, 1)
		if f.is_symlink():
			new_symlink = symlink_path(new_root, os.readlink(f.path))
			print("symlink: " + f.path + " -> " + new_path + " @" + new_symlink)
			with contextlib.suppress(FileNotFoundError):
				os.unlink(new_path)
			os.symlink(new_symlink, new_path)
		elif f.is_dir():
			os.mkdir(new_path)
			reroot(root, new_root, f.path)
		else:
			print("file link: " + f.path + " -> " + new_path)
			with contextlib.suppress(FileNotFoundError):
				os.unlink(new_path)
			os.symlink(f.path, new_path)

reroot(sys.argv[1], sys.argv[2], sys.argv[1])
