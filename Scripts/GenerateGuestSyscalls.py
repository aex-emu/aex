#!/bin/python3
from dataclasses import dataclass
from io import TextIOWrapper
import json
import sys
import jinja2

ValidTags = [
    "passthrough",
    "exit",
    "kernel",
    "stub",
    "error",
    "arch",
    "sync"
]

def FatalError():
    print("Fatal Error: check the json file")
    sys.exit(-1)

def RenderTemplate(OutFolder: str, TemplateDirs: list, TemplateNames: list, Data: dict):
    for TemplateName in TemplateNames:
        environment = jinja2.Environment(loader=jinja2.FileSystemLoader(TemplateDirs))
        template = environment.get_template(TemplateName + ".in")
        template.stream(Data).dump(OutFolder + "/" + TemplateName)

def MakeSyscallEntry(Name, Arguments, Arch: str, Tags: dict):
    EffectiveTags = Tags.copy()

    EffectiveArch = EffectiveTags.get("arch", False)

    # is correct arch?
    if EffectiveArch and EffectiveArch != Arch:
        return None

    # default to the x86_64 handlers if no arch is specified
    if not EffectiveArch:
        EffectiveArch = "x86_64"

    # Flatten down arch specific tags
    for k, v in Tags.items():
        ArchSpecific = k.split(":")
        if len(ArchSpecific) > 1:
            if ArchSpecific[1] == Arch:
                EffectiveTags[ArchSpecific[0]] = v
    
    Kernel = EffectiveTags.get("kernel", False)

    if Kernel:
        Kernel = Kernel.replace(".", ",")

    GuestHostName = Name.split(":");
    GuestName = GuestHostName[0]
    HostName = GuestHostName[-1]

    return {
        "name": GuestName,
        "host_name": HostName,
        "args": Arguments,
        "arch": Arch,
        "effective_arch": EffectiveArch,
        "tags": EffectiveTags,
        "is_passthrough": EffectiveTags.get("passthrough", False),
        "is_stub": EffectiveTags.get("stub", False),
        "error": EffectiveTags.get("error", False),
        "kernel": Kernel,
        "exit_check_before": EffectiveTags.get("sync", EffectiveTags.get("exit", ) in ["before", "all"]),
        "exit_check_after": EffectiveTags.get("sync", EffectiveTags.get("exit", False) in ["after", "all"])
    }

def ParseSyscall(SyscallDef: str, Tags: dict, Options: dict, Arches: list):
    SyscallDef = SyscallDef.split("#", maxsplit=1)[0].strip();
    NameAndArgs = SyscallDef.split(" ", maxsplit=1)
    Name = NameAndArgs[0]
    Arguments = []
    if len(NameAndArgs) == 2:
        Arguments = [
            { "name": v[-1], "type": " ".join(v[:-1]), "decl": " ".join(v).replace("* ", "*") }
            for v in [
                [
                    v for v in e.replace("*", "* ").strip().split(" ") if v
                ] for e in NameAndArgs[1].split(",")
            ]
        ]

    rv = []
    for Arch in Arches:
        Entry = MakeSyscallEntry(Name, Arguments, Arch, Tags)
        if Entry:
            rv.append(Entry)

    return rv

def Parse(rv: dict, JSONDef, InheritedTags: dict, Arches: list):
    if type(JSONDef) is dict:
        for k, v in JSONDef.items():
            k = k.split("#")[0].strip();
            if (k == "docs"):
                rv["docs"] = v
                continue
            IsTags = True
            Tags = InheritedTags.copy()
            if (len(k) > 0):
                for TagEntry in k.split(","):
                    TagEntry = TagEntry.strip();
                    TagDetails = TagEntry.split( "-")
                    TagArchName = TagDetails[0].strip()
                    TagName = TagArchName.split(":")[0]
                    if not TagName in ValidTags:
                        IsTags = False
                        Tags = InheritedTags
                        print(f'warning: ~{TagName}~')
                        break
                    TagValue = True
                    if len(TagDetails) == 2:
                        TagValue = TagDetails[1]
                    if TagValue == None:
                        TagValue = True
                    Tags[TagArchName] = TagValue
            if IsTags:
                Parse(rv, v, Tags, Arches)
            elif (type(v) is dict):
                rv["syscalls"].extend(ParseSyscall(k, Tags, v, Arches))
            else:
                FatalError()
    elif type(JSONDef) is list:
        for k in JSONDef:
            rv["syscalls"].extend(ParseSyscall(k, InheritedTags, {}, Arches))
    else:
        FatalError()
    return rv

def ExitError(msg):
    print(msg)
    sys.exit(-1)

def main():
    if (len(sys.argv) < 6):
        ExitError("expecting at least 5 args: path/to/syscall.json <outfolder> <arch> <template name> ... -- <template dirs> ...")

    SyscallsJSONPath = sys.argv[1]

    OutFolder = sys.argv[2]
    Arch = sys.argv[3]

    ArgIndex = 4
    ArgNamesStart = ArgIndex
    while ArgIndex < len(sys.argv):
        Arg = sys.argv[ArgIndex]
        if (Arg == "--"):
            break;

        ArgIndex += 1
    
    TemplateNames = sys.argv[ArgNamesStart:ArgIndex]

    ArgIndex += 1
    ArgDirsStart = ArgIndex

    TemplateDirs = sys.argv[ArgDirsStart:]

    json_file = open(SyscallsJSONPath, "r")
    json_text = json_file.read()
    json_file.close()

    json_object = json.loads(json_text)

    rv = { "docs":[], "syscalls": [] }
    Parse(rv, json_object, {}, [Arch])

    RenderTemplate(OutFolder, TemplateDirs, TemplateNames, rv)

if __name__ == "__main__":
# execute only if run as a script
    sys.exit(main())