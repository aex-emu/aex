#!/usr/bin/python3
from tokenize import Name
from typing import Dict
from xmlrpc.client import Boolean
import clang.cindex
from clang.cindex import *
from clang.cindex import conf, register_function
import sys
from dataclasses import dataclass, field, replace
import subprocess
import logging
import jinja2
import hashlib
from textwrap import wrap

logger = logging.getLogger()
logger.setLevel(logging.WARNING)

def UnqualifiedTypeName(type: clang.cindex.Type) -> str:
    spelling: str = type.spelling
    spelling = spelling.replace("const ", "").replace("restrict ", "").replace("volatile ", "")
    if "(anonymous " in spelling:
        spelling = "anon_" + hashlib.sha256(spelling.encode('utf-8')).hexdigest()
    return spelling
    

def QualifySpelling(spelling: str, type: clang.cindex.Type) -> str:
    if (type.is_const_qualified):
        spelling = "const " + spelling;

    if (type.is_restrict_qualified):
        spelling = "restrict " + spelling;

    if (type.is_volatile_qualified):
        spelling = "volatile " + spelling;

    return spelling

def AddGuestStruct(Data: Dict, record: clang.cindex.Type):
    name = UnqualifiedTypeName(record)
    record = record.get_canonical()
    GuestStructsDict: dict = Data["structs_dict"]
    GuestStructs: list = Data["structs"]
    if (name in GuestStructsDict):
        return
    else:
        struct = {}
        GuestStructsDict[name] = struct
        struct["name"] = name
        struct["fields"] = fields = list()
        for field in record.get_fields():
            f:clang.cindex.Cursor = field
            fields.append({
                #"type": UnqualifiedTypeName(f.type),
                "qual_type": f.type.spelling,
                "name": f.spelling,
                "mapped_type": MapGuestType(Data, f.type),
                "offset": int(f.get_field_offsetof()/8),
                "size": f.type.get_size()
            })
        struct["size"] = record.get_size()

        GuestStructs.append(struct)

def AddGuestEnum(Data: Dict, record: clang.cindex.Type):
    name = UnqualifiedTypeName(record)
    record = record.get_canonical()
    GuestEnumsDict: dict = Data["enums_dict"]
    GuestEnums: list = Data["enums"]
    if (name in GuestEnumsDict):
        return
    else:
        enum = {}
        GuestEnumsDict[name] = enum
        enum["name"] = name
        
        GuestEnums.append(enum)

def MapGuestType(Data: Dict, type: clang.cindex.Type) -> str:
    type = type.get_canonical()
    name = UnqualifiedTypeName(type)

    if (type.get_pointee().kind != TypeKind.INVALID):
        return "guest_ptr<" + str(type.get_size()) + ", " + MapGuestType(Data, type.get_pointee()) + ">"
    else:
        if (type.kind in [TypeKind.UINT, TypeKind.ULONG, TypeKind.ULONGLONG, TypeKind.USHORT, TypeKind.UCHAR, TypeKind.CHAR_U]):
            return "guest_uint<" + str(type.get_size()) + ">"
        elif (type.kind in [TypeKind.INT, TypeKind.LONG,TypeKind.LONGLONG,TypeKind.SHORT, TypeKind.SCHAR, TypeKind.CHAR_S]):
            return "guest_int<" + str(type.get_size()) + ">"
        elif (type.kind in [TypeKind.FLOAT, TypeKind.DOUBLE]):
            return "guest_float<" + str(type.get_size()) + ">"
        elif  (type.kind in [TypeKind.RECORD]):
            if (type.get_size() < 0):
                return "guest_incomplete<" + name + ">";
            else:
                AddGuestStruct(Data, type)
                return "guest_type<" + name + ">";
        elif (type.kind in [TypeKind.FUNCTIONPROTO]):
            return "guest_function<" + type.spelling + ">"
        elif (type.kind in [TypeKind.VOID]):
            return "void"
        elif (type.kind in [TypeKind.CONSTANTARRAY]):
            return "guest_array<" + str(type.get_array_size()) + ", " + MapGuestType(Data, type.get_array_element_type()) + ">"
        elif (type.kind in [TypeKind.ENUM]):
            AddGuestEnum(Data, type)
            return "guest_enum<" + str(type.get_size()) + ", " + name + ">"
        else:
            print("ERROR: ", type.kind)
            return None

def ParseFromCursor(Data: Dict, Namespace: list, Cursor: Cursor) -> None:
    GuestThunks: list = Data["thunks"]
    Entrypoints: list = Data["entrypoints"]
    LibName: str = Data["libname"]

    if (Cursor.kind.is_invalid()):
        Diags = TranslationUnit.diagnostics
        for Diag in Diags:
            logging.warning (Diag.format())

    if (Cursor.kind == CursorKind.TRANSLATION_UNIT):
        for Child in Cursor.get_children():
            ParseFromCursor(Data, Namespace, Child)
    elif (Cursor.kind == CursorKind.NAMESPACE):
        Namespace.append(Cursor.spelling)

        for Child in Cursor.get_children():
            ParseFromCursor(Data, Namespace, Child)

        Namespace.pop()
    elif (Cursor.kind == CursorKind.STRUCT_DECL and Cursor.type.get_declaration().spelling == "fex_gen_config"):
        type: Type = Cursor.type
        fnproto: Type = type.get_template_argument_type(0)
        name: str = None

        assert fnproto.kind == TypeKind.FUNCTIONPROTO

        for Child in Cursor.get_children():
            if (Child.kind == CursorKind.DECL_REF_EXPR):
                name = Child.spelling
        tags : dict = { }

        # non-type arguments don't count in get_num_template_arguments, but do in get_num_template_arguments
        if type.get_num_template_arguments() > 2:
            for i in range(2, type.get_num_template_arguments()):
                tagtype: Type = type.get_template_argument_type(i)
                assert tagtype.kind == TypeKind.RECORD
                tags[tagtype.get_declaration().spelling] = True

        args = []
        for arg in fnproto.argument_types():
            args.append({
                "type": UnqualifiedTypeName(arg),
                "qual_type": arg.spelling,
                "mapped_type": MapGuestType(Data, arg),
                "index": len(args)
            })
        rv_type = fnproto.get_result()
        rv = {
            "type": UnqualifiedTypeName(rv_type),
            "qual_type": rv_type.spelling,
            "mapped_type": MapGuestType(Data, rv_type),
            "index": -1
        }

        hashname = hashlib.sha256((LibName + ":" + name).encode('utf-8')).hexdigest()
        hashname_hostaddr = hashlib.sha256((LibName + ":" + name + "_hostaddr").encode('utf-8')).hexdigest()
        GuestThunks.append({
            "fn_type": fnproto.spelling,
            "name": name,
            "sha256": "0x" + ",0x".join(wrap(hashname, 2)),
            "hostaddr_sha256": "0x" + ",0x".join(wrap(hashname_hostaddr, 2)),
            "args": args,
            "rv": rv,
            "has_rv": rv_type.kind != TypeKind.VOID,
            "tags": tags
        })

        Entrypoints.append(GuestThunks[-1])

            #    print(" arg: ", GetGuestType(arg))
            #print(" Result: ", GetGuestType(fnproto.get_result()))

        #        print(Child.spelling)

def ParseInterface(LibName: str, InterfaceFile, args) -> Dict:
    Index = clang.cindex.Index.create()

    # sadly not available
    # register_function(conf.lib, ("clang_getUnqualifiedType", [Type],Type, Type.from_result), False)

    TU: TranslationUnit = None

    try:
        TU = Index.parse(InterfaceFile, args=args, options=TranslationUnit.PARSE_NONE)
        Diags = TU.diagnostics
        for Diag in Diags:
            logging.warning (Diag.format())
        if (len(Diags)):
            return None
    except TranslationUnitLoadError as TULE:
        logging.error(str(TULE))
        return None

    Data = {"libname": LibName, "structs_dict": {}, "structs": [], "enums_dict": {}, "enums": [], "thunks": [], "entrypoints": []}
    ParseFromCursor(Data, [], TU.cursor)
    Data["entrypoints"].sort(key=lambda x: x["name"]);
    return Data

def RenderTemplate(OutFolder: str, TemplateDirs: list, TemplateNames: list, Data: Dict):

    for TemplateName in TemplateNames:
        environment = jinja2.Environment(loader=jinja2.FileSystemLoader(TemplateDirs))
        template = environment.get_template(TemplateName + ".in")
        template.stream(Data).dump(OutFolder + "/" + TemplateName)

def main():
    if sys.version_info[0] < 3:
        logging.critical ("Python 3 or a more recent version is required.")

    if (len(sys.argv) < 5):
        print ("usage: %s <outfolder> <libname> <interface file> <template names>... --  <template dirs>... -- <clang arguments...>" % (sys.argv[0]))
        sys.exit ("\t-no-linux: Do not parse Linux");


    OutFolder = sys.argv[1]
    LibName = sys.argv[2]
    InterfaceFile = sys.argv[3]
    TemplateNames = None
    TemplateDirs = None
    clangArgs = None

    ArgIndex = 4
    ArgNamesStart = ArgIndex
    while ArgIndex < len(sys.argv):
        Arg = sys.argv[ArgIndex]
        if (Arg == "--"):
            break;

        ArgIndex += 1

    if (ArgNamesStart != ArgIndex):
        TemplateNames = sys.argv[ArgNamesStart:ArgIndex]

    ArgIndex += 1
    ArgDirsStart = ArgIndex

    while ArgIndex < len(sys.argv):
        Arg = sys.argv[ArgIndex]
        if (Arg == "--"):
            break;
        ArgIndex += 1

    if (ArgDirsStart != ArgIndex):
        TemplateDirs = sys.argv[ArgDirsStart:ArgIndex]
    
    ArgIndex += 1

    clangArgs = sys.argv[ArgIndex:]

    Data = ParseInterface(LibName, InterfaceFile, clangArgs)
    RenderTemplate(OutFolder, TemplateDirs, TemplateNames, Data)

if __name__ == "__main__":
# execute only if run as a script
    sys.exit(main())
